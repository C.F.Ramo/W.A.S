﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BoxMouse : MonoBehaviour
{
    Transform cardSort;
    ListMaker script;
    RectTransform cardScroll;
    private Scroll scroll;
    public bool isGeneral;
    public bool sizeChange = false;
    bool mouse = false;
    public bool drag = false;
    public bool moving = false;
    public float currentTime;
    public int stage;
    public int maxStage = 4;
    float delay = 0.25f;
    float pos;
    float baseValue = 136.3722f;
    float scrollPos;
    float interval = 17f;
    float origin = -462;
    float scrollOrgin = 69;
    public float[] intervals;
    float[] scrollIntervals;
    public float newOrigin;
    public float newInterval;
    void Start()
    {
        if (!isGeneral)
        {
            script = GameObject.Find("DBReader").GetComponent<ListMaker>();
            scroll = GameObject.Find("DomainSort").GetComponent<Scroll>();
            cardScroll = this.transform.GetChild(1).GetChild(0).GetComponent<RectTransform>();
            cardSort = this.transform.Find("CardSort");
        }
        else
        {
            script = GameObject.Find("DBReader").GetComponent<ListMaker>();
            cardScroll = GameObject.Find("ListScroll").GetComponent<RectTransform>();
            cardSort = this.transform;
            baseValue = 181.75f;
            interval = 11.46f;
            origin = 0;
            scrollOrgin = 91f;
            mouse = true;
        }
        currentTime = delay + 1;
    }
    public void In()
    {
        if (script.focus) {
        mouse = true;
        if (!isGeneral)
        {
            scroll.mouse = false;
        } }
    }
    public void Out()
    {
        mouse = false;
        if (!isGeneral)
        {
            scroll.mouse = true;
        }
    }
    private void Update()
    {
        if (!drag)
        {
            if (isGeneral && !script.interfaceList) { }
            else if (isGeneral && maxStage == 0) { }
            else if (!isGeneral && script.interfaceList) { }
            else if (!script.gameOn) { }
            else if (mouse && script.focus)
            {
                if (Input.mouseScrollDelta.y < 0 && stage < maxStage)
                {
                    script.PlaySound(script.clips[0], 0.5f, 2);
                    stage++;
                    currentTime = 0;
                }
                else if (Input.mouseScrollDelta.y > 0 && stage > 0)
                {
                    script.PlaySound(script.clips[0], 0.5f, 2);
                    currentTime = 0;
                    stage--;
                }
            }
            if (currentTime <= delay)
            {
                currentTime += Time.deltaTime;
                pos = Mathf.Lerp(cardSort.localPosition.y, origin + (stage * interval), currentTime * 2f);
                cardSort.localPosition = new Vector3(cardSort.localPosition.x, pos);
                scrollPos = Mathf.Lerp(cardScroll.localPosition.y, newOrigin - newInterval * (stage), currentTime);
                cardScroll.localPosition = new Vector2(cardScroll.localPosition.x, scrollPos);
            }
            else
            {
                cardSort.localPosition = new Vector3(cardSort.localPosition.x, origin + (stage * interval));
                cardScroll.localPosition = new Vector2(cardScroll.localPosition.x, newOrigin - newInterval * (stage));
            }
        }
        else
        {
            cardSort.localPosition = new Vector3(cardSort.localPosition.x, scrollIntervals[stage]);
            cardScroll.localPosition = new Vector2(cardScroll.localPosition.x, intervals[stage]);
        }
    }
    public void AddItem(int count)
    {
        sizeChange = true;
        if (cardScroll == null)
        {
            cardScroll = this.transform.GetChild(1).GetChild(0).GetComponent<RectTransform>();
        }
        if (!isGeneral)
        {
            maxStage = count + 7;
        }
        else
        {
            maxStage = count - 17;
            if (maxStage < 0)
            {
                maxStage = 0;
            }
        }
        float scrollSize = baseValue / ((maxStage + 1) * 0.2f);
        if (maxStage > 0)
        {
            cardScroll.sizeDelta = new Vector2(4, Mathf.Clamp(scrollSize, 5, 100));
        }
        else
        {
            cardScroll.sizeDelta = new Vector2(4, baseValue);
        }
        newOrigin = scrollOrgin - (0.5f * cardScroll.sizeDelta.y);
        newInterval = (baseValue - (cardScroll.sizeDelta.y) * 0.8f) / (maxStage + 1);
        intervals = new float[maxStage + 1];
        scrollIntervals = new float[maxStage + 1];
        for (int i = 0; i < intervals.Length; i++)
        {
            intervals[i] = newOrigin - (newInterval * i);
            scrollIntervals[i] = origin + (interval * i);
        }
        sizeChange = false;
    }
}
