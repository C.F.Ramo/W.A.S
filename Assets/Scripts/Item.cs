﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class Item
{
    public List<TextMeshProUGUI> SubTexts;
    public List<string> SubGenders;
    public List<int> SubRefs;
    public Transform Package;
    public Transform Card;
    public Sprite Icon;
    public GameObject Filter;
    public GameObject EraIcon;
    public string Name;
    public string Type;
    public double Price;
    public int Ref;
    public int Era;
    public int Total;
    public int Level;
    public Item(Transform pack, string name, string type, int reference, int era)
    {
        Package = pack;
        Name = name;
        Type = type;
        Ref = reference;
        Era = era;
    }
    public Item(string name, string type, int reference, int era)
    {
        Name = name;
        Type = type;
        Ref = reference;
        Era = era;
    }
}
