﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ShopScroll : MonoBehaviour
{
    private ListMaker script;
    private Scroll scroll;
    private RectTransform size;
    public RectTransform scrollBar;
    public float maxStage = 1;
    bool mouse;
    public float currentTime;
    public float delay = 0.25f;
    float pos;
    public int stage;
    float origin = -42;
    float interval = 22;
    public float newOrigin;
    public float newInterval;
    float scrollPos;
    void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        size = this.transform.Find("ItemSort").GetComponent<RectTransform>();
        scroll = GameObject.Find("DomainSort").GetComponent<Scroll>();
        size.sizeDelta = new Vector2(64.75f, 126f);
        size.localPosition = new Vector2(0, -42);
    }
    private void Update()
    {
        if (mouse && !script.drawing && script.focus)
        {
            if (Input.mouseScrollDelta.y < 0 && stage < maxStage)
            {
                script.PlaySound(script.clips[0], 0.5f, 2);
                stage++;
                currentTime = 0;
            }
            else if (Input.mouseScrollDelta.y > 0 && stage > 0)
            {
                script.PlaySound(script.clips[0], 0.5f, 2);
                stage--;
                currentTime = 0;
            }
        }
        if (currentTime <= delay)
        {
            currentTime += Time.deltaTime;
            pos = Mathf.Lerp(size.localPosition.y, origin + (stage * interval), currentTime * 1f);
            size.localPosition = new Vector3(size.localPosition.x, pos);
            scrollPos = Mathf.Lerp(scrollBar.localPosition.y, newOrigin - newInterval * (stage), currentTime);
            scrollBar.localPosition = new Vector2(scrollBar.localPosition.x, scrollPos);
        }
        else
        {
            scrollBar.localPosition = new Vector2(scrollBar.localPosition.x, newOrigin - newInterval * (stage));
            size.localPosition = new Vector3(size.localPosition.x, origin + (stage * interval));
        }
    }
    public void AddItem()
    {
        double count = (transform.GetChild(0).childCount - 1) / 3;
        maxStage = (float)Math.Ceiling(count);
        scrollBar.sizeDelta = new Vector2(4, 53.36092f / (maxStage + 1));
        newOrigin = (27f - (scrollBar.sizeDelta.y * 0.5f));
        newInterval = 53.36092f / (maxStage + 1);
    }
    public void In()
    {
        if (script.focus)
        {
            scroll.mouse = false;
            mouse = true;
        }
    }
    public void Out()
    {
        scroll.mouse = true;
        mouse = false;
    }
    public void TitleIn()
    {
        if (script.focus)
        {
            script.panel.gameObject.SetActive(true);
            script.panelTitle.text = "Currents";
            script.panelText.text = "Unlocking a new artistic Current allows you to enroll any Artist who depend on it, unless they also depend on another Current which is still locked.";
            script.panelDisplay = true;
            In();
        }
    }
    public void TitleOut()
    {
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
        Out();
    }
}
