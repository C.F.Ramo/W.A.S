﻿using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.Rendering.PostProcessing;

public class ListMaker : MonoBehaviour
{
    // Variables Information
    #region Information

    #region Setup Artists
    DomainShop[] domainShops = new DomainShop[20];
    List<ArtistList> artistDomainLocked = new List<ArtistList>();
    List<ArtistList> artistDomainAvailable = new List<ArtistList>();
    List<ArtistList> artistDomainActive = new List<ArtistList>();
    List<ArtistSimple> simplifiedArtists = new List<ArtistSimple>();
    public List<string> domainShortened = new List<string>();
    public List<string> domain = new List<string>();
    ArtistList artistList = new ArtistList();
    public EffectList effectList = new EffectList();
    public string[] bonusTypes = new string[5];
    public string[] bonusNames = new string[5];
    public string[] bonusDescriptions = new string[5];
    public int[] bonusPrices = new int[5];
    public List<string> subTypes = new List<string>();
    #endregion

    #region  Setup Maluses
    ArtistList shamefulInfluenceLocked = new ArtistList();
    ArtistList[] shamefulInfluenceAvailable = new ArtistList[20];
    ArtistList shamefulInfluenceActive = new ArtistList();
    #endregion

    #region Setup News
    NewsList newsList = new NewsList();
    NewsList newsListAvailable = new NewsList();
    int newsEra = 1;
    int newsRef;
    int newsPlay;
    bool beginning = true;
    bool newsChange;
    float newsTime = 27;
    #endregion

    #endregion
    // Variables Mathematics
    #region Mathematics

    #region Statistics Count
    List<Attribute>[] bonusLists = new List<Attribute>[5];
    List<Attribute> areas = new List<Attribute>();
    List<Attribute> tags = new List<Attribute>();
    List<Attribute> currents = new List<Attribute>();
    Bonus[] bonuses = new Bonus[5];
    double[] domainProductionC = new double[20];
    double[] domainProductionA = new double[20];
    double[] domainProductionP = new double[20];
    float available;
    float erasPassed;
    int women;
    int men;
    int nonBinary;
    int blindEyes;
    int rejected;
    int[] bonusStats = new int[5];
    #endregion

    #region Production Count
    float[] domainMultiplicatorsC = new float[20];
    float[] domainMultiplicatorsA = new float[20];
    float[] domainMultiplicatorsP = new float[20];
    float[] domainEffectMultiplicator = new float[20];
    float[] domainPower = new float[20];
    float totalPower;
    float globalMultiplicatorEffectC;
    float globalMultiplicatorEffectA;
    float globalMultiplicatorEffectP;
    double productionC;
    public double stockC;
    double productionA;
    public double stockA;
    double productionP;
    public double stockP;
    int itemNumber;
    readonly float itemBonus = 5;
    readonly float multEffectiveness = 0.12f;
    readonly float eraFactor = 1.5f;
    readonly float bonusFactor1 = 5f;
    readonly float bonusFactor2 = 10f;
    float eraMultiplicator = 1;
    #endregion

    #region Gauge Count
    public float globalMultiplicator;
    float globalMultiplicatorEffectivenessC;
    float globalMultiplicatorEffectivenessA;
    float globalMultiplicatorEffectivenessP;
    float imperialRatio;
    float patriarcalRatio;
    float heteroRatio;
    float justiceRatio;
    float marginalizedMultiplicator;
    float womenMultiplicator;
    float queerMultiplicator;
    readonly float globalOrigin = 0.5f;
    int currentGauge = 1;
    #endregion

    #region Card Price
    float priceModifierC = 1;
    float priceModifierP = 1;
    #endregion

    #endregion
    // Variables Display
    #region Display

    #region Navigation
    TextMeshProUGUI[] scrollTexts = new TextMeshProUGUI[22];
    public Sprite[] shopEras;
    public RectTransform scrollList;
    public RectTransform domainList;
    public RectTransform scrollButton;
    public CanvasGroup impacts;
    public CanvasGroup impactsButton;
    List<RectTransform> cardBoxes = new List<RectTransform>();
    RectTransform cardBoxShame;
    RectTransform cardBoxItems;
    public Transform canvasTR;
    public GameObject cardBoxOther;
    public GameObject cardBox;
    public GameObject card;
    public GameObject shameCard;
    public GameObject effect;
    public GameObject nation;
    public GameObject clickBonus;
    public int domainScroll = 1;
    float currentTimePanel;
    #endregion

    #region List Interface
    Image canvas;
    Image starMask;
    Image background;
    GameObject cameraDefault;
    GameObject cameraList;
    Canvas cameraDefaultLayer;
    Canvas cameraListLayer;
    GameObject listObjects;
    GameObject sideBar;
    Transform artistsList;
    BoxMouse listScroll;
    public GameObject line;
    public GameObject listEffect;
    public GameObject listNation;
    public GameObject listTitle;
    public GameObject listTitleSmall;
    public GameObject listSubTitle;
    public Sprite backgroundDefault;
    public Sprite backgroundList;
    public Sprite starMaskDefault;
    public Sprite starMaskList;
    public Sprite canvasDefault;
    public Sprite canvasList;
    List<List<int>> effectBoxes = new List<List<int>>();
    public bool interfaceList = false;
    List<GameObject> titles = new List<GameObject>();
    #endregion

    #region Card Sprites
    public List<Sprite> flags = new List<Sprite>();
    #endregion

    #region News
    public TextMeshProUGUI newsText;
    public Transform transformNews;
    #endregion

    #region Statistics
    public List<TextMeshProUGUI> statisticTexts = new List<TextMeshProUGUI>();
    public TextMeshProUGUI productionDisplayC;
    public TextMeshProUGUI stockDisplayC;
    public TextMeshProUGUI productionDisplayA;
    public TextMeshProUGUI stockDisplayA;
    public TextMeshProUGUI productionDisplayP;
    public TextMeshProUGUI stockDisplayP;
    public TextMeshProUGUI gaugeText;
    public TextMeshProUGUI gaugeTitle;
    public CanvasGroup statsDisplay;
    public CanvasGroup gaugeDisplay;
    #endregion

    #region Gauge
    public Transform gauge;
    public Image gaugeIsUp;
    public Image gaugeIsDown;
    float gaugeSize = 0;
    float gaugeTime;
    #endregion

    #region Draw
    public CanvasGroup blur;
    #endregion

    #region Bought Items
    public GameObject smallCurrent;
    public GameObject bigCurrent;
    public GameObject SubCurrent;
    #endregion

    #region Transitions
    public List<GameObject> trendSlots = new List<GameObject>();
    public SocietyStage[] societies = new SocietyStage[13];
    public CanvasGroup transBackground;
    public CanvasGroup transPanel;
    public CanvasGroup transClick;
    public CanvasGroup transParagraph;
    public TextMeshProUGUI transTitle;
    public TextMeshProUGUI transSubTitle;
    public TextMeshProUGUI transText;
    public TextMeshProUGUI name1;
    #endregion

    #region Introduction
    [TextAreaAttribute] public List<string> introTexts = new List<string>();
    public GameObject loadingScreen;
    public List<Artist> selected = new List<Artist>();
    public List<CanvasGroup> introPanels = new List<CanvasGroup>();
    public CanvasGroup validate;
    public CanvasGroup introMenu;
    public GameObject border;
    public GameObject unlockText;
    public GameObject highLight;
    GameObject[] borders;
    List<Transform> firstCards = new List<Transform>();
    List<Transform> secondCards = new List<Transform>();
    Transform introCards;
    #endregion

    #region Explanations
    public TextMeshProUGUI panelText;
    public TextMeshProUGUI panelTitle;
    public TextMeshProUGUI nationTitle;
    public TextMeshProUGUI nationText;
    public RectTransform nationPanel;
    public RectTransform nationPanelIn;
    public RectTransform nationPanelText;
    public RectTransform panel;
    public bool panelDisplay;
    public bool displayOn = true;
    bool noiseOn = true;
    bool screenOn = true;
    bool starsOn = true;
    #endregion

    #region Menu
    public TextMeshProUGUI soundText;
    public TextMeshProUGUI displayText;
    public TextMeshProUGUI noiseText;
    public TextMeshProUGUI starsText;
    public TextMeshProUGUI screenText;
    public PostProcessVolume post;
    Grain grain;
    public CanvasGroup prodStats;
    public CanvasGroup menu;
    public CanvasGroup confirm;
    public TextMeshProUGUI confirmText;
    public bool menuOn = false;
    public bool confirmOn = false;
    public int choice;
    public Transform menuButton;
    #endregion

    #region Sounds
    bool audioOn = true;
    float volume = 1f;
    AudioSource sound;
    AudioSource sound2;
    AudioSource sound3;
    public AudioClip[] clips = new AudioClip[14];
    public AudioClip[] melodies = new AudioClip[6];
    #endregion

    #region Colors
    Color[] domainColors = new Color[22];
    public Color colorMalus;
    readonly Color[] effectColors = new Color[15];
    #endregion

    #endregion
    // Variables Mechanics
    #region Mechanics

    #region DrawCard
    public bool focus = true;
    public Transform cardPlace;
    public List<Transform>[] cards = new List<Transform>[20];
    bool[] infected = new bool[20];
    public bool drawing = false;
    public bool drawOn = false;
    public int domainDraw;
    public int drawRef;
    public bool overDraw = false;
    int[] drawSuspended = new int[20];
    float cardSpeed = 1;
    public bool rightClick = false;
    #endregion

    #region Card Drags
    public Artist[] leadArtists = new Artist[20];
    public Artist[] supremeLeadArtists = new Artist[5];
    public Transform[] domainDrops = new Transform[20];
    public List<Transform> headerDrops = new List<Transform>();
    public bool[] domainLock = new bool[20];
    public List<bool> headerLock = new List<bool>();
    public Transform slotsList;
    public GameObject trendSlot;
    public int cardDomain;
    public int domainDrop;
    public int headerDrop;
    public bool DomainDropAvailable = false;
    public bool HeaderDropAvailable = false;
    public bool drag = false;
    #endregion

    #region Shop
    List<CanvasGroup> newItems = new List<CanvasGroup>();
    public List<Item> activeItems = new List<Item>();
    public ShopScroll shopScroll;
    public Transform storeFront;
    public UnityEngine.Object storeItem;
    public Sprite[] currentIcons = new Sprite[13];
    MajorCurrent[] majorCurrents = new MajorCurrent[13];
    public string[] currentNames = new string[13];
    public int[] currentEras = new int[13];
    public float[] currentPrices = new float[13];
    Item[] availableItems = new Item[13];
    #endregion

    #region Transitions
    public bool endAvailable = false;
    int currentSociety = 0;
    public bool click = false;
    public GameObject star;
    Transform Background;
    List<RectTransform> stars = new List<RectTransform>();
    List<CanvasGroup> starsAlpha = new List<CanvasGroup>();
    Vector2 verticalLimit = new Vector2(-153, 153);
    Vector2 horizontalLimit = new Vector2(-270, 270);
    #endregion

    #region Introduction
    public bool gameOn = false;
    public bool firstDrawOn = false;
    public bool secondDrawOn = false;
    bool infoOn = true;
    bool introButton = false;
    bool goingOn = true;
    bool reversing = false;
    float timeIntro;
    int infoRef;
    #endregion

    #region Save Game
    public Save save;
    bool loading = false;
    #endregion

    #endregion

    void Start()
    {
        #region Set Save
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Save oldSave = (Save)bf.Deserialize(file);
            file.Close();
            if (oldSave.loadOn)
            {
                loading = true;
                save = oldSave;
            }
            else
            {
                loading = false;
                save = new Save();
                save.loadOn = false;
            }
        }
        else
        {
            loading = false;
            save = new Save();
            save.loadOn = false;
        }

        #endregion

        #region Set Sound
        sound = this.GetComponent<AudioSource>();
        sound2 = Camera.main.GetComponent<AudioSource>();
        sound3 = GameObject.Find("User Interface").GetComponent<AudioSource>();

        #endregion

        #region Set Colors
        loadingScreen.SetActive(true);
        float blueColorDiv = 1f / 35;
        for (int i = 0; i < domainColors.Length - 2; i++)
        {
            domainColors[i] = Color.HSVToRGB(0.4f + blueColorDiv * i, 0.5f, 0.5f);
        }
        domainColors[20] = colorMalus;
        domainColors[21] = colorMalus;
        float colorDiv = 1f / (effectColors.Length - 1);
        for (int i = 0; i < effectColors.Length - 1; i++)
        {
            effectColors[i] = Color.HSVToRGB(colorDiv * i, 0.3f, 0.9f);
        }
        effectColors[effectColors.Length - 1] = Color.white;

        #endregion

        #region Read List
        TextAsset DB = Resources.Load("Artist") as TextAsset;
        TextAsset DBS = Resources.Load("Shameful") as TextAsset;
        TextAsset DBE = Resources.Load("Effect") as TextAsset;
        TextAsset DBN = Resources.Load("News") as TextAsset;
        artistList = JsonUtility.FromJson<ArtistList>(DB.text);
        shamefulInfluenceLocked = JsonUtility.FromJson<ArtistList>(DBS.text);
        effectList = JsonUtility.FromJson<EffectList>(DBE.text);
        newsList = JsonUtility.FromJson<NewsList>(DBN.text);
        for (int i = 0; i < shamefulInfluenceAvailable.Length; i++)
        {
            shamefulInfluenceAvailable[i] = new ArtistList();
        }

        #endregion

        #region Prepare Stats
        for (int i = 0; i < subTypes.Count; i++)
        {
            effectBoxes.Add(new List<int>());
        }
        for (int i = 0; i < bonusLists.Length; i++)
        {
            bonusLists[i] = new List<Attribute>();
        }
        for (int i = 0; i < majorCurrents.Length; i++)
        {
            majorCurrents[i] = new MajorCurrent
            {
                SubCurrents = new List<int>(),
                Name = currentNames[i]
            };
        }
        for (int i = 0; i < artistList.Artist.Count; i++)
        {
            bool[] verifs = new bool[majorCurrents.Length];
            for (int y = 0; y < verifs.Length; y++)
            {
                verifs[y] = false;
            }
            for (int x = 0; x < artistList.Artist[i].Currents.Count; x++)
            {
                bool same = false;
                for (int w = 0; w < currents.Count; w++)
                {
                    if (artistList.Artist[i].Currents[x] == currents[w].name)
                    {
                        same = true;
                        currents[w].total++;
                        artistList.Artist[i].currentRefs.Add(w);
                        break;
                    }
                }
                if (!same && artistList.Artist[i].Currents[x] != "None")
                {
                    currents.Add(new Attribute(artistList.Artist[i].Currents[x], 1));
                    artistList.Artist[i].currentRefs.Add(currents.Count - 1);
                }
                for (int w = 0; w < effectList.Effect.Count; w++)
                {
                    if (artistList.Artist[i].Currents[x] == effectList.Effect[w].Name)
                    {
                        for (int y = 0; y < majorCurrents.Length; y++)
                        {
                            if (effectList.Effect[w].SubType == majorCurrents[y].Name + " Current")
                            {
                                if (verifs[y])
                                {
                                    majorCurrents[y].Doubles++;
                                }
                                verifs[y] = true;
                                if (!effectList.Effect[w].Added)
                                {
                                    for (int z = 0; z < currents.Count; z++)
                                    {
                                        if (currents[z].name == effectList.Effect[w].Name)
                                        {
                                            majorCurrents[y].SubCurrents.Add(z);
                                            break;
                                        }
                                    }
                                    effectList.Effect[w].Added = true;
                                }
                            }
                        }
                        break;
                    }
                }
            }
            for (int x = 0; x < artistList.Artist[i].Effects.Count; x++)
            {
                bool found = false;
                for (int w = 0; w < effectList.Effect.Count; w++)
                {
                    if (artistList.Artist[i].Effects[x] == effectList.Effect[w].Name)
                    {
                        artistList.Artist[i].effectRefs.Add(w);
                        found = true;
                    }
                }
                if (!found)
                {
                    print("effect not written " + artistList.Artist[i].Effects[x]);
                }
            }
            for (int x = 0; x < artistList.Artist[i].Tags.Count; x++)
            {
                bool same = false;
                for (int w = 0; w < tags.Count; w++)
                {
                    if (artistList.Artist[i].Tags[x] == tags[w].name)
                    {
                        same = true;
                        tags[w].total++;
                        artistList.Artist[i].tagRefs.Add(w);
                        break;
                    }
                }
                if (!same && artistList.Artist[i].Tags[x] != "None")
                {
                    tags.Add(new Attribute(artistList.Artist[i].Tags[x], 1));
                    artistList.Artist[i].tagRefs.Add(tags.Count - 1);
                }
            }
            for (int x = 0; x < artistList.Artist[i].Nation.Count; x++)
            {
                bool same = false;
                for (int w = 0; w < bonusLists[0].Count; w++)
                {
                    if (artistList.Artist[i].Nation[x] == bonusLists[0][w].name)
                    {
                        same = true;
                        bonusLists[0][w].total++;
                        artistList.Artist[i].nationRefs.Add(w);
                        break;
                    }
                }
                if (!same && artistList.Artist[i].Nation[x] != "None")
                {
                    bonusLists[0].Add(new Attribute(artistList.Artist[i].Nation[x], 1));
                    artistList.Artist[i].nationRefs.Add(bonusLists[0].Count - 1);
                }
            }
            artistList.Artist[i].effectRefs.Sort((x, y) => x.CompareTo(y));
        }
        for (int i = 0; i < effectList.Effect.Count; i++)
        {
            for (int y = 1; y < bonusLists.Length; y++)
            {
                if (effectList.Effect[i].Type == bonusTypes[y])
                {
                    bool typeFound = false;
                    for (int z = 0; z < bonusLists[y].Count; z++)
                    {
                        if (effectList.Effect[i].Name == bonusLists[y][z].name)
                        {
                            typeFound = true;
                        }
                    }
                    if (!typeFound)
                    {
                        bonusLists[y].Add(new Attribute(effectList.Effect[i].Name, 0));
                    }
                }
            }
            for (int y = 0; y < subTypes.Count; y++)
            {
                if (effectList.Effect[i].Type == subTypes[y])
                {
                    effectBoxes[y].Add(i);
                }
            }
        }
        for (int i = 0; i < majorCurrents.Length; i++)
        {
            for (int y = 0; y < majorCurrents[i].SubCurrents.Count; y++)
            {
                majorCurrents[i].Total += currents[majorCurrents[i].SubCurrents[y]].total;
            }
            majorCurrents[i].Total -= majorCurrents[i].Doubles;
        }
        areas.Add(new Attribute("Africa", 0));
        areas.Add(new Attribute("South America", 0));
        areas.Add(new Attribute("Central America", 0));
        areas.Add(new Attribute("North America", 0));
        areas.Add(new Attribute("Eastern Europe", 0));
        areas.Add(new Attribute("Western Europe", 0));
        areas.Add(new Attribute("Middle East", 0));
        areas.Add(new Attribute("Southern Asia", 0));
        areas.Add(new Attribute("Eastern Asia", 0));
        for (int x = 0; x < bonusLists[0].Count; x++)
        {
            Attribute nationID = bonusLists[0][x];
            string nationName = nationID.name;
            if (nationName == "Nigeria" ||
                nationName == "South Africa" ||
                nationName == "Algeria" ||
                nationName == "Morocco" ||
                nationName == "Kenya" ||
                nationName == "Ethiopia" ||
                nationName == "Angola" ||
                nationName == "Ghana" ||
                nationName == "Tanzania" ||
                nationName == "Cameroon" ||
                nationName == "Tunisia" ||
                nationName == "Ugandda" ||
                nationName == "Senegal" ||
                nationName == "Zambia" ||
                nationName == "Zimbabwe" ||
                nationName == "Mali" ||
                nationName == "Gabon" ||
                nationName == "Burkina Faso" ||
                nationName == "Mozambique" ||
                nationName == "Namibia" ||
                nationName == "Congo" ||
                nationName == "Somalia" ||
                nationName == "Mauritania" ||
                nationName == "Liberia" ||
                nationName == "Cape Verde"
                )
            {
                nationID.area = 0;
                areas[0].total += nationID.total;
            }
            else if (nationName == "Argentina" ||
                nationName == "Bolivia" ||
                nationName == "Brazil" ||
                nationName == "Chile" ||
                nationName == "Colombia" ||
                nationName == "Ecuador" ||
                nationName == "Guyana" ||
                nationName == "Paraguay" ||
                nationName == "Peru" ||
                nationName == "Uruguay" ||
                nationName == "Suriname" ||
                nationName == "Venezuela"
                )
            {
                nationID.area = 1;
                areas[1].total += nationID.total;
            }
            else if (nationName == "Barbados" ||
                nationName == "Cayman Islands" ||
                nationName == "Mexico" ||
                nationName == "Costa Rica" ||
                nationName == "Cuba" ||
                nationName == "Dominica" ||
                nationName == "Guatemala" ||
                nationName == "Haiti" ||
                nationName == "Jamaica" ||
                nationName == "Martinique" ||
                nationName == "Nicaragua" ||
                nationName == "Puerto Rico"
                )
            {
                nationID.area = 2;
                areas[2].total += nationID.total;
            }
            else if (nationName == "Canada" ||
                nationName == "United States"
                )
            {
                nationID.area = 3;
                areas[3].total += nationID.total;
            }
            else if (nationName == "Estonia" ||
                nationName == "Latvia" ||
                nationName == "Lithuania" ||
                nationName == "Belarus" ||
                nationName == "Russia" ||
                nationName == "Ukraine" ||
                nationName == "Romania" ||
                nationName == "Czechia" ||
                nationName == "Croatia" ||
                nationName == "Serbia" ||
                nationName == "Slovakia" ||
                nationName == "Slovenia" ||
                nationName == "Yugoslavia" ||
                nationName == "Albania" ||
                nationName == "Bulgaria" ||
                nationName == "Greece" ||
                nationName == "Macedonia" ||
                nationName == "Austria" ||
                nationName == "Hungary" ||
                nationName == "Poland" ||
                nationName == "Austria-Hungary"
                )
            {
                nationID.area = 4;
                areas[4].total += nationID.total;
            }
            else if (nationName == "Belgium" ||
                nationName == "Denmark" ||
                nationName == "Finland" ||
                nationName == "Germany" ||
                nationName == "Iceland" ||
                nationName == "Ireland" ||
                nationName == "Italy" ||
                nationName == "Luxembourg" ||
                nationName == "Netherlands" ||
                nationName == "Norway" ||
                nationName == "Portugal" ||
                nationName == "Spain" ||
                nationName == "Sweden" ||
                nationName == "Switzerland" ||
                nationName == "England" ||
                nationName == "Wales" ||
                nationName == "Scotland" ||
                nationName == "France"
                )
            {
                nationID.area = 5;
                areas[5].total += nationID.total;
            }
            else if (nationName == "Kyrgyzstan" ||
                nationName == "Armenia" ||
                nationName == "Azerbaijan" ||
                nationName == "Iran" ||
                nationName == "Georgia" ||
                nationName == "Egypt" ||
                nationName == "Iraq" ||
                nationName == "Israel" ||
                nationName == "Palestine" ||
                nationName == "Lebanon" ||
                nationName == "Pakistan" ||
                nationName == "Syria" ||
                nationName == "Turkey" ||
                nationName == "Yemen"
                )
            {
                nationID.area = 6;
                areas[6].total += nationID.total;
            }
            else if (nationName == "Bangladesh" ||
                nationName == "Cambodia" ||
                nationName == "India" ||
                nationName == "Indonesia" ||
                nationName == "Laos" ||
                nationName == "Malaysia" ||
                nationName == "Philippines" ||
                nationName == "Singapore" ||
                nationName == "Sri Lanka" ||
                nationName == "Thailand" ||
                nationName == "Vietnam" ||
                nationName == "Australia" ||
                nationName == "New Zealand" ||
                nationName == "Yogyakarta"
                )
            {
                nationID.area = 7;
                areas[7].total += nationID.total;
            }
            else if (nationName == "China" ||
                nationName == "Korea" ||
                nationName == "North Korea" ||
                nationName == "Japan" ||
                nationName == "Taiwan" ||
                nationName == "Hong Kong"
                )
            {
                nationID.area = 8;
                areas[8].total += nationID.total;
            }
        }
        bool nHDrop = false;
        headerLock.Add(nHDrop);
        save.headerLock.Add(nHDrop);

        #endregion

        #region Set Mathematical Values
        post.profile.TryGetSettings(out grain);
        available = 0;
        globalMultiplicatorEffectivenessC = globalOrigin;
        globalMultiplicatorEffectivenessA = globalOrigin;
        globalMultiplicatorEffectivenessP = globalOrigin;
        gauge.localScale = new Vector2(1, 0);

        #endregion

        #region Set List Interface
        canvas = GameObject.Find("Canvas").GetComponent<Image>();
        Background = GameObject.Find("Background").transform;
        background = GameObject.Find("OuterSpace").GetComponent<Image>();
        starMask = Background.gameObject.GetComponent<Image>();
        sideBar = GameObject.Find("Sidebar");
        cameraDefault = GameObject.Find("Camera");
        cameraList = GameObject.Find("ListCamera");
        cameraDefaultLayer = GameObject.Find("Cards").GetComponent<Canvas>();
        cameraListLayer = GameObject.Find("ListInterface").GetComponent<Canvas>();
        listObjects = GameObject.Find("ListObjects");
        artistsList = GameObject.Find("ArtistsList").transform;
        canvasTR = GameObject.Find("User Interface").transform;
        introCards = GameObject.Find("IntroCards").transform;
        listScroll = artistsList.GetComponent<BoxMouse>();
        canvas.sprite = canvasDefault;
        starMask.sprite = starMaskDefault;
        background.sprite = backgroundDefault;
        sideBar.SetActive(true);
        cameraDefault.gameObject.SetActive(true);
        cameraList.gameObject.SetActive(false);
        listObjects.SetActive(false);
        cameraList.gameObject.SetActive(false);
        interfaceList = false;
        cameraDefaultLayer.sortingOrder = 1;
        cameraListLayer.sortingOrder = 0;

        #endregion

        #region Divide List
        for (int i = 0; i < domain.Count; i++)
        {
            artistDomainLocked.Add(new ArtistList());
            artistDomainAvailable.Add(new ArtistList());
            artistDomainActive.Add(new ArtistList());
            if (!loading)
            {
                save.artistDomainActive.Add(new List<string>());
                save.isArtistLead.Add(new List<int>());
                save.eraList.Add(new int());
            }
            for (int y = 0; y < artistList.Artist.Count; y++)
            {
                if (artistList.Artist[y].Domain == domain[i])
                {
                    artistDomainLocked[i].Artist.Add(artistList.Artist[y]);
                }
            }
            artistDomainLocked[i].name = artistDomainLocked[i].Artist[0].Domain;
            artistDomainAvailable[i].name = artistDomainLocked[i].Artist[0].Domain;
            artistDomainActive[i].name = artistDomainLocked[i].Artist[0].Domain;
        }

        #endregion

        #region Create Cardboxes
        List<RectTransform> motherCardBoxes = new List<RectTransform>();
        for (int i = 0; i < domain.Count + 2; i++)
        {
            if (i == 0)
            {
                RectTransform newBox = GameObject.Instantiate(cardBoxOther, domainList).GetComponent<RectTransform>();
                Transform background = newBox.Find("Background");
                background.GetComponent<Image>().color = domainColors[21];
                cardBoxItems = background.Find("CardSort").GetComponent<RectTransform>();
                TextMeshProUGUI domainText = newBox.Find("CardBox Shop").Find("Text").GetComponent<TextMeshProUGUI>();
                domainText.text = "Here are the major Currents\nyou have unlocked.";
                domainText.fontSize = 7;
                domainText.alignment = TextAlignmentOptions.Center;
                newBox.Find("CardBox Shop").Find("DomainTitle").GetComponent<TextMeshProUGUI>().text = "Directions";
            }
            else if (i < domain.Count + 1)
            {
                motherCardBoxes.Add(GameObject.Instantiate(cardBox, domainList).GetComponent<RectTransform>());
                Transform background = motherCardBoxes[i - 1].Find("Background");
                cardBoxes.Add(background.Find("CardSort").GetComponent<RectTransform>());
                background.GetComponent<Image>().color = domainColors[i - 1];
                Transform domainShop = motherCardBoxes[i - 1].Find("CardBox Shop").Find("CardSort");
                domainShop.GetComponent<DomainShop>().Initiate(i - 1, artistDomainLocked[i - 1].Artist.Count.ToString());
                domainShop.parent.Find("DomainName").GetComponent<TextMeshProUGUI>().text = artistDomainAvailable[i - 1].name;
                Transform image = domainShop.GetChild(0).Find("Dark");
                Color.RGBToHSV(domainColors[i - 1], out float H, out _, out _);
                image.GetComponent<Image>().color = Color.HSVToRGB(H, 0.3f, 0.9f);
                image.GetChild(0).GetComponent<Image>().color = Color.HSVToRGB(H, 0.2f, 0.9f);
                Transform dropBox = domainShop.Find("Card (2)");
                dropBox.GetComponent<DomainDrop>().domain = i - 1;
                domainDrops[i - 1] = dropBox;
                domainShops[i - 1] = domainShop.GetComponent<DomainShop>();
            }
            else
            {
                RectTransform newBox = GameObject.Instantiate(cardBoxOther, domainList).GetComponent<RectTransform>();
                Transform background = newBox.Find("Background");
                background.GetComponent<Image>().color = domainColors[20];
                cardBoxShame = background.Find("CardSort").GetComponent<RectTransform>();
                newBox.Find("CardBox Shop").Find("DomainTitle").GetComponent<TextMeshProUGUI>().text = "Banished Influences";
                newBox.Find("CardBox Shop").Find("Text").GetComponent<TextMeshProUGUI>().text = "These artists have exerted a substantial influence on their respective domains, while also causing terrible damage. Their acts are now part of our history, but by condemning them, we've made a move toward improvement.";
            }
        }
        for (int i = 0; i < domain.Count; i++)
        {
            domainShops[i].price = EraPrice(domainShops[i].domain, domainShops[i]);
        }

        #endregion

        #region Fill Shop
        for (int i = 0; i < majorCurrents.Length; i++)
        {
            majorCurrents[i].Era = currentEras[i];
            if (!loading)
            {
                if (majorCurrents[i].Era <= 1)
                {
                    DisplayItem(i);
                }
            }
        }

        #endregion

        #region Prepare ScrollList
        for (int i = 0; i < scrollTexts.Length; i++)
        {
            scrollTexts[i] = scrollList.Find("Text (TMP) (" + i + ")").GetComponent<TextMeshProUGUI>();
        }
        scrollTexts[0].text = "---  ";
        scrollTexts[1].text = "Painting";
        scrollTexts[1].color = domainColors[0];

        #endregion

        #region Prepare TrendSlots
        headerDrops.Add(slotsList.Find("TrendSlot"));
        #endregion

        #region Set News
        if (newsEra < 2)
        {
            for (int i = 0; i < newsList.News.Count; i++)
            {
                if (newsList.News[i].Era == newsEra)
                {
                    newsListAvailable.News.Add(newsList.News[i]);
                }
            }
        }

        #endregion

        #region Display First Stats
        if (!loading)
        {
            for (int i = 0; i < domainLock.Length; i++)
            {
                domainLock[i] = false;
                save.domainLock[i] = false;
            }
            for (int i = 0; i < headerLock.Count; i++)
            {
                headerLock[i] = false;
                save.domainLock[i] = false;
            }
            DisplayStats();
            StartCoroutine(Beginning());
            save.displayOn = true;
            save.audioOn = true;
            save.noiseOn = true;
            save.starsOn = true;
            save.screenOn = true;
            blindEyes = 0;
        }
        else
        {
            LoadGame();
        }
        if (displayOn)
        {
            displayText.text = "Info On";
        }
        else
        {
            displayText.text = "Info Off";
        }
        if (audioOn)
        {
            soundText.text = "Audio On";
        }
        else
        {
            soundText.text = "Audio Off";
        }
        if (noiseOn)
        {
            noiseText.text = "Noise On";
        }
        else
        {
            noiseText.text = "Noise Off";
        }
        if (starsOn)
        {
            starsText.text = "Stars On";
        }
        else
        {
            starsText.text = "Stars Off";
        }
        artistList.Artist.Clear();
        #endregion

    }
    void Update()
    {
        // Click Check
        if (click && !introButton && focus)
        {
            if (Input.GetMouseButtonDown(0))
            {
                click = false;
                if (!gameOn)
                {
                    PlaySound(clips[13], 1.55f, 2);
                }
            }
        }
        // Update (Game)
        if (gameOn)
        {
            // Numbers Display
            stockDisplayC.text = RoundNumber(stockC, productionC, false);
            stockDisplayA.text = RoundNumber(stockA, productionA, false);
            stockDisplayP.text = RoundNumber(stockP, productionP, false);
            // Routines
            newsTime += Time.deltaTime;
            stockC += productionC * Time.deltaTime;
            stockA += productionA * Time.deltaTime;
            stockP += productionP * Time.deltaTime;
            save.stockC = stockC;
            save.stockA = stockA;
            save.stockP = stockP;
            // Gauge Animation
            if (gaugeTime < 2)
            {
                gaugeTime += Time.deltaTime;
                gauge.localScale = new Vector3(gauge.localScale.x, Mathf.Lerp(gauge.localScale.y, gaugeSize, gaugeTime * 0.5f));
                if (gauge.localScale.y > 0.005f)
                {
                    if (globalMultiplicator < 0)
                    {
                        gauge.transform.SetParent(gaugeIsUp.transform);
                    }
                    else
                    {
                        gauge.transform.SetParent(gaugeIsDown.transform);
                    }
                }
            }
            // News Animation
            if (newsTime > 20)
            {
                StartCoroutine(NewsChange());
                SaveGame();
                newsTime = 0;
            }
            // Input Hide Draw
            if (drawOn && focus)
            {
                if (!overDraw && !rightClick && !Input.GetMouseButton(1))
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        StartCoroutine(HideDraw(drawRef));
                    }
                }
            }
            // Panel Position
            if (panelDisplay)
            {
                if (Input.GetMouseButton(1))
                {
                    if (currentTimePanel < 0.1f)
                    {
                        currentTimePanel += Time.deltaTime;
                        panel.transform.localScale = new Vector2(Mathf.Lerp(panel.transform.localScale.x, 1.5f, currentTimePanel * 10), Mathf.Lerp(panel.transform.localScale.y, 1.5f, currentTimePanel * 10));
                    }
                }
                else
                {
                    if (currentTimePanel < 0.1f)
                    {
                        currentTimePanel += Time.deltaTime;
                        panel.transform.localScale = new Vector2(Mathf.Lerp(panel.transform.localScale.x, 1, currentTimePanel * 10), Mathf.Lerp(panel.transform.localScale.y, 1, currentTimePanel * 10));
                    }
                }
                if (Input.GetMouseButtonUp(1) || Input.GetMouseButtonDown(1))
                {
                    currentTimePanel = 0;
                }
                if (interfaceList)
                {
                    panel.position = new Vector3(Mathf.Clamp(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, -3.76f, 5.99f), Mathf.Clamp(Camera.main.ScreenToWorldPoint(Input.mousePosition).y, -3.81f, 1f), 1000);
                }
                else
                {
                    panel.position = new Vector3(Mathf.Clamp(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, -3.76f, 6.21f), Mathf.Clamp(Camera.main.ScreenToWorldPoint(Input.mousePosition).y, -3.81f, -0.86f), 1000);
                }
            }
            else
            {
                if (!Input.GetMouseButton(1))
                {
                    panel.transform.localScale = new Vector2(1, 1);
                }
            }
        }
        // Update (Intro)
        else
        {
            if (secondDrawOn)
            {
                if (selected.Count == 2)
                {
                    if (!validate.gameObject.activeSelf)
                    {
                        validate.gameObject.SetActive(true);
                    }
                    if (validate.alpha < 1)
                    {
                        validate.alpha += Time.deltaTime;
                    }
                }
                else
                {
                    if (validate.gameObject.activeSelf)
                    {
                        validate.gameObject.SetActive(false);
                    }
                    if (validate.alpha >= 0)
                    {
                        validate.alpha -= Time.deltaTime;
                    }
                }
            }
        }
        #region Debug Endgame
        /*if (Input.GetKey(KeyCode.Space))
        {
            stockA += (productionA * 100);
        }*/
        #endregion
        // Stars
        if (starsOn)
        {
            for (int i = 0; i < stars.Count; i++)
            {
                starsAlpha[i].alpha += UnityEngine.Random.Range(-0.01f, 0.01f);
                stars[i].localPosition = new Vector2(stars[i].localPosition.x + 0.02f, stars[i].localPosition.y);
                if (stars[i].localPosition.x > horizontalLimit.y)
                {
                    stars[i].localPosition = new Vector2(horizontalLimit.x, stars[i].localPosition.y);
                }
            }
        }
        // Right Click
        if (Input.GetMouseButtonDown(1))
        {
            rightClick = true;
        }
        if (Input.GetMouseButtonDown(0))
        {
            rightClick = false;
        }
    }

    // Functions Cards
    #region Actions: Cards

    public IEnumerator DrawCards(int domain, int drawNumber)
    {
        drawing = true;
        HidePanel();
        PlaySound(clips[6], 0.4f, 2);
        if (drawOn)
        {
            drawOn = false;
            if (domainDraw == domain)
            {
                float currentTime = 0;
                //blur.blocksRaycasts = false;
                while (currentTime <= 0.5f)
                {
                    currentTime += Time.deltaTime;
                    for (int i = 0; i < cards[domain].Count; i++)
                    {
                        cards[domain][i].localPosition = new Vector2(cards[domain][i].localPosition.x, Mathf.Lerp(cards[domain][i].localPosition.y, 250, currentTime * cardSpeed));
                    }
                    yield return null;
                }
                for (int i = 0; i < cards[domain].Count; i++)
                {
                    if (cards[domain][i] != card)
                    {
                        GameObject.Destroy(cards[domain][i].gameObject);
                    }
                }
                cards[domain].Clear();
            }
            else
            {
                float currentTime = 0;
                drawSuspended[domainDraw] = 1;
                while (currentTime <= 1 && drawSuspended[domainDraw] == 1)
                {
                    currentTime += Time.deltaTime;
                    for (int i = 0; i < cards[domainDraw].Count; i++)
                    {
                        cards[domainDraw][i].localPosition = new Vector2(cards[domainDraw][i].localPosition.x, Mathf.Lerp(cards[domainDraw][i].localPosition.y, 250, currentTime * 0.5f));
                    }
                    yield return null;
                }
                for (int i = 0; i < cards[domainDraw].Count; i++)
                {
                    cards[domainDraw][i].localPosition = new Vector2(cards[domainDraw][i].localPosition.x, 250);
                }
            }
        }
        drawRef = domain;
        domainDraw = domain;
        if (drawSuspended[domain] == 1)
        {
            drawSuspended[domain] = 0;
            for (int y = 0; y < cards[domain].Count; y++)
            {
                CardSelection script = cards[domain][y].GetComponent<CardSelection>();
                Artist cardArtist = script.artist;
                if (cardArtist.Shame != null)
                {
                    script.price = Price(domain, 4);
                    TextMeshProUGUI cashDescription = cards[domain][y].Find("ProductionC").GetComponent<TextMeshProUGUI>();
                    cashDescription.text = "Cost of banishment: " + RoundNumber(script.price, true) + " <sprite index=1>";
                    script.outOfDomain = true;
                }
                else
                {
                    TextMeshProUGUI cardPrice = cards[domain][y].Find("ProductionA").GetComponent<TextMeshProUGUI>();
                    cards[domain][y].transform.localScale = new Vector2(0.88f, 0.88f);
                    script.price = Price(domain, cardArtist.Effects.Count);
                    cardPrice.text = "Price: " + RoundNumber(script.price, true) + " <sprite index=1>";
                }
            }
            if (infected[domain])
            {
                StartCoroutine(DisplayAbout(2));
            }
            float currentTime = 0;
            blur.blocksRaycasts = true;
            while (currentTime <= 0.5f)
            {
                currentTime += Time.deltaTime;
                blur.alpha = Mathf.Lerp(0, 1, currentTime * 2);
                for (int i = 0; i < cards[domain].Count; i++)
                {
                    cards[domain][i].localPosition = new Vector2(cards[domain][i].localPosition.x, Mathf.Lerp(cards[domain][i].localPosition.y, -60, currentTime * cardSpeed));
                }
                yield return null;
            }
            blur.alpha = 1;
            drawing = false;
            drawOn = true;
        }
        else
        {
            int shame = 0;
            if (shamefulInfluenceAvailable[domain].Artist.Count > 0)
            {
                if (drawNumber < 5)
                {
                    drawNumber++;
                    shame = 1;
                }
            }
            int[] cardNumbers = new int[drawNumber];
            cards[domain] = new List<Transform>();
            for (int y = 0; y < drawNumber - shame; y++)
            {
                if (artistDomainAvailable[domain].Artist.Count <= drawNumber - shame)
                {
                    cardNumbers[y] = y;
                }
                else
                {
                    int verified = 0;
                    int number = UnityEngine.Random.Range(0, artistDomainAvailable[domain].Artist.Count);
                    while (verified < (drawNumber - 1) - shame)
                    {
                        for (int i = 0; i < drawNumber - shame; i++)
                        {
                            if (number == cardNumbers[i])
                            {
                                verified = 0;
                                number = UnityEngine.Random.Range(0, artistDomainAvailable[domain].Artist.Count);
                                i = 0;
                            }
                            else if (y != i)
                            {
                                verified++;
                            }
                        }
                        yield return null;
                    }
                    cardNumbers[y] = number;
                }
            }
            if (shamefulInfluenceAvailable[domain].Artist.Count > 0)
            {
                cardNumbers[drawNumber - 1] = 666;
                infected[domain] = true;
                StartCoroutine(DisplayAbout(2));
            }
            else
            {
                infected[domain] = false;
            }
            for (int y = 0; y < drawNumber; y++)
            {
                Artist currentArtist;
                bool isBad = false;
                if (cardNumbers[y] == 666)
                {
                    cards[domain].Add(GameObject.Instantiate(shameCard, cardPlace).transform);
                    isBad = true;
                    currentArtist = shamefulInfluenceAvailable[domain].Artist[UnityEngine.Random.Range(0, shamefulInfluenceAvailable[domain].Artist.Count)];
                }
                else
                {
                    cards[domain].Add(GameObject.Instantiate(card, cardPlace).transform);
                    currentArtist = artistDomainAvailable[domain].Artist[cardNumbers[y]];
                }
                DisplayCard(currentArtist, cards[domain][y], domain, isBad, false);
            }
            // Animate cards
            StartCoroutine(SpreadCards(drawNumber, cards[domain], true));
        }
        yield break;
    }
    public IEnumerator HideDraw(int domain)
    {
        if (drawOn)
        {
            drawing = true;
            PlaySound(clips[4], 1f, 2);
            float currentTime = 0;
            drawSuspended[domain] = 1;
            drawOn = false;
            blur.blocksRaycasts = false;
            while (currentTime <= 0.5f && drawSuspended[domain] == 1)
            {
                currentTime += Time.deltaTime;
                blur.alpha = 1 - currentTime * 2;
                for (int i = 0; i < cards[domain].Count; i++)
                {
                    cards[domain][i].localPosition = new Vector2(cards[domain][i].localPosition.x, Mathf.Lerp(cards[domain][i].localPosition.y, 250, currentTime * cardSpeed));
                }
                yield return null;
            }
            for (int i = 0; i < cards[domain].Count; i++)
            {
                cards[domain][i].localPosition = new Vector2(cards[domain][i].localPosition.x, 250);
            }
            blur.alpha = 0;
            drawing = false;
        }
        yield break;
    }
    public IEnumerator CardSelect(int domain, Artist artist, Transform newCard, bool outOfDomain)
    {
        domainLock[domain] = false;
        save.domainLock[domain] = false;
        for (int i = 0; i < headerLock.Count; i++)
        {
            headerLock[i] = false;
            save.headerLock[i] = false;
        }
        drawing = true;
        newCard.GetComponent<CardSelection>().enabled = false;
        newCard.GetComponent<EventTrigger>().enabled = false;
        PlaySound(clips[13], 3f, 2);
        drawOn = false;
        Artist current = artist;
        // Organize Cards
        Transform movedCard = newCard;
        ArtistList currentDomain = artistDomainActive[0];
        if (outOfDomain)
        {
            cardBoxShame.parent.GetComponent<BoxMouse>().AddItem(shamefulInfluenceActive.Artist.Count);
            currentDomain = shamefulInfluenceActive;
            int domainRef = 0;
            for (int i = 0; i < artistDomainAvailable.Count; i++)
            {
                if (artistDomainAvailable[i].name == current.Domain)
                {
                    domainRef = i;
                }
            }
            currentDomain.Artist.Add(current);
            save.shamefulInfluenceActive.Add(current.Name);
            shamefulInfluenceAvailable[domainRef].Artist.Remove(current);
        }
        else
        {
            if (infected[domain])
            {
                blindEyes++;
                save.blindEyes++;
                StartCoroutine(GaugeChange(3));
            }
            domainShops[domain].boxMouse.AddItem(artistDomainActive[domain].Artist.Count);
            currentDomain = artistDomainActive[domain];
            currentDomain.Artist.Add(current);
            save.artistDomainActive[domain].Add(current.Name);
            save.isArtistLead[domain].Add(current.Lead);
            artistDomainAvailable[domain].Artist.Remove(current);
            for (int w = 0; w < current.Nation.Count; w++)
            {
                newCard.Find("NationSort").GetChild(w).GetComponent<NationMouse>().cardActive = true;
            }
        }
        AddCard(artist, newCard, domain, outOfDomain);
        if (!outOfDomain)
        {
            domainShops[domain].cardText.text = domainShops[domain].availableCards.ToString();
            domainShops[domain].cardText2.text = artistDomainLocked[domain].Artist.Count.ToString();
        }
        currentDomain.Artist.Sort((x, y) => x.Birth.CompareTo(y.Birth));
        float translationAmount = 350;
        if (currentDomain.Artist.Count > 1)
        {
            for (int i = 0; i < currentDomain.Artist.Count; i++)
            {
                if (currentDomain.Artist[i].Card == newCard && i + 1 < currentDomain.Artist.Count)
                {
                    movedCard = currentDomain.Artist[i + 1].Card;
                    translationAmount = 100;
                    break;
                }
            }
        }
        // Hide Cards
        float actual;
        float actual2;
        float currentTime = 0;
        blur.blocksRaycasts = false;
        while (currentTime <= 0.5f)
        {
            currentTime += Time.deltaTime;
            blur.alpha = 1 - currentTime * 2;
            for (int i = 0; i < cards[domain].Count; i++)
            {
                if (cards[domain][i] != newCard)
                {
                    actual = Mathf.Lerp(cards[domain][i].localPosition.y, 250, currentTime * cardSpeed);
                    cards[domain][i].localPosition = new Vector2(cards[domain][i].localPosition.x, actual);
                }
                else if (movedCard != newCard)
                {
                    actual = Mathf.Lerp(cards[domain][i].localPosition.y, -250, currentTime * cardSpeed);
                    actual2 = Mathf.Lerp(movedCard.localPosition.x, movedCard.localPosition.x + translationAmount, currentTime * cardSpeed);
                    cards[domain][i].localPosition = new Vector2(cards[domain][i].localPosition.x, actual);
                    movedCard.localPosition = new Vector2(actual2, movedCard.localPosition.y);
                }
                else
                {
                    movedCard.localPosition = new Vector2(movedCard.localPosition.x, Mathf.Lerp(movedCard.localPosition.y, -250, currentTime * cardSpeed));
                }
            }
            yield return null;
        }
        for (int i = 0; i < cards[domain].Count; i++)
        {
            if (cards[domain][i] != newCard)
            {
                cards[domain][i].localPosition = new Vector2(cards[domain][i].localPosition.x, 250);
            }
            else if (movedCard != newCard)
            {
                cards[domain][i].localPosition = new Vector2(cards[domain][i].localPosition.x, 250);
                movedCard.localPosition = new Vector2(movedCard.localPosition.x + translationAmount, movedCard.localPosition.y);
            }
            else
            {
                movedCard.localPosition = new Vector2(movedCard.localPosition.x, -250);
            }
        }
        blur.alpha = 0;
        // Set Parents
        int reference = 0;
        for (int i = 0; i < currentDomain.Artist.Count; i++)
        {
            if (currentDomain.Artist[i] == artist)
            {
                reference = i;
            }
        }
        if (reference == 0)
        {
            if (!outOfDomain)
            {
                artist.Card.SetParent(cardBoxes[domain]);
            }
            else
            {
                artist.Card.SetParent(cardBoxShame);
            }
        }
        else
        {
            artist.Card.SetParent(currentDomain.Artist[reference - 1].Card.Find("NextCard"));
        }
        artist.Card.localPosition = Vector2.zero;
        if (reference < currentDomain.Artist.Count - 1)
        {
            currentDomain.Artist[reference + 1].Card.SetParent(currentDomain.Artist[reference].Card.Find("NextCard"));
            currentDomain.Artist[reference + 1].Card.localPosition = Vector2.zero;
        }
        #region reparent all
        /*currentDomain.Artist[0].Card.SetParent(cardBoxes[domain]);
        for (int i = 1; i < currentDomain.Artist.Count; i++)
        {
            currentDomain.Artist[i].Card.SetParent(currentDomain.Artist[i - 1].Card.Find("NextCard"));
            currentDomain.Artist[i].Card.localPosition = Vector2.zero;
        }*/
        #endregion
        // Update Stats
        int cardEra = 1;
        if (!outOfDomain)
        {
            if (current.Birth > 1970)
            {
                cardEra = 7;
            }
            else if (current.Birth > 1940)
            {
                cardEra = 6;
            }
            else if (current.Birth > 1905)
            {
                cardEra = 5;
            }
            else if (current.Birth > 1870)
            {
                cardEra = 4;
            }
            else if (current.Birth > 1835)
            {
                cardEra = 3;
            }
            else if (current.Birth > 1800)
            {
                cardEra = 2;
            }
            if (newsEra < cardEra)
            {
                for (int i = 0; i < majorCurrents.Length; i++)
                {
                    if (majorCurrents[i].Level == 0)
                    {
                        if (majorCurrents[i].Era <= cardEra && !majorCurrents[i].Unlocked)
                        {
                            save.itemsAvailable[i] = true;
                            DisplayItem(i);
                        }
                    }
                    newsEra = cardEra;
                    newsChange = true;
                }
                int bonusNumber = 0;
                int childCount = 0;
                for (int i = 0; i < bonuses.Length; i++)
                {
                    if (bonuses[i] != null)
                    {
                        bonusNumber++;
                        childCount = bonuses[i].Item.transform.parent.childCount;
                    }
                }
                if (bonusNumber > 0)
                {
                    for (int i = 0; i < bonuses.Length; i++)
                    {
                        if (bonuses[i] != null)
                        {
                            bonuses[i].Item.transform.SetSiblingIndex(childCount - (i));
                        }
                    }
                }
                StartCoroutine(ItemsFade());
            }
        }
        // Set Cards
        if (movedCard != newCard)
        {
            movedCard.localPosition = Vector2.zero;
        }
        Vector2 destination = Vector2.zero;
        float basis = 460;
        if (outOfDomain)
        {
            basis = 494;
        }
        if (newCard == currentDomain.Artist[0].Card)
        {
            destination = new Vector2(0, basis);
        }
        else
        {
            currentDomain.Artist[0].Card.transform.localPosition = new Vector2(0, basis);
        }
        newCard.localPosition = new Vector2(destination.x + 100, destination.y);
        currentTime = 0;
        // Check Society Stage
        if (available >= societies[currentSociety + 1].Population)
        {
            currentSociety++;
            RectTransform panelSize = transPanel.GetComponent<RectTransform>();
            panelSize.sizeDelta = new Vector2(panelSize.sizeDelta.x, 80);
            transClick.transform.localPosition = new Vector2(transClick.transform.localPosition.x, -21.8f);
            transTitle.transform.localPosition = new Vector2(transTitle.transform.localPosition.x, 24.2f);
            transText.transform.localPosition = new Vector2(transText.transform.localPosition.x, -7.1f);
            transPanel.transform.localPosition = new Vector2(transPanel.transform.localPosition.x, -3);
            transTitle.text = societies[currentSociety - 1].Name + " becomes The " + societies[currentSociety].Name + ".";
            transSubTitle.text = "Your gathering reached the milestone of " + societies[currentSociety].Population + " Artists.";
            if (currentSociety == 2 || currentSociety == 4 || currentSociety == 6 || currentSociety == 9)
            {
                if (currentSociety == 3)
                {
                    slotsList.parent.Find("Trendsetters").GetComponent<TextMeshProUGUI>().text = "Global Trendsetters";
                }
                PlaySound(melodies[2], 1f, 3);
                trendSlots[0].SetActive(true);
                headerDrops.Add(trendSlots[0].transform);
                trendSlots.Remove(trendSlots[0]);
                bool nHDrop = false;
                headerLock.Add(nHDrop);
                save.headerLock.Add(nHDrop);
                transText.text = "A change in your scale of activity increases your cultural, aesthetic and paradigmatic impacts. You can also have one more global Trendsetter.";
            }
            else
            {
                transText.text = "A change in your scale of activity increases your cultural, aesthetic and paradigmatic impacts.";
            }
            transClick.alpha = 0;
            transText.fontSize = 6;
            transTitle.alpha = 1;
            transSubTitle.alpha = 1;
            transText.alpha = 1;
            transParagraph.alpha = 1;
            transPanel.gameObject.SetActive(true);
            currentTime = 0;
            while (currentTime <= 0.5f)
            {
                newCard.localPosition = Vector2.Lerp(newCard.localPosition, destination, currentTime * cardSpeed);
                currentTime += Time.deltaTime;
                transPanel.alpha = currentTime * 2;
                yield return null;
            }
            click = true;
            transPanel.alpha = 1;
            name1.text = societies[currentSociety].Name;
            Calculation();
            currentTime = 0;
            while (currentTime <= 0.5f && click)
            {
                currentTime += Time.deltaTime;
                transClick.alpha = currentTime * 2;
                yield return null;
            }
            while (click) { yield return null; }
            currentTime = 0;
            while (currentTime <= 0.5f)
            {
                currentTime += Time.deltaTime;
                transClick.alpha = 1 - currentTime * 2;
                transPanel.alpha = 1 - currentTime * 2;
                yield return null;
            }
            transPanel.alpha = 0;
            transClick.alpha = 1;
            transPanel.gameObject.SetActive(false);
        }
        else
        {
            while (currentTime <= 0.5f)
            {
                newCard.localPosition = Vector2.Lerp(newCard.localPosition, destination, currentTime * cardSpeed);
                currentTime += Time.deltaTime;
                yield return null;
            }
        }
        newCard.localPosition = destination;
        for (int i = 0; i < cards[domain].Count; i++)
        {
            if (cards[domain][i] != newCard)
            {
                GameObject.Destroy(cards[domain][i].gameObject);
            }
        }
        cards[domain].Clear();
        drawing = false;
        yield break;
    }
    public void DisplayCard(Artist artist, Transform card, int domain, bool isBad, bool isDrawn)
    {
        if (!interfaceList)
        {
            card.localScale = new Vector2(0.88f, 0.88f);
            CardSelection script = card.GetComponent<CardSelection>();
            script.artist = artist;
            script.outOfDomain = isBad;
            script.domain = domain;
        }
        if (!isBad)
        {
            CardDisplay cardDisplay = new CardDisplay(card);
            cardDisplay.cardDesc.text = artist.Text;
            // Read Card Name
            #region Read Name
            string gender1;
            string gender2;
            if (artist.Gender[0] == "Female")
            {
                gender1 = "<sprite index=3>";
            }
            else if (artist.Gender[0] == "Male")
            {
                gender1 = "<sprite index=4>";
            }
            else
            {
                gender1 = "<sprite index=5>";
            }
            if (artist.Gender.Count > 1)
            {
                if (artist.Gender[1] == "Female")
                {
                    gender2 = "<sprite index=3>";
                }
                else if (artist.Gender[1] == "Male")
                {
                    gender2 = "<sprite index=4>";
                }
                else
                {
                    gender2 = "<sprite index=5>";
                }
            }
            else { gender2 = gender1; }
            if (artist.Name2 == null)
            {
                string birth = artist.Birth.ToString();
                string death = artist.Death.ToString();
                if (birth == "9999")
                {
                    birth = "???";
                }
                if (death == "9999")
                {
                    death = " ";
                }
                cardDisplay.cardText.text = artist.Name + " <size=80%><alpha=#CC>(" + birth + "-" + death + " " + gender1 + ")<size=100%><alpha=#FF>";
            }
            else if (artist.Name3 == null)
            {
                string birth = artist.Birth.ToString();
                string death = artist.Death.ToString();
                string birth2 = artist.Birth2.ToString();
                string death2 = artist.Death2.ToString();
                if (birth == "9999")
                {
                    birth = "???";
                }
                if (death == "9999")
                {
                    death = " ";
                }
                if (birth2 == "9999")
                {
                    birth2 = "???";
                }
                if (death2 == "9999")
                {
                    death2 = " ";
                }
                if (artist.CommonName == null)
                {
                    cardDisplay.cardText.text = artist.Name + " <size=80%><alpha=#CC>(" + birth + "-" + death + " " + gender1 + ")<size=100%><alpha=#FF> & "
                    + artist.Name2 + " <size=80%><alpha=#CC>(" + birth2 + "-" + death2 + " " + gender2 + ")<size=100%><alpha=#FF>";
                }
                else
                {
                    cardDisplay.cardText.text = artist.Name + " <size=80%><alpha=#CC>(" + birth + "-" + death + " " + gender1 + ")<size=100%><alpha=#FF> & "
                    + artist.Name2 + " <size=80%><alpha=#CC>(" + birth2 + "-" + death2 + " " + gender2 + ")<size=100%><alpha=#FF> " + artist.CommonName;
                }
            }
            else
            {
                cardDisplay.cardText.text = artist.Name + ", " + artist.Name2 + " & " + artist.Name3 + " " + artist.CommonName + " <size=80%><alpha=#CC>(1816/18/20-1855/48/49 " + gender1 + ")<size=100%><alpha=#FF>";
            }
            #endregion
            // Read Card Informations
            #region Read Info
            if (!interfaceList)
            {
                artist.Colors = new List<int>();
                artist.Titles = new List<string>();
                artist.Texts = new List<string>();
                artist.Spes = new List<string>();
            }
            for (int x = 0; x < artist.nationRefs.Count; x++)
            {
                GameObject newNation = GameObject.Instantiate(nation, cardDisplay.nationSort);
                if (artist.nationRefs[x] == 8 && artist.Birth > 1900 && artist.Birth < 1980)
                {
                    newNation.GetComponent<Image>().sprite = flags[118];
                }
                else
                {
                    newNation.GetComponent<Image>().sprite = flags[artist.nationRefs[x]];
                }
                NationMouse nm = newNation.GetComponent<NationMouse>();
                nm.active = true;
                nm.nation = artist.Nation[x];
                nm.pos = card;
            }
            for (int i = 0; i < artist.effectRefs.Count; i++)
            {
                Transform newEffect = GameObject.Instantiate(effect, cardDisplay.effectSort).transform;
                TextMeshProUGUI effectTitle = newEffect.Find("Effect Name").GetComponent<TextMeshProUGUI>();
                TextMeshProUGUI effectDescription = newEffect.Find("Effect").GetComponent<TextMeshProUGUI>();
                EffectClick click = newEffect.GetComponent<EffectClick>();
                click.artist = artist;
                click.effect = i;
                Effect effectRef = effectList.Effect[artist.effectRefs[i]];
                effectTitle.text = effectRef.Name;
                artist.Colors.Add(0);
                artist.Titles.Add(effectRef.Name);
                artist.Spes.Add("");
                #region Effects
                if (effectRef.Type == "Current")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[0];
                    artist.Colors[i] = 0;
                    effectDescription.text = "Boosts each Artist linked to " + effectRef.SubType.Remove(effectRef.SubType.Length - 8);
                }
                else if (effectRef.Type == "Domain-defined movement")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[1];
                    artist.Colors[i] = 1;
                    effectDescription.text = "Boosts " + domainShortened[domain] + "'s <sprite index=1> for each minor current";
                    artist.Spes[i] = "<sprite index=1>";
                }
                else if (effectRef.Type == "Area-defined movement")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[2];
                    artist.Colors[i] = 2;
                    effectDescription.text = "Boosts " + effectRef.Area + " for each Artist in " + domainShortened[domain];
                }
                else if (effectRef.Type == "Group")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[3];
                    artist.Colors[i] = 3;
                    if (effectRef.SubType == "Company or School")
                    {
                        effectDescription.text = "Strengthens Social Impact's influence on <sprite index=2>";
                        artist.Spes[i] = "<sprite index=2>";
                    }
                    else if (effectRef.SubType == "Cooperative")
                    {
                        effectDescription.text = "Strengthens Social Impact's influence on <sprite index=1>";
                        artist.Spes[i] = "<sprite index=1>";
                    }
                    else
                    {
                        effectDescription.text = "Strengthens Social Impact's influence on <sprite index=0>";
                        artist.Spes[i] = "<sprite index=0>";
                    }
                }
                else if (effectRef.Type == "Philosophy")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[4];
                    artist.Colors[i] = 4;
                    effectDescription.text = "Boosts <sprite index=0> proportionally to your <sprite index=2>";
                    artist.Spes[i] = "<sprite index=0>";
                }
                else if (effectRef.Type == "Aesthetic")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[5];
                    artist.Colors[i] = 5;
                    effectDescription.text = "Boosts <sprite index=0> proportionally to <sprite index=1> score";
                    artist.Spes[i] = "<sprite index=0>";
                }
                else if (effectRef.Type == "Genre")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[6];
                    artist.Colors[i] = 6;
                    if (effectRef.SubType == "Cultural")
                    {
                        effectDescription.text = "Boosts " + effectRef.Domain[0] + "'s <sprite index=0>";
                        artist.Spes[i] = "<sprite index=0>";
                    }
                    else if (effectRef.SubType == "Aesthetic")
                    {
                        effectDescription.text = "Boosts " + effectRef.Domain[0] + "'s <sprite index=1>";
                        artist.Spes[i] = "<sprite index=1>";
                    }
                    else
                    {
                        effectDescription.text = "Boosts " + effectRef.Domain[0] + "'s <sprite index=2>";
                        artist.Spes[i] = "<sprite index=2>";
                    }
                }
                else if (effectRef.Type == "Activity")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[7];
                    artist.Colors[i] = 7;
                    if (effectRef.Name == "Multitalented Artist")
                    {
                        effectDescription.text = "Slightly boosts " + domainShortened[domain] + "'s <sprite index=0> for each Artist";
                    }
                    else
                    {
                        int domainReference = 0;
                        for (int z = 0; z < artistDomainActive.Count; z++)
                        {
                            if (effectRef.Domain[0] == artistDomainActive[z].name)
                            {
                                domainReference = z;
                            }
                        }
                        int domainReference2 = 0;
                        for (int z = 0; z < artistDomainActive.Count; z++)
                        {
                            if (effectRef.Domain[1] == artistDomainActive[z].name)
                            {
                                domainReference2 = z;
                            }
                        }
                        if (domainReference == domainReference2) { effectDescription.text = "Boosts " + domainShortened[domainReference] + "'s <sprite index=0> for each of its Artists"; }
                        else { effectDescription.text = "Boosts " + domainShortened[domainReference] + "'s <sprite index=0> for each Artist in " + domainShortened[domainReference2]; }
                    }
                    artist.Spes[i] = "<sprite index=0>";
                }
                else if (effectRef.Type == "Contribution")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[8];
                    artist.Colors[i] = 8;
                    if (effectRef.SubType == "Cultural")
                    {
                        effectDescription.text = "Slightly reduces the price of Eras";
                        artist.Spes[i] = "<sprite index=0>";
                    }
                    else
                    {
                        effectDescription.text = "Reduces the price of Directions";
                        artist.Spes[i] = "<sprite index=2>";
                    }
                }
                else if (effectRef.Type == "Discovery")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[9];
                    artist.Colors[i] = 9;
                    effectDescription.text = "Boosts <sprite index=2> within " + domainShortened[domain];
                    artist.Spes[i] = "<sprite index=2>";
                }
                else if (effectRef.Type == "Title")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[10];
                    artist.Colors[i] = 10;
                    effectDescription.text = "Strengthens all Effects within " + domainShortened[domain];
                }
                else if (effectRef.Type == "Prize")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[11];
                    artist.Colors[i] = 11;
                    effectDescription.text = "Boosts <sprite index=0> and <sprite index=1>";
                }
                else if (effectRef.Type == "Range")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[12];
                    artist.Colors[i] = 12;
                    effectDescription.text = "Strengthens this Artist's other Effects";
                }
                else if (effectRef.Type == "Achievement")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[13];
                    artist.Colors[i] = 13;
                    if (effectRef.Name == "Popular Success")
                    {
                        effectDescription.text = "Boosts <sprite index=0> everywhere";
                        artist.Spes[i] = "<sprite index=0>";
                    }
                    else if (effectRef.Name == "Influence")
                    {
                        effectDescription.text = "Boosts <sprite index=1> everywhere";
                        artist.Spes[i] = "<sprite index=1>";
                    }
                    else
                    {
                        effectDescription.text = "Boosts <sprite index=2> everywhere";
                        artist.Spes[i] = "<sprite index=2>";
                    }
                }
                else if (effectRef.Type == "Activism")
                {
                    newEffect.gameObject.GetComponent<Image>().color = effectColors[14];
                    artist.Colors[i] = 14;
                    if (effectRef.SubType == "Feminism")
                    {
                        effectDescription.text = "Improves Social Impact and boosts women Artists";
                    }
                    else if (effectRef.SubType == "Antiracism")
                    {
                        effectDescription.text = "Improves Social Impact and boosts racialized Artists";
                    }
                    else if (effectRef.SubType == "Queer")
                    {
                        effectDescription.text = "Improves Social Impact and boosts queer Artists";
                    }
                    else
                    {
                        effectDescription.text = "Improves Social Impact";
                    }
                }

                #endregion
                artist.Texts.Add(effectDescription.text);
            }
            if (artist.Tags[0] == "None")
            {
                cardDisplay.cardTags.text = "//";
            }
            else if (artist.Tags.Count == 1)
            {
                cardDisplay.cardTags.text = artist.Tags[0];
            }
            else if (artist.Tags.Count == 2)
            {
                cardDisplay.cardTags.text = artist.Tags[0] + ", " + artist.Tags[1];
            }
            else
            {
                cardDisplay.cardTags.text = artist.Tags[0] + ", " + artist.Tags[1] + ", " + artist.Tags[2];
            }
            if (artist.Currents[0] == "None")
            {
                cardDisplay.cardCurrents.text = "//";
            }
            else if (artist.Currents.Count == 1)
            {
                cardDisplay.cardCurrents.text = artist.Currents[0];
            }
            else if (artist.Currents.Count == 2)
            {
                cardDisplay.cardCurrents.text = artist.Currents[0] +
                    ", " + artist.Currents[1];
            }
            else if (artist.Currents.Count == 3)
            {
                cardDisplay.cardCurrents.text = artist.Currents[0] +
                    ", " + artist.Currents[1] +
                    ", " + artist.Currents[2];
            }
            else if (artist.Currents.Count == 4)
            {
                cardDisplay.cardCurrents.text = artist.Currents[0] +
                    ", " + artist.Currents[1] +
                    ", " + artist.Currents[2] +
                    ", " + artist.Currents[3];
            }
            else if (artist.Currents.Count == 5)
            {
                cardDisplay.cardCurrents.text = artist.Currents[0] +
                    ", " + artist.Currents[1] +
                    ", " + artist.Currents[2] +
                    ", " + artist.Currents[3] +
                    ", " + artist.Currents[4];
            }

            #endregion
            if (!isDrawn)
            {
                cardDisplay.script.domain = domain;
                cardDisplay.script.artist = artist;
                cardDisplay.script.outOfDomain = false;
                cardDisplay.script.price = Price(domain, artist.Effects.Count);
                cardDisplay.cardPrice.text = "Price: " + RoundNumber(cardDisplay.script.price, true) + " <sprite index=1>";
                bool OK = true;
                if (artist.currentRefs != null)
                {
                    for (int z = 0; z < artist.currentRefs.Count; z++)
                    {
                        if (!currents[artist.currentRefs[z]].available)
                        {
                            OK = false;
                        }
                    }
                }
                if (!OK)
                {
                    artist.Locked = true;
                }
                else
                {
                    artist.Locked = false;
                }
            }
        }
        else
        {
            string CWs = "Content Warning:\n<size=4><color=black>";
            for (int i = 0; i < artist.CW.Length; i++)
            {
                if (i > 0)
                {
                    CWs += ", ";
                }
                CWs += artist.CW[i];
            }
            CWs += "</size></color>\n\nClick to show the description.";
            card.Find("Panel").GetChild(1).GetComponent<TextMeshProUGUI>().text = CWs;
            card.Find("Text").GetComponent<TextMeshProUGUI>().text = artist.Text;
            card.Find("Name").GetComponent<TextMeshProUGUI>().text = artist.Name;
            if (!isDrawn)
            {
                artist.ProdC = card.Find("ProductionC").GetComponent<TextMeshProUGUI>();
                float price = Price(domain, 4);
                CardSelection mouse = card.GetComponent<CardSelection>();
                mouse.outOfDomain = true;
                mouse.price = price;
                artist.ProdC.text = "Cost of banishment: " + RoundNumber(price, true) + "<sprite index=1>";
                artist.Locked = false;
            }
        }
    }
    IEnumerator SpreadCards(int drawNumber, List<Transform> cardsDrawn, bool gameOn)
    {
        int height = -80;
        int horizontalLeft = 65 * (drawNumber - 1);
        int horizontalRight = 130;
        float actual;
        if (gameOn)
        {
            //horizontalLeft = 45 * (drawNumber - 1);
            // horizontalRight = 95;
            horizontalLeft = 53 * (drawNumber - 1);
            horizontalRight = 106;
            height = -60;
            blur.blocksRaycasts = true;
        }
        float currentTime = 0;
        float basisBlur = blur.alpha;
        while (currentTime <= 0.5f)
        {
            currentTime += Time.deltaTime;
            if (gameOn)
            {
                blur.alpha = Mathf.Lerp(basisBlur, 1, currentTime);
            }
            for (int i = 0; i < drawNumber; i++)
            {
                actual = Mathf.Lerp(cardsDrawn[i].localPosition.y, height, currentTime * 1.5f);
                cardsDrawn[i].localPosition = new Vector2(cardsDrawn[i].localPosition.x, actual);
            }
            yield return null;
        }
        for (int i = 0; i < drawNumber; i++)
        {
            cardsDrawn[i].localPosition = new Vector2(cardsDrawn[i].localPosition.x, height);
        }
        if (gameOn)
        {
            blur.alpha = 1;
        }
        if (drawNumber > 1)
        {
            PlaySound(clips[2], 1f, 2);
            currentTime = 0;
            while (currentTime <= 0.5f)
            {
                currentTime += Time.deltaTime;
                for (int i = 0; i < drawNumber; i++)
                {
                    actual = Mathf.Lerp(cardsDrawn[i].localPosition.x, -horizontalLeft + (horizontalRight * i), currentTime * 1.5f);
                    cardsDrawn[i].localPosition = new Vector2(actual, height);
                }
                yield return null;
            }
            for (int i = 0; i < drawNumber; i++)
            {
                cardsDrawn[i].localPosition = new Vector2(-horizontalLeft + (horizontalRight * i), height);
            }
        }
        drawing = false;
        if (gameOn)
        {
            drawOn = true;
        }
        for (int i = 0; i < drawNumber; i++)
        {
            CardSelection script = cardsDrawn[i].GetComponent<CardSelection>();
            script.basisPos = cardsDrawn[i].localPosition;
            script.newPos = script.basisPos;
            script.worldPos = cardsDrawn[i].position;
        }
        yield break;
    }
    IEnumerator HideCards(Transform chosenCard, Transform chosenCard2, List<Transform> cards)
    {
        float currentTime = 0;
        float actual;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            for (int i = 0; i < cards.Count; i++)
            {
                if (cards[i] == chosenCard || cards[i] == chosenCard2)
                {
                    actual = Mathf.Lerp(cards[i].localPosition.y, -250, currentTime * cardSpeed);
                    cards[i].localPosition = new Vector2(cards[i].localPosition.x, actual);
                }
                else
                {
                    actual = Mathf.Lerp(cards[i].localPosition.y, 250, currentTime * cardSpeed);
                    cards[i].localPosition = new Vector2(cards[i].localPosition.x, actual);
                }
            }
            yield return null;
        }
        yield break;
    }
    void AddCard(Artist artist, Transform card, int domain, bool isBad)
    {
        card.localScale = new Vector2(1f, 1f);
        card.GetComponent<CardSelection>().enabled = false;
        card.GetComponent<EventTrigger>().enabled = false;
        artist.Card = card;
        GameObject detectorObject = card.Find("MouseDetector").gameObject;
        detectorObject.SetActive(true);
        MouseDetector detector = detectorObject.GetComponent<MouseDetector>();
        detector.nextCard = card.Find("NextCard").transform;
        detector.currentCard = card;
        detector.mouseOver = false;
        if (!isBad)
        {
            domainShops[domain].availableCards = artistDomainAvailable[domain].Artist.Count;
            if (domainShops[domain].availableCards == 0)
            {
                domainShops[domain].cardShadow.SetActive(true);
            }
            artist.ProdC = card.Find("ProductionC").GetComponent<TextMeshProUGUI>();
            artist.ProdA = card.Find("ProductionA").GetComponent<TextMeshProUGUI>();
            artist.ProdP = card.Find("ProductionP").GetComponent<TextMeshProUGUI>();
            artist.ProdC.gameObject.SetActive(true);
            artist.ProdA.gameObject.SetActive(true);
            artist.ProdP.gameObject.SetActive(true);
            artist.ProdC.fontSize = 4;
            artist.ProdA.fontSize = 4;
            artist.ProdP.fontSize = 4;
            // Update Stats
            #region Stats
            available++;
            Transform nationSort = card.Find("NationSort");
            for (int i = 0; i < artist.nationRefs.Count; i++)
            {
                bonusLists[0][artist.nationRefs[i]].amount++;
                nationSort.GetChild(i).GetComponent<NationMouse>().cardActive = true;
            }
            for (int i = 0; i < artist.tagRefs.Count; i++)
            {
                tags[artist.tagRefs[i]].amount++;
            }
            for (int i = 0; i < artist.currentRefs.Count; i++)
            {
                currents[artist.currentRefs[i]].amount++;
            }
            if (artist.Gender.Count > 1)
            {
                men++;
                women++;
            }
            else if (artist.Gender[0] == "Female")
            {
                women++;
            }
            else if (artist.Gender[0] == "Non-Binary")
            {
                nonBinary++;
            }
            else
            {
                men++;
            }

            #endregion
            // Create Miniature
            #region Miniature
            detector.box = domainShops[domain].boxMouse;
            detector.cardColor = domainColors[domain];
            detector.domain = domain;
            detector.artist = artist;
            string listName = artist.Name;
            if (artist.Name2 != null)
            {
                if (artist.Name3 == null)
                {
                    listName = listName + " & " + artist.Name2;
                }
                else
                {
                    listName = listName + ", " + artist.Name2;
                }
            }
            if (artist.Name3 != null)
            {
                listName = listName + " & " + artist.Name3;
            }
            if (artist.CommonName != null)
            {
                listName = listName + " " + artist.CommonName;
            }
            detector.artistName = listName;
            detector.effectName = new string[artist.Effects.Count];
            detector.effectColor = new Color[artist.Effects.Count];
            List<string> effectSpe = new List<string>();
            for (int i = 0; i < artist.Effects.Count; i++)
            {
                Effect effectRef = effectList.Effect[artist.effectRefs[i]];
                detector.effectName[i] = effectRef.Name;
                detector.effectColor[i] = effectColors[artist.Colors[i]];
                for (int y = 1; y < bonusLists.Length; y++)
                {
                    if (effectRef.Type == bonusTypes[y])
                    {
                        for (int z = 1; z < bonusLists[y].Count; z++)
                        {
                            if (effectRef.Name == bonusLists[y][z].name)
                            {
                                if (bonusLists[y][z].amount == 0)
                                {
                                    bonusStats[y]++;
                                    if (bonusStats[y] >= 10 && bonuses[y] == null)
                                    {
                                        DisplayBonus(y, 0);
                                        StartCoroutine(ItemsFade());
                                    }
                                    bonusLists[y][z].amount++;
                                }
                            }
                        }
                    }
                }
                effectSpe.Add("");
                if (effectRef.Type == "Title")
                {
                    domainShops[domain].titleCount++;
                }
                if (effectRef.Type == "Contribution")
                {
                    if (effectRef.SubType == "Cultural")
                    {
                        effectSpe[i] = "<sprite index=0>";
                    }
                    else
                    {
                        effectSpe[i] = "<sprite index=2>";
                    }
                }
                else if (effectRef.Type == "Group")
                {
                    if (effectRef.SubType == "Company or School")
                    {
                        effectSpe[i] = "<sprite index=2>";
                    }
                    else if (effectRef.SubType == "Cooperative")
                    {
                        effectSpe[i] = "<sprite index=1>";
                    }
                    else
                    {
                        effectSpe[i] = "<sprite index=0>";
                    }
                }
                else if (effectRef.Type == "Genre")
                {
                    if (effectRef.SubType == "Cultural")
                    {
                        effectSpe[i] = "<sprite index=0>";
                    }
                    else if (effectRef.SubType == "Aesthetic")
                    {
                        effectSpe[i] = "<sprite index=1>";
                    }
                    else
                    {
                        effectSpe[i] = "<sprite index=2>";
                    }
                }
                else if (effectRef.Type == "Achievement")
                {
                    if (effectRef.Name == "Popular Success")
                    {
                        effectSpe[i] = "<sprite index=0>";
                    }
                    else if (effectRef.Name == "Influence")
                    {
                        effectSpe[i] = "<sprite index=1>";
                    }
                    else
                    {
                        effectSpe[i] = "<sprite index=2>";
                    }
                }
            }

            #endregion
            // Create Star
            #region Star
            stars.Add(GameObject.Instantiate(star, Background).GetComponent<RectTransform>());
            starsAlpha.Add(stars[stars.Count - 1].GetComponent<CanvasGroup>());
            float scale = UnityEngine.Random.Range(0.5f, 1.5f);
            stars[stars.Count - 1].localScale = new Vector2(scale, scale);
            stars[stars.Count - 1].localPosition = new Vector2(UnityEngine.Random.Range(horizontalLimit.x, horizontalLimit.y), UnityEngine.Random.Range(verticalLimit.x, verticalLimit.y));
            starsAlpha[stars.Count - 1].alpha = 0;
            #endregion
            // Prepare Line
            #region Line
            string Birth = artist.Birth.ToString();
            string Death = artist.Death.ToString();
            if (Birth == "9999")
            {
                Birth = "?";
            }
            if (Death == "9999")
            {
                Death = "  ";
            }
            string listEra;
            if (artist.Birth3 != 0)
            {
                listEra = "1816/20-1849/55";
            }
            else if (artist.Birth2 != 0)
            {
                string Birth2 = artist.Birth2.ToString().Remove(0, 2);
                string Death2 = artist.Death2.ToString().Remove(0, 2);
                if (Birth2 == "99")
                {
                    Birth2 = "?";
                }
                if (Death2 == "99")
                {
                    Death2 = " ";
                }
                listEra = Birth + "/" + Birth2 + "-" + Death + "/" + Death2;
            }
            else
            {
                listEra = Birth + "-" + Death;
            }
            simplifiedArtists.Add(new ArtistSimple(artist, listName, listEra, domain));
            #endregion
            domainPower[domain] = DomainPower(domain);
            totalPower = TotalPower();
            //print("Domain: " + domainPower[domain] + " / Total: " + totalPower);
        }
        else
        {
            rejected++;
            card.Find("ProductionC").gameObject.SetActive(false);
            detector.outOfDomain = true;
            detector.SwitchSize();
        }
        if (gameOn)
        {
            Calculation();
            //print("with " + artist.Name + " / C: " + productionC + " / A: " + productionA + " / P:" + productionP);
        }
    }

    #endregion
    // Functions Shop
    #region Actions: Shop
    void DisplayItem(int cur)
    {
        MajorCurrent major = majorCurrents[cur];
        GameObject newItem = (GameObject)Instantiate(storeItem, storeFront);
        availableItems[cur] = new Item(newItem.transform, major.Name, "Major Current", cur, currentEras[cur]);
        save.itemsAvailable[cur] = true;
        newItems.Add(newItem.GetComponent<CanvasGroup>());
        Item item = availableItems[cur];
        newItem.GetComponent<CanvasGroup>().alpha = 0;
        ShopClick shop = newItem.GetComponent<ShopClick>();
        shop.current = item;
        shop.shadow = newItem.transform.Find("Filter").gameObject;
        item.Icon = currentIcons[cur];
        newItem.transform.GetChild(0).GetComponent<Image>().sprite = item.Icon;
        item.Filter = newItem.transform.Find("Filter").gameObject;
        item.EraIcon = newItem.transform.Find("Era").gameObject;
        ItemCurrentsCount(item, major);
        item.Total = major.Total;
        item.Price = ItemPrice(item, cur);
        major.Unlocked = true;
        shopScroll.AddItem();
    }
    void ItemCurrentsCount(Item item, MajorCurrent major)
    {
        item.SubGenders = new List<string>();
        if (major.SubCurrents != null)
        {
            for (int x = 0; x < major.SubCurrents.Count; x++)
            {
                item.SubGenders.Add(currents[major.SubCurrents[x]].name);
            }
        }
    }
    IEnumerator ItemsFade()
    {
        float currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            for (int i = 0; i < newItems.Count; i++)
            {
                newItems[i].alpha = currentTime;
            }
            yield return null;
        }
        for (int i = 0; i < newItems.Count; i++)
        {
            newItems[i].alpha = 1;
        }
        newItems.Clear();
        yield break;
    }
    public void BuyItem(Item item, int reference)
    {
        HidePanel();
        item.Level++;
        majorCurrents[reference].Level = item.Level;
        save.items[reference] = item.Level;
        if (item.Level < 5)
        {
            item.EraIcon.SetActive(true);
            item.EraIcon.GetComponent<Image>().sprite = shopEras[item.Level - 1];
        }
        if (item.Level == 1 || item.Card == null)
        {
            itemNumber++;
            cardBoxItems.parent.GetComponent<BoxMouse>().AddItem(itemNumber);
            drawing = true;
            for (int i = 0; i < currents.Count; i++)
            {
                if (item.Name == currents[i].name)
                {
                    currents[i].available = true;
                }
                if (item.SubGenders != null)
                {
                    for (int y = 0; y < item.SubGenders.Count; y++)
                    {
                        if (item.SubGenders[y] == currents[i].name)
                        {
                            currents[i].available = true;
                        }
                    }
                }
            }
            for (int i = 0; i < artistDomainActive.Count; i++)
            {
                if (cards[i] != null)
                {
                    for (int y = 0; y < cards[i].Count; y++)
                    {
                        bool available = true;
                        Artist artist = cards[i][y].GetComponent<CardSelection>().artist;
                        if (artist.currentRefs != null)
                        {
                            for (int z = 0; z < artist.currentRefs.Count; z++)
                            {
                                if (!currents[artist.currentRefs[z]].available)
                                {
                                    available = false;
                                }
                            }
                        }
                        if (!available)
                        {
                            artist.Locked = true;
                        }
                        else
                        {
                            artist.Locked = false;
                        }
                    }
                }
            }
            AddItem(item);
        }
        else
        {
            if (item.Level < 4)
            {
                item.Card.Find("Era").GetComponent<TextMeshProUGUI>().text = item.Level.ToString();
            }
            else
            {
                item.Card.Find("Era").GetComponent<TextMeshProUGUI>().text = "Max";
                item.Price = 0;
            }
        }
        item.Price = ItemPrice(item, reference);
        if (gameOn)
        {
            Calculation();
            drawing = false;
        }
    }
    void AddItem(Item item)
    {
        activeItems.Add(item);
        Transform parent = cardBoxItems;
        if (activeItems.Count > 1)
        {
            Transform cardParent = activeItems[activeItems.Count - 2].Card;
            ItMouseDetector mouse = cardParent.Find("MouseDetector").GetComponent<ItMouseDetector>();
            if (!mouse.small)
            {
                mouse.background.gameObject.SetActive(true);
            }
            parent = cardParent.Find("NextCard").transform;
        }
        item.SubRefs = new List<int>();
        item.SubTexts = new List<TextMeshProUGUI>();
        if (item.SubGenders.Count > 1)
        {
            item.Card = Instantiate(bigCurrent, parent).transform;
            RectTransform subList = item.Card.transform.Find("SubCurrents").GetComponent<RectTransform>();
            for (int i = 0; i < item.SubGenders.Count; i++)
            {
                GameObject sub = Instantiate(SubCurrent, subList);
                int count = 0;
                for (int y = 0; y < currents.Count; y++)
                {
                    if (currents[y].name == item.SubGenders[i])
                    {
                        item.SubRefs.Add(y);
                        count = currents[y].amount;
                    }
                }
                item.SubTexts.Add(sub.GetComponent<TextMeshProUGUI>());
                item.SubTexts[i].text = item.SubGenders[i] + ": " + count + " artists";
            }
        }
        else
        {
            item.Card = Instantiate(smallCurrent, parent).transform;
            int totalArtists = 0;
            for (int y = 0; y < currents.Count; y++)
            {
                if (currents[y].name == item.Name)
                {
                    item.SubRefs.Add(y);
                    totalArtists = currents[y].total;
                }
            }
            item.SubTexts.Add(item.Card.Find("Amount").GetComponent<TextMeshProUGUI>());
            item.SubTexts[0].text = "Artists: " + item.Total;
            item.Card.Find("Total").GetComponent<TextMeshProUGUI>().text = "Maximum: " + totalArtists;
        }
        item.Card.Find("Icon").GetComponent<Image>().sprite = item.Icon;
        item.Card.Find("Name").GetComponent<TextMeshProUGUI>().text = item.Name;
        if (item.Level < 4)
        {
            item.Card.Find("Era").GetComponent<TextMeshProUGUI>().text = item.Level.ToString();
        }
        else
        {
            item.Card.Find("Era").GetComponent<TextMeshProUGUI>().text = "Max";
        }
        item.Card.localScale = Vector2.one;
        Vector2 destination = Vector2.zero;
        if (item == activeItems[0])
        {
            if (item.SubGenders.Count > 1)
            {
                destination = new Vector2(0, 483.8f);
            }
            else
            {
                destination = new Vector2(0, 510);
            }
        }
        else if (item.SubGenders.Count > 1)
        {
            destination = new Vector2(0, -26.2f);
        }
        item.Card.GetComponent<RectTransform>().localPosition = destination;
        ItMouseDetector detector = item.Card.Find("MouseDetector").GetComponent<ItMouseDetector>();
        detector.nextCard = item.Card.Find("NextCard").transform;
        detector.box = cardBoxItems.parent.GetComponent<BoxMouse>();
        if (item.SubGenders.Count < 2)
        {
            detector.Small();
        }
        else
        {
            detector.background = item.Card.Find("NextCard").Find("Background").GetComponent<RectTransform>();
            detector.background.gameObject.SetActive(false);
            detector.PointerOut();
        }
    }
    void DisplayBonus(int bonusRef, int level)
    {
        string name = bonusNames[bonusRef];
        string desc = bonusDescriptions[bonusRef];
        Sprite icon = currentIcons[bonusRef + 13];
        if (save.bonus[bonusRef] != 0)
        {
            level = save.bonus[bonusRef];
        }
        bonuses[bonusRef] = new Bonus(icon, name, level, desc, bonusRef);
        if (level < 4)
        {
            bonuses[bonusRef].Price = BonusPrice(bonusRef);
        }
        else
        {
            bonuses[bonusRef].Price = 0;
        }
        bonuses[bonusRef].Item = (GameObject)Instantiate(storeItem, storeFront);
        GameObject newItem = bonuses[bonusRef].Item;
        newItems.Add(newItem.GetComponent<CanvasGroup>());
        bonuses[bonusRef].Item.GetComponent<CanvasGroup>().alpha = 0;
        ShopClick shop = newItem.GetComponent<ShopClick>();
        shop.currentB = bonuses[bonusRef];
        shop.isBonus = true;
        shop.shadow = newItem.transform.Find("Filter").gameObject;
        newItem.transform.GetChild(0).GetComponent<Image>().sprite = icon;
        bonuses[bonusRef].Filter = newItem.transform.Find("Filter").gameObject;
        bonuses[bonusRef].EraIcon = newItem.transform.Find("Era").gameObject;
        if (level > 0)
        {
            itemNumber++;
            bonuses[bonusRef].EraIcon.SetActive(true);
            bonuses[bonusRef].EraIcon.GetComponent<Image>().sprite = shopEras[bonuses[bonusRef].Level - 1];
        }
        shopScroll.AddItem();
    }
    public void BuyBonus(int bonusRef)
    {
        HidePanel();
        bonuses[bonusRef].Level++;
        save.bonus[bonusRef]++;
        bonuses[bonusRef].EraIcon.SetActive(true);
        bonuses[bonusRef].EraIcon.GetComponent<Image>().sprite = shopEras[bonuses[bonusRef].Level - 1];
        if (bonuses[bonusRef].Level < 4)
        {
            bonuses[bonusRef].Price = BonusPrice(bonusRef);
        }
        else
        {
            bonuses[bonusRef].Price = 0;
        }
        if (gameOn)
        {
            Calculation();
        }
    }

    #endregion
    // Functions Trendsetters
    #region Actions: Trendsetters

    public void DomainDropOn(int domain)
    {
        if (domain == cardDomain)
        {
            domainDrop = domain;
            DomainDropAvailable = true;
        }
    }
    public void DomainDropOff()
    {
        DomainDropAvailable = false;
    }
    public void HeaderDropOn(int header)
    {
        headerDrop = header;
        HeaderDropAvailable = true;
    }
    public void HeaderDropOff()
    {
        HeaderDropAvailable = false;
    }
    public void HeaderChange(int header)
    {
        headerLock[header] = true;
        save.headerLock[header] = true;
    }
    public void DomainChange(int domain)
    {
        domainLock[domain] = true;
        save.domainLock[domain] = true;
    }

    #endregion
    // Functions Routine
    #region Routine

    void OnApplicationFocus(bool hasFocus)
    {
        focus = hasFocus;
    }
    IEnumerator EndLoad()
    {
        if (loading)
        {
            while (Time.realtimeSinceStartup < 5) { yield return null; }
            while (!gameOn) { yield return null; }
        }
        else
        {
            while (Time.realtimeSinceStartup < 7) { yield return null; }
        }
        CanvasGroup load = loadingScreen.GetComponent<CanvasGroup>();
        float currentTime = 0;
        while (currentTime < 2)
        {
            currentTime += Time.deltaTime;
            load.alpha = 1 - currentTime * 0.5f;
            yield return null;
        }
        loadingScreen.SetActive(false);
        yield break;
    }
    public void ShowImpacts()
    {
        PlaySound(clips[10], 0.25f, 1);
        StartCoroutine(ImpactsFade());
    }
    IEnumerator ImpactsFade()
    {
        impacts.gameObject.SetActive(true);
        float basisAlpha = impactsButton.alpha;
        float currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            impacts.alpha = currentTime;
            impactsButton.alpha = Mathf.Lerp(basisAlpha, 0, currentTime);
            yield return null;
        }
        impacts.alpha = 1;
        impactsButton.gameObject.SetActive(false);
        yield break;
    }
    public void EraPriceDisplay(DomainShop shop)
    {
        if (shop.era == 5)
        {
            bool max = true;
            for (int i = 0; i < domainShops.Length; i++)
            {
                if (domainShops[i].era < 5)
                {
                    max = false;
                }
            }
            if (max)
            {
                endAvailable = true;
                for (int i = 0; i < domainShops.Length; i++)
                {
                    domainShops[i].eraText.text = "Post-time";
                    domainShops[i].price = 1000000000000000;
                    domainShops[i].eraText2.text = RoundNumber(1000000000000000, true);
                    domainShops[i].cardShadow.SetActive(false);
                }
            }
            else
            {
                shop.eraText.text = "Max";
                shop.eraText2.text = "???";
                shop.eraShadow.SetActive(true);
            }
        }
        else
        {
            shop.eraText.text = shop.era.ToString();
            shop.eraText2.text = RoundNumber(shop.price, true);
        }
    }
    IEnumerator GaugeChange(int type)
    {
        if (type == 1)
        {
            PlaySound(melodies[1], 1f, 3);
            gaugeTitle.text = "Social Impact is Up!";
            gaugeText.text = "Your Social Impact gauge just passed a new level!\nGlobal output is multiplied.";
        }
        else if (type == 2)
        {
            PlaySound(melodies[4], 1.5f, 3);
            gaugeTitle.text = "Social Impact is Down.";
            gaugeText.text = "Your Social Impact gauge just lost a level!\nGlobal output is divided.";
        }
        else
        {
            PlaySound(melodies[4], 1.5f, 3);
            gaugeTitle.text = "Social Impact malus.";
            gaugeText.text = "You've turned a blind eye on the crimes of an artist.";
        }
        float currentTime = 0;
        while (currentTime <= 1f)
        {
            currentTime += Time.deltaTime;
            statsDisplay.alpha = 1 - currentTime;
            gaugeDisplay.alpha = currentTime;
            yield return null;
        }
        statsDisplay.alpha = 0;
        gaugeDisplay.alpha = 1;
        click = true;
        while (click) { yield return null; }
        currentTime = 0;
        while (currentTime <= 0.5f)
        {
            currentTime += Time.deltaTime;
            gaugeDisplay.alpha = 1 - currentTime * 2;
            statsDisplay.alpha = currentTime * 2;
            yield return null;
        }
        gaugeDisplay.alpha = 0;
        statsDisplay.alpha = 1;
        yield break;
    }
    public void EraChange(int domain, DomainShop shop)
    {
        erasPassed++;
        statisticTexts[2].text = "<b>Eras:</b> " + erasPassed + "%";
        for (int i = 0; i < artistDomainLocked[domain].Artist.Count; i++)
        {
            if (artistDomainLocked[domain].Artist[i].Era == shop.era)
            {
                artistDomainAvailable[domain].Artist.Add(artistDomainLocked[domain].Artist[i]);
                artistDomainLocked[domain].Artist.Remove(artistDomainLocked[domain].Artist[i]);
                i--;
            }
        }
        for (int i = 0; i < shamefulInfluenceLocked.Artist.Count; i++)
        {
            if (shamefulInfluenceLocked.Artist[i].Domain == artistDomainAvailable[domain].name)
            {
                if (shamefulInfluenceLocked.Artist[i].Era <= shop.era)
                {
                    shamefulInfluenceAvailable[domain].Artist.Add(shamefulInfluenceLocked.Artist[i]);
                    shamefulInfluenceLocked.Artist.Remove(shamefulInfluenceLocked.Artist[i]);
                }
            }
        }
        EraPriceDisplay(domainShops[domain]);
        scrollTexts[domain + 1].text = artistDomainAvailable[domain].name;
        scrollTexts[domain + 1].color = domainColors[domain];
        if (domain > 1 && shop.era < 2 && domainScroll < 20)
        {
            domainScroll++;
        }
        if (domainScroll > 18)
        {
            scrollTexts[21].text = "--- ";
        }
        for (int i = 0; i < domainShops.Length; i++)
        {
            save.eraList[i] = domainShops[i].era;
        }
        domainShops[domain].cardShadow.SetActive(false);
    }
    IEnumerator NewsChange()
    {
        int newRef = UnityEngine.Random.Range(0, newsListAvailable.News.Count);
        if (beginning)
        {
            newRef = 0;
        }
        else
        {
            if (newsPlay >= newsListAvailable.News.Count)
            {
                newsPlay = 0;
                for (int i = 0; i < newsListAvailable.News.Count; i++)
                {
                    newsListAvailable.News[i].played = false;
                }
            }
            while (newsListAvailable.News[newRef].played == true)
            {
                newRef = UnityEngine.Random.Range(0, newsListAvailable.News.Count);
                yield return null;
            }
        }
        beginning = false;
        newsRef = newRef;
        newsListAvailable.News[newsRef].played = true;
        newsPlay++;
        string text = newsListAvailable.News[newsRef].Text;
        float timeFade = 0;
        float origin = transformNews.localPosition.y;
        while (timeFade < 1)
        {
            timeFade += Time.deltaTime;
            transformNews.localPosition = new Vector2(newsText.transform.localPosition.x, Mathf.Lerp(origin, origin - 2, timeFade));
            newsText.color = new Color(newsText.color.r, newsText.color.g, newsText.color.b, Mathf.Lerp(1, 0, timeFade));
            yield return null;
        }
        newsText.text = text;
        timeFade = 0;
        while (timeFade < 1)
        {
            timeFade += Time.deltaTime;
            transformNews.localPosition = new Vector2(newsText.transform.localPosition.x, Mathf.Lerp(origin + 2, origin, timeFade));
            newsText.color = new Color(newsText.color.r, newsText.color.g, newsText.color.b, Mathf.Lerp(0, 1, timeFade));
            yield return null;
        }
        yield break;
    }
    public void PlaySound(AudioClip clip, float gain, int canal)
    {
        if (audioOn)
        {
            AudioSource source;
            if (canal == 1)
            {
                source = sound;
            }
            else if (canal == 2)
            {
                source = sound2;
            }
            else
            {
                source = sound3;
            }
            source.clip = clip;
            source.volume = volume * gain;
            if (clip == clips[1])
            {
                if (source.time > 0.05f || source.time == 0)
                {
                    source.Play();
                    source.volume = 0.1f;
                    StartCoroutine(SoundFade(source, clip, volume * gain));
                }
            }
            else
            {
                source.Play();
            }
        }
    }
    IEnumerator SoundFade(AudioSource source, AudioClip clip, float volume)
    {
        while (source.isPlaying && source.volume < volume)
        {
            source.volume += Time.deltaTime / clip.length;
            yield return null;
        }
    }
    void HidePanel()
    {
        panel.gameObject.SetActive(false);
        panelTitle.fontSize = 6.5f;
        //panelText.fontSize = 5;
        panelTitle.text = "";
        panelText.text = "";
        panelDisplay = false;
    }
    void DisplayStats()
    {
        productionDisplayC.text = RoundNumber(productionC, false) + " / sec";
        productionDisplayA.text = RoundNumber(productionA, false) + " / sec";
        productionDisplayP.text = RoundNumber(productionP, false) + " / sec";
        statisticTexts[0].text = "<color=#E0CBAE><b>Artists:</b> </color>" + available;
        statisticTexts[1].text = women + " <sprite index=6>/ " + men + " <sprite index=7>/ " + nonBinary + " <sprite index=8>";
        statisticTexts[2].text = "<color=#E0CBAE><b>Eras:</b> </color>" + erasPassed + " %";
        statisticTexts[3].text = "<color=#E0CBAE><b>Gauge:</b> </color>" + Math.Round(globalMultiplicator, 1);
        statisticTexts[4].text = "<color=#E0CBAE><b>Currents:</b> </color>" + activeItems.Count;

        statisticTexts[5].text = "<color=#E0CBAE><b>Nations:</b> </color>" + bonusStats[0];
        statisticTexts[6].text = "<color=#E0CBAE><b>Genres:</b> </color>" + bonusStats[1];
        statisticTexts[7].text = "<color=#E0CBAE><b>Groups:</b> </color>" + bonusStats[2];
        statisticTexts[8].text = "<color=#E0CBAE><b>Movements:</b> </color>" + bonusStats[3];
        statisticTexts[9].text = "<color=#E0CBAE><b>Styles:</b> </color>" + bonusStats[4];
    }
    public string DomainText(int domain)
    {
        string domainText = "Drag and drop an Artist here to have their Effects enhanced for each other enrolled Artist in the Domain.\n\n<align=center><color=#AF8B5A><b>Current Domain Production:\n</b></color> " + RoundNumber(domainProductionC[domain], true) + " <sprite index=0> // " + RoundNumber(domainProductionA[domain], true) + " <sprite index=1> // " + RoundNumber(domainProductionP[domain], true) + " <sprite index=2>";
        return domainText;
    }
    public IEnumerator ClickIcon(int impact)
    {
        PlaySound(clips[14], 0.5f, 2);
        Transform bonusDisplay = GameObject.Instantiate(clickBonus, canvasTR).transform;
        if (impact == 0)
        {
            double newBonus = 1;
            stockC += newBonus;
            bonusDisplay.GetComponent<TextMeshProUGUI>().text = "+ 1";
        }
        else if (impact == 1)
        {
            double newBonus = 1;
            stockA += newBonus;
            bonusDisplay.GetComponent<TextMeshProUGUI>().text = "+ 1";
        }
        else
        {
            double newBonus = 1;
            stockP += newBonus;
            bonusDisplay.GetComponent<TextMeshProUGUI>().text = "+ 1";
        }
        bonusDisplay.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 1000);
        CanvasGroup alpha = bonusDisplay.GetComponent<CanvasGroup>();
        float X = bonusDisplay.position.x;
        float basis = bonusDisplay.position.y;
        float destination = bonusDisplay.position.y + 1;
        float Y;
        float currentTimeClick = 0;
        float scale;
        while (currentTimeClick < 1)
        {
            currentTimeClick += Time.deltaTime;
            alpha.alpha = Mathf.Lerp(1, 0, currentTimeClick * 1.5f);
            Y = Mathf.Lerp(basis, destination, currentTimeClick);
            scale = Mathf.Lerp(1, 2, currentTimeClick);
            bonusDisplay.position = new Vector3(X, Y, 1000);
            bonusDisplay.localScale = new Vector2(scale, scale);
            yield return null;
        }
        GameObject.Destroy(bonusDisplay.gameObject);
        yield break;
    }
    public IEnumerator EffectTest(Artist artist, int effect)
    {
        int reference = artist.effectRefs[effect];
        int domain = 0;
        if (effectList.Effect[reference].Type == "Title")
        {
            for (int i = 0; i < domainShops.Length; i++)
            {
                if (domainShops[i].name == artist.Domain)
                {
                    domain = i;
                }
            }
            domainShops[domain].titleCount--;
        }
        else
        {
            artist.effectRefs[effect] = 0;
        }
        Calculation();
        double withoutC = productionC;
        double withoutA = productionA;
        double withoutP = productionP;
        yield return new WaitForSeconds(0.5f);
        if (effectList.Effect[reference].Type == "Title")
        {
            domainShops[domain].titleCount++;
        }
        else
        {
            artist.effectRefs[effect] = reference;
        }
        Calculation();
        print("with " + effectList.Effect[reference].Name + " / C: " + Mathf.Round((float)(productionC - withoutC)) + " / A: " + Mathf.Round((float)(productionA - withoutA)) + " / P:" + Mathf.Round((float)(productionP - withoutP)));
        yield break;
    }

    #endregion
    // Functions Mathematics
    #region Mathematics

    float DomainPower(int domain)
    {
        return Mathf.Clamp((artistDomainActive[domain].Artist.Count * 0.3f) + 3f, 3f, 18f);
    }
    float TotalPower()
    {
        return Mathf.Clamp(((available) * 0.017f) + 2f, 2f, 18f);
    }
    float Price(int domain, int effectCount)
    {
        float result = Mathf.Pow((20 + domain), 2)
            * (Mathf.Pow((artistDomainActive[domain].Artist.Count + 1), domainPower[domain])
            + Mathf.Pow((available), totalPower))
            * (effectCount) * 0.1f;
        //float min = (float)productionA * effectCount * (domain + 20) * 0.05f;
        //float max = (float)productionA * (artistDomainActive[domain].Artist.Count + 1) * effectCount * (domain + 20) * 0.5f;
        //return Mathf.Clamp(result, min, max);
        return result;
    }
    float ItemPrice(Item item, int reference)
    {
        /*return (((item.Total * 30)
            * Mathf.Pow(50, item.Level))
            * (Mathf.Pow(10, item.Era))
            * priceModifierP);*/
        if (item.Level >= 4)
        {
            return 0;
        }
        if (item.Level > 0)
        {
            return Mathf.Pow(200, item.Level) * currentPrices[reference];
        }
        else
        {
            return currentPrices[reference];
        }
    }
    float BonusPrice(int bonusRef)
    {
        /*return bonusStats[bonusRef] * Mathf.Pow(50, bonuses[bonusRef].Level) * bonusPrice;*/
        if (bonuses[bonusRef].Level > 0)
        {
            return Mathf.Pow(200, bonuses[bonusRef].Level) * bonusPrices[bonusRef];
        }
        else
        {
            return bonusPrices[bonusRef];
        }
    }
    public float EraPrice(int domain, DomainShop shop)
    {
        return (100 * Mathf.Pow(20, (shop.era + 1)) * Mathf.Pow(2, (domain + 1)) * priceModifierC);
    }
    public string RoundNumber(double number, double currentProd, bool isShort)
    {
        string numberDisplay;
        string Splus = " Sept.";
        string S = " Sext.";
        string Qui = "Quint.";
        string Q = " Quad.";
        string T = " Trillion";
        string B = " Billion";
        string M = " Million";
        string K = " Thousand";
        if (isShort)
        {
            Splus = "S+";
            S = "S";
            Qui = "Qi";
            Q = "Q";
            T = "T";
            B = "B";
            M = "M";
            K = "K";
        }
        double prod = currentProd;
        if (number >= 1000000000000000000000000f)
        {
            double numb = number * 0.000000000000000000000001f;
            if (prod * 30 < 1000000000000000000000000f)
            {
                numberDisplay = Math.Round(numb, 2) + Splus;
            }
            else if (prod * 3 < 1000000000000000000000000f)
            {
                numberDisplay = Math.Round(numb, 1) + Splus;
            }
            else
            {
                numberDisplay = Math.Round(numb) + Splus;
            }
        }
        else if (number >= 1000000000000000000000f)
        {
            double numb = number * 0.000000000000000000001f;
            if (prod * 30 < 1000000000000000000000f)
            {
                numberDisplay = Math.Round(numb, 2) + S;
            }
            else if (prod * 3 < 1000000000000000000000f)
            {
                numberDisplay = Math.Round(numb, 1) + S;
            }
            else
            {
                numberDisplay = Math.Round(numb) + S;
            }
        }
        else if (number >= 1000000000000000000)
        {
            double numb = number * 0.000000000000000001f;
            if (prod * 30 < 1000000000000000000)
            {
                numberDisplay = Math.Round(numb, 2) + Qui;
            }
            else if (prod * 3 < 1000000000000000000)
            {
                numberDisplay = Math.Round(numb, 1) + Qui;
            }
            else
            {
                numberDisplay = Math.Round(numb) + Qui;
            }
        }
        else if (number >= 1000000000000000)
        {
            double numb = number * 0.000000000000001f;
            if (prod * 30 < 1000000000000000)
            {
                numberDisplay = Math.Round(numb, 2) + Q;
            }
            else if (prod * 3 < 1000000000000000)
            {
                numberDisplay = Math.Round(numb, 1) + Q;
            }
            else
            {
                numberDisplay = Math.Round(numb) + Q;
            }
        }
        else if (number >= 1000000000000)
        {
            double numb = number * 0.000000000001f;
            if (prod * 30 < 1000000000000)
            {
                numberDisplay = Math.Round(numb, 2) + T;
            }
            else if (prod * 3 < 1000000000000)
            {
                numberDisplay = Math.Round(numb, 1) + T;
            }
            else
            {
                numberDisplay = Math.Round(numb) + T;
            }
        }
        else if (number >= 1000000000)
        {
            double numb = number * 0.000000001f;
            if (prod * 30 < 1000000000)
            {
                numberDisplay = Math.Round(numb, 2) + B;
            }
            else if (prod * 3 < 1000000000)
            {
                numberDisplay = Math.Round(numb, 1) + B;
            }
            else
            {
                numberDisplay = Math.Round(numb) + B;
            }
        }
        else if (number >= 1000000)
        {
            double numb = number * 0.000001f;
            if (prod * 30 < 1000000)
            {
                numberDisplay = Math.Round(numb, 2) + M;
            }
            else if (prod * 3 < 1000000)
            {
                numberDisplay = Math.Round(numb, 1) + M;
            }
            else
            {
                numberDisplay = Math.Round(numb) + M;
            }
        }
        else if (number >= 1000)
        {
            double numb = number * 0.001f;
            if (prod * 30 < 1000)
            {
                numberDisplay = Math.Round(numb, 2) + K;
            }
            else if (prod * 3 < 1000)
            {
                numberDisplay = Math.Round(numb, 1) + K;
            }
            else
            {
                numberDisplay = Math.Round(numb) + K;
            }
        }
        else
        {
            if (prod * 5 < 100)
            {
                numberDisplay = Math.Round(number, 1).ToString();
            }
            else
            {
                numberDisplay = Math.Round(number).ToString();
            }
        }
        return numberDisplay;
    }
    public string RoundNumber(double number, bool isShort)
    {
        string numberDisplay;
        string Splus = " Sept.";
        string S = " Sext.";
        string Qui = "Quint.";
        string Q = " Quad.";
        string T = " Trillion";
        string B = " Billion";
        string M = " Million";
        string K = " Thousand";
        if (isShort)
        {
            Splus = "S+";
            S = "S";
            Qui = "Qi";
            Q = "Q";
            T = "T";
            B = "B";
            M = "M";
            K = "K";
        }
        if (number >= 1000000000000000000000000f)
        {
            double numb = number * 0.000000000000000000000001f;
            if (numb > 100)
            {
                numberDisplay = Math.Round(numb) + Splus;
            }
            else if (numb > 10)
            {
                numberDisplay = Math.Round(numb, 1) + Splus;
            }
            else
            {
                numberDisplay = Math.Round(numb, 2) + Splus;
            }
        }
        else if (number >= 1000000000000000000000f)
        {
            double numb = number * 0.000000000000000000001f;
            if (numb > 100)
            {
                numberDisplay = Math.Round(numb) + S;
            }
            else if (numb > 10)
            {
                numberDisplay = Math.Round(numb, 1) + S;
            }
            else
            {
                numberDisplay = Math.Round(numb, 2) + S;
            }
        }
        else if (number >= 1000000000000000000)
        {
            double numb = number * 0.000000000000000001f;
            if (numb > 100)
            {
                numberDisplay = Math.Round(numb) + Qui;
            }
            else if (numb > 10)
            {
                numberDisplay = Math.Round(numb, 1) + Qui;
            }
            else
            {
                numberDisplay = Math.Round(numb, 2) + Qui;
            }
        }
        else if (number >= 1000000000000000)
        {
            double numb = number * 0.000000000000001f;
            if (numb > 100)
            {
                numberDisplay = Math.Round(numb) + Q;
            }
            else if (numb > 10)
            {
                numberDisplay = Math.Round(numb, 1) + Q;
            }
            else
            {
                numberDisplay = Math.Round(numb, 2) + Q;
            }
        }
        else if (number >= 1000000000000)
        {
            double numb = number * 0.000000000001f;
            if (numb > 100)
            {
                numberDisplay = Math.Round(numb) + T;
            }
            else if (numb > 10)
            {
                numberDisplay = Math.Round(numb, 1) + T;
            }
            else
            {
                numberDisplay = Math.Round(numb, 2) + T;
            }
        }
        else if (number >= 1000000000)
        {
            double numb = number * 0.000000001f;
            if (numb > 100)
            {
                numberDisplay = Math.Round(numb) + B;
            }
            else if (numb > 10)
            {
                numberDisplay = Math.Round(numb, 1) + B;
            }
            else
            {
                numberDisplay = Math.Round(numb, 2) + B;
            }
        }
        else if (number >= 1000000)
        {
            double numb = number * 0.000001f;
            if (numb > 100)
            {
                numberDisplay = Math.Round(numb) + M;
            }
            else if (numb > 10)
            {
                numberDisplay = Math.Round(numb, 1) + M;
            }
            else
            {
                numberDisplay = Math.Round(numb, 2) + M;
            }
        }
        else if (number >= 1000)
        {
            double numb = number * 0.001f;
            if (numb > 100)
            {
                numberDisplay = Math.Round(numb) + K;
            }
            else if (numb > 10)
            {
                numberDisplay = Math.Round(numb, 1) + K;
            }
            else
            {
                numberDisplay = Math.Round(numb, 2) + K;
            }
        }
        else
        {
            if (number > 100)
            {
                numberDisplay = Math.Round(number).ToString();
            }
            else if (number > 10)
            {
                numberDisplay = Math.Round(number, 1).ToString();
            }
            else
            {
                numberDisplay = Math.Round(number, 2).ToString();
            }
        }
        return numberDisplay;
    }
    float ArtistOutput(float multi, int domain, float gaugeEffect)
    {
        float personal;
        float result = 0;
        if (multi < 0)
        {
            personal = 10 / ((1 + multi * multEffectiveness) * 0.5f);
        }
        else
        {
            personal = 10 * ((1 + multi * multEffectiveness));
        }
        if (globalMultiplicator > 0)
        {
            result = personal * gaugeEffect * gaugeEffect * eraMultiplicator;
        }
        else if (globalMultiplicator < 0)
        {
            result = (personal / gaugeEffect) * eraMultiplicator;
        }
        else if (globalMultiplicator == 0)
        {
            result = personal * eraMultiplicator;
        }
        return result;
    }
    public void Calculation()
    {
        // Reset
        #region Reset
        globalMultiplicatorEffectC = 0;
        globalMultiplicatorEffectA = 0;
        globalMultiplicatorEffectP = 0;
        globalMultiplicatorEffectivenessC = globalOrigin;
        globalMultiplicatorEffectivenessA = globalOrigin;
        globalMultiplicatorEffectivenessP = globalOrigin;
        priceModifierC = 1;
        priceModifierP = 1;
        productionC = 0;
        productionA = 0;
        productionP = 0;
        bonusStats[0] = 0;
        for (int z = 0; z < domainEffectMultiplicator.Length; z++)
        {
            domainEffectMultiplicator[z] = 1;
        }
        for (int z = 0; z < domainMultiplicatorsC.Length; z++)
        {
            domainMultiplicatorsC[z] = 0;
            domainMultiplicatorsA[z] = 0;
            domainMultiplicatorsP[z] = 0;
        }
        for (int z = 0; z < areas.Count; z++)
        {
            areas[z].multiplicator = 0;
        }
        for (int z = 0; z < tags.Count; z++)
        {
            tags[z].multiplicator = 0;
        }
        for (int z = 0; z < currents.Count; z++)
        {
            currents[z].multiplicator = 0;
        }
        for (int z = 0; z < domainProductionC.Length; z++)
        {
            domainProductionC[z] = 0;
        }
        for (int z = 0; z < domainProductionA.Length; z++)
        {
            domainProductionA[z] = 0;
        }
        for (int z = 0; z < domainProductionP.Length; z++)
        {
            domainProductionP[z] = 0;
        }

        #endregion
        // Stats
        #region Stats
        int marginalized = 0;
        int marginalized2 = 0;
        int queer = 0;
        for (int i = 0; i < areas.Count; i++)
        {
            areas[i].amount = 0;
        }
        for (int i = 0; i < bonusLists[0].Count; i++)
        {
            areas[bonusLists[0][i].area].amount += bonusLists[0][i].amount;
            if (bonusLists[0][i].amount > 0)
            {
                bonusStats[0]++;
            }
        }
        if (bonusStats[0] >= 10 && bonuses[0] == null)
        {
            DisplayBonus(0, 0);
            StartCoroutine(ItemsFade());
        }
        for (int i = 0; i < areas.Count; i++)
        {
            if (areas[i].name != "North America" && areas[i].name != "Eastern Europe" && areas[i].name != "Western Europe")
                marginalized += areas[i].amount;
        }
        for (int i = 0; i < tags.Count; i++)
        {
            if (tags[i].name == "Racialized" || tags[i].name == "Native")
            {
                marginalized += tags[i].amount;
            }
            else if (tags[i].name == "Queer")
            {
                queer += tags[i].amount;
            }
            else
            {
                marginalized2 += tags[i].amount;
            }
        }
        for (int i = 0; i < activeItems.Count; i++)
        {
            if (activeItems[i].SubGenders.Count > 1)
            {
                for (int y = 0; y < activeItems[i].SubGenders.Count; y++)
                {
                    activeItems[i].SubTexts[y].text = currents[activeItems[i].SubRefs[y]].name + ": " + currents[activeItems[i].SubRefs[y]].amount + " artists";
                }
            }
            else
            {
                activeItems[i].SubTexts[0].text = "Artists: " + currents[activeItems[i].SubRefs[0]].amount;
            }
        }

        #endregion
        // Ratios
        #region Ratios
        imperialRatio = marginalized - (available * 0.45f);
        justiceRatio = marginalized2 - (available * 0.2f);
        patriarcalRatio = women - men;
        heteroRatio = queer - (available * 0.09f);
        womenMultiplicator = patriarcalRatio * 0.2f;
        marginalizedMultiplicator = imperialRatio * 0.2f;
        queerMultiplicator = heteroRatio * 0.2f;
        for (int i = 0; i < majorCurrents.Length; i++)
        {
            for (int y = 0; y < majorCurrents[i].SubCurrents.Count; y++)
            {
                if (availableItems[i] != null)
                {
                    if (availableItems[i].Level > 0)
                    {
                        currents[majorCurrents[i].SubCurrents[y]].multiplicator += Mathf.Pow(itemBonus, availableItems[i].Level);
                    }
                }
            }
        }

        #endregion
        // Calculate Effects
        #region Calculate Effects
        // Reading Effects
        int Ps = 0;
        int As = 0;
        for (int i = 0; i < artistDomainActive.Count; i++)
        {
            domainEffectMultiplicator[i] += domainShops[i].titleCount * 0.05f;
            for (int y = 0; y < artistDomainActive[i].Artist.Count; y++)
            {
                float effectMultiplicator = domainEffectMultiplicator[i];
                if (artistDomainActive[i].Artist[y].Lead == 1)
                {
                    effectMultiplicator += (artistDomainActive[i].Artist.Count) * 0.1f;
                }
                else if (artistDomainActive[i].Artist[y].Lead == 2)
                {
                    effectMultiplicator += available * 0.05f;
                }
                // Checking for "Range" Multiplicators
                for (int x = 0; x < artistDomainActive[i].Artist[y].effectRefs.Count; x++)
                {
                    string newEffect = effectList.Effect[artistDomainActive[i].Artist[y].effectRefs[x]].Type;
                    if (newEffect == "Range")
                    {
                        effectMultiplicator *= 1.5f;
                    }
                }
                // Checking for other effects
                for (int z = 0; z < artistDomainActive[i].Artist[y].effectRefs.Count; z++)
                {
                    Effect newEffect = effectList.Effect[artistDomainActive[i].Artist[y].effectRefs[z]];
                    if (newEffect.Type == "Activism")
                    {
                        if (newEffect.SubType == "Feminism")
                        {
                            patriarcalRatio += 4.5f * effectMultiplicator;
                        }
                        else if (newEffect.SubType == "Antiracism")
                        {
                            imperialRatio += 4.5f * effectMultiplicator;
                        }
                        else if (newEffect.SubType == "Black Feminism")
                        {
                            patriarcalRatio += 2.5f * effectMultiplicator;
                            imperialRatio += 2.5f * effectMultiplicator;
                        }
                        else if (newEffect.SubType == "Justice")
                        {
                            justiceRatio += 4.5f;
                        }
                        else if (newEffect.SubType == "Queer")
                        {
                            heteroRatio += 4.5f * effectMultiplicator;
                        }
                    }
                    else if (newEffect.Type == "Contribution")
                    {
                        if (newEffect.SubType == "Cultural")
                        {
                            priceModifierC *= 0.9f;
                        }
                        else
                        {
                            priceModifierP *= 0.9f;
                        }
                    }
                    else if (newEffect.Type == "Aesthetic")
                    {
                        As += 10 * (int)effectMultiplicator;
                    }
                    else if (newEffect.Type == "Philosophy")
                    {
                        Ps += 10 * (int)effectMultiplicator;
                    }
                    else if (newEffect.Type == "Current")
                    {
                        for (int w = 0; w < currents.Count; w++)
                        {
                            if (newEffect.Name == currents[w].name)
                            {
                                currents[w].multiplicator += 1 + (14f * (currents[w].amount / currents[w].total) * (1 / (currents[w].total + 40)) * 65 * effectMultiplicator);
                            }
                        }
                    }
                    else if (newEffect.Type == "Genre")
                    {
                        for (int w = 0; w < artistDomainActive.Count; w++)
                        {
                            if (newEffect.Domain[0] == artistDomainActive[w].name)
                            {
                                if (newEffect.SubType == "Cultural")
                                {
                                    domainMultiplicatorsC[w] += (3 + (27 * (artistDomainActive[w].Artist.Count / 50))) * effectMultiplicator;
                                }
                                else if (newEffect.SubType == "Aesthetic")
                                {
                                    domainMultiplicatorsA[w] += (3 + (27 * (artistDomainActive[w].Artist.Count / 50))) * effectMultiplicator;
                                }
                                else
                                {
                                    domainMultiplicatorsP[w] += (3 + (27 * (artistDomainActive[w].Artist.Count / 50))) * effectMultiplicator;
                                }
                            }
                        }
                    }
                    else if (newEffect.Type == "Achievement")
                    {
                        if (newEffect.Name == "Popular Success")
                        {
                            globalMultiplicatorEffectC += effectMultiplicator * 0.75f;
                        }
                        else if (newEffect.Name == "Influence")
                        {
                            globalMultiplicatorEffectA += effectMultiplicator * 0.75f;
                        }
                        else
                        {
                            globalMultiplicatorEffectP += effectMultiplicator * 0.75f;
                        }
                    }
                    else if (newEffect.Type == "Prize")
                    {
                        globalMultiplicatorEffectA += effectMultiplicator * 0.375f;
                        globalMultiplicatorEffectC += effectMultiplicator * 0.375f;
                    }
                    else if (newEffect.Type == "Domain-defined movement")
                    {
                        for (int w = 0; w < artistDomainActive.Count; w++)
                        {
                            if (newEffect.Domain[0] == artistDomainActive[w].name)
                            {
                                domainMultiplicatorsA[w] += (2 + (28 * (bonusStats[4] / bonusLists[4].Count))) * effectMultiplicator;
                            }
                        }
                    }
                    else if (newEffect.Type == "Activity")
                    {
                        if (newEffect.Name == "Multitalented Artist")
                        {
                            domainMultiplicatorsC[i] += Mathf.Clamp(2 + (available * 0.1f), 2, 35) * effectMultiplicator;
                        }
                        else
                        {
                            List<int> refs = new List<int>();
                            for (int w = 0; w < newEffect.Domain.Count; w++)
                            {
                                for (int v = 0; v < artistDomainActive.Count; v++)
                                {
                                    if (artistDomainActive[v].name == newEffect.Domain[w])
                                    {
                                        refs.Add(v);
                                    }
                                }
                            }
                            if (refs.Count == 3)
                            {
                                domainMultiplicatorsC[refs[0]] += Mathf.Clamp(2f + ((artistDomainActive[refs[1]].Artist.Count + artistDomainActive[refs[2]].Artist.Count) * 0.4f), 2, 30) * effectMultiplicator;
                            }
                            else
                            {
                                domainMultiplicatorsC[refs[0]] += Mathf.Clamp(2f + (artistDomainActive[refs[1]].Artist.Count * 0.75f), 2, 30) * effectMultiplicator;
                            }
                        }
                    }
                    else if (newEffect.Type == "Area-defined movement")
                    {
                        for (int w = 0; w < areas.Count; w++)
                        {
                            if (areas[w].name == newEffect.Area)
                            {
                                areas[w].multiplicator += (2 + (28 * (areas[w].amount / areas[w].total)) * (1 / (areas[w].total + 40)) * 65) * effectMultiplicator;
                            }
                        }
                    }
                    else if (newEffect.Type == "Group")
                    {
                        if (newEffect.SubType == "Company or School")
                        {
                            globalMultiplicatorEffectivenessP *= (1.1f * effectMultiplicator);
                        }
                        else if (newEffect.SubType == "Cooperative")
                        {
                            globalMultiplicatorEffectivenessA *= (1.1f * effectMultiplicator);
                        }
                        else
                        {
                            globalMultiplicatorEffectivenessC *= (1.1f * effectMultiplicator);
                        }
                    }
                    else if (newEffect.Type == "Discovery")
                    {
                        domainMultiplicatorsP[i] += (4 + (36 * (artistDomainActive[i].Artist.Count / 50))) * effectMultiplicator;
                    }
                }
            }
        }
        // Applying Philosophy and Aesthetic
        for (int z = 0; z < Ps; z++)
        {
            globalMultiplicatorEffectC += globalMultiplicatorEffectP * 0.005f;
        }
        for (int z = 0; z < As; z++)
        {
            globalMultiplicatorEffectC += globalMultiplicatorEffectA * 0.005f;
        }

        #endregion
        // Global Multiplicator
        #region Global Multiplicator
        eraMultiplicator = Mathf.Pow(eraFactor, currentSociety);
        globalMultiplicator = 2 - blindEyes + (patriarcalRatio + imperialRatio + justiceRatio + heteroRatio) * 0.25f;
        globalMultiplicator += rejected * 2;
        gaugeSize = (Mathf.Abs(globalMultiplicator) * 0.006455f);
        if (gaugeSize != gauge.localScale.y)
        {
            gaugeTime = 0;
        }

        #endregion
        // News
        #region News
        if (newsChange)
        {
            save.newsEra = newsEra;
            newsPlay = 0;
            newsListAvailable.News.Clear();
            for (int i = 0; i < newsList.News.Count; i++)
            {
                if (newsList.News[i].Era == newsEra)
                {
                    newsListAvailable.News.Add(newsList.News[i]);
                }
            }
            newsRef = 0;
            newsChange = false;
        }

        #endregion
        // Production
        #region Production
        int gaugeRatio = (int)Math.Floor(globalMultiplicator / 10);
        float gaugeEffectC = 1 + Mathf.Abs(gaugeRatio * globalMultiplicatorEffectivenessC);
        float gaugeEffectA = 1 + Mathf.Abs(gaugeRatio * globalMultiplicatorEffectivenessA);
        float gaugeEffectP = 1 + Mathf.Abs(gaugeRatio * globalMultiplicatorEffectivenessP);
        if (gameOn)
        {
            if (gaugeRatio > currentGauge)
            {
                StartCoroutine(GaugeChange(1));
            }
            else if (gaugeRatio < currentGauge)
            {
                StartCoroutine(GaugeChange(2));
            }
        }
        currentGauge = gaugeRatio;
        List<float> BonusHelp = new List<float>();
        for (int z = 0; z < bonuses.Length; z++)
        {
            if (bonuses[z] != null)
            {
                if (bonuses[z].Level > 0)
                {
                    BonusHelp.Add(Mathf.Pow(bonusFactor1 + ((bonusStats[z] / bonusLists[z].Count) * bonusFactor2), bonuses[z].Level));
                }
            }
        }
        for (int i = 0; i < artistDomainActive.Count; i++)
        {
            for (int y = 0; y < artistDomainActive[i].Artist.Count; y++)
            {
                // Check Multiplicators
                bool marginalizedArtist = false;
                float multi = 1;
                float multiC = 1;
                float multiA = 1;
                float multiP = 1;
                multiC += domainMultiplicatorsC[i];
                multiA += domainMultiplicatorsA[i];
                multiP += domainMultiplicatorsP[i];
                for (int x = 0; x < artistDomainActive[i].Artist[y].currentRefs.Count; x++)
                {
                    multi += currents[artistDomainActive[i].Artist[y].currentRefs[x]].multiplicator;
                }
                for (int x = 0; x < artistDomainActive[i].Artist[y].tagRefs.Count; x++)
                {
                    multi += tags[artistDomainActive[i].Artist[y].tagRefs[x]].multiplicator;
                    if (tags[artistDomainActive[i].Artist[y].tagRefs[x]].name == "Racialized" ||
                        tags[artistDomainActive[i].Artist[y].tagRefs[x]].name == "Native")
                    {
                        marginalizedArtist = true;
                    }
                    else if (tags[artistDomainActive[i].Artist[y].tagRefs[x]].name == "Queer")
                    {
                        multi += queerMultiplicator;
                    }
                }
                for (int x = 0; x < artistDomainActive[i].Artist[y].Nation.Count; x++)
                {
                    if (!marginalizedArtist)
                    {
                        if (areas[bonusLists[0][artistDomainActive[i].Artist[y].nationRefs[x]].area].name != "North America" &&
                            areas[bonusLists[0][artistDomainActive[i].Artist[y].nationRefs[x]].area].name != "Eastern Europe" &&
                            areas[bonusLists[0][artistDomainActive[i].Artist[y].nationRefs[x]].area].name != "Western Europe" &&
                            areas[bonusLists[0][artistDomainActive[i].Artist[y].nationRefs[x]].area].name != "Eastern Asia")
                        {
                            marginalizedArtist = true;
                        }
                    }
                }
                if (marginalizedArtist)
                {
                    multi += marginalizedMultiplicator;
                }
                else if (marginalizedMultiplicator < 0)
                {
                    multi -= marginalizedMultiplicator * 0.5f;
                }
                if (artistDomainActive[i].Artist[y].Gender.Count > 1)
                {
                }
                else if (artistDomainActive[i].Artist[y].Gender[0] == "Female")
                {
                    multi += womenMultiplicator;
                }
                else if (womenMultiplicator < 0)
                {
                    multi -= womenMultiplicator * 0.5f;
                }
                multiC += globalMultiplicatorEffectC;
                multiA += globalMultiplicatorEffectA;
                multiP += globalMultiplicatorEffectP;
                for (int z = 0; z < BonusHelp.Count; z++)
                {
                    multi += BonusHelp[z];
                }
                // Multiply
                if (multi < 0)
                {
                    multi = 0;
                }
                artistDomainActive[i].Artist[y].ProductionC = ArtistOutput(multi * multiC, i, gaugeEffectC);
                artistDomainActive[i].Artist[y].ProductionA = ArtistOutput(multi * multiA, i, gaugeEffectA);
                artistDomainActive[i].Artist[y].ProductionP = ArtistOutput(multi * multiP, i, gaugeEffectP);
                domainProductionC[i] += artistDomainActive[i].Artist[y].ProductionC;
                productionC += artistDomainActive[i].Artist[y].ProductionC;
                domainProductionA[i] += artistDomainActive[i].Artist[y].ProductionA;
                productionA += artistDomainActive[i].Artist[y].ProductionA;
                domainProductionP[i] += artistDomainActive[i].Artist[y].ProductionP;
                productionP += artistDomainActive[i].Artist[y].ProductionP;
                artistDomainActive[i].Artist[y].ProdC.text = "<sprite index=0> " + RoundNumber(artistDomainActive[i].Artist[y].ProductionC, true);
                artistDomainActive[i].Artist[y].ProdA.text = "<sprite index=1> " + RoundNumber(artistDomainActive[i].Artist[y].ProductionA, true);
                artistDomainActive[i].Artist[y].ProdP.text = "<sprite index=2> " + RoundNumber(artistDomainActive[i].Artist[y].ProductionP, true);
            }
        }
        if (stockC < 0)
        {
            stockC = 100;
        }
        if (stockA < 0)
        {
            stockA = 100;
        }
        if (stockP < 0)
        {
            stockP = 100;
        }

        #endregion
        //Stats Display
        DisplayStats();
        SaveGame();
        // Set Prices
        for (int i = 0; i < domainShops.Length; i++)
        {
            if (domainShops[i].era != 0)
            {
                domainShops[i].price = EraPrice(i, domainShops[i]);
                EraPriceDisplay(domainShops[i]);
            }
        }
    }

    #endregion
    // Functions List
    #region List

    public void SwitchInterface()
    {
        if (!rightClick && !drawOn && !drawing && !Input.GetMouseButton(1))
        {
            if (interfaceList)
            {
                PlaySound(clips[12], 0.25f, 1);
                for (int i = 0; i < titles.Count; i++)
                {
                    GameObject.Destroy(titles[i]);
                }
                for (int i = 0; i < artistsList.childCount; i++)
                {
                    artistsList.GetChild(artistsList.childCount - (1 + i)).gameObject.SetActive(false);
                }
                cameraListLayer.gameObject.SetActive(false);
                canvas.sprite = canvasDefault;
                starMask.sprite = starMaskDefault;
                background.sprite = backgroundDefault;
                sideBar.SetActive(true);
                cameraDefault.gameObject.SetActive(true);
                cameraList.gameObject.SetActive(false);
                listObjects.SetActive(false);
                interfaceList = false;
                cameraDefaultLayer.sortingOrder = 1;
                cameraListLayer.sortingOrder = 0;
                panel.gameObject.SetActive(false);
            }
            else
            {
                PlaySound(clips[10], 0.25f, 1);
                cameraListLayer.gameObject.SetActive(true);
                canvas.sprite = canvasList;
                starMask.sprite = starMaskList;
                background.sprite = backgroundList;
                sideBar.SetActive(false);
                cameraDefault.SetActive(false);
                cameraList.SetActive(true);
                listObjects.SetActive(true);
                interfaceList = true;
                cameraDefaultLayer.sortingOrder = 0;
                cameraListLayer.sortingOrder = 1;
                SortList(0);
                listScroll.AddItem(simplifiedArtists.Count);
                listScroll.currentTime = 0;
                listScroll.stage = 0;
                panel.gameObject.SetActive(false);
            }
        }
    }
    public void SortList(int reference)
    {
        PlaySound(clips[11], 0.25f, 1);
        int objectNumber = 0;
        for (int i = 0; i < titles.Count; i++)
        {
            GameObject.Destroy(titles[i]);
        }
        titles = new List<GameObject>();
        for (int i = 0; i < simplifiedArtists.Count; i++)
        {
            simplifiedArtists[i].Occurences = 0;
        }
        // Alphabetical Name
        if (reference == 0)
        {
            simplifiedArtists.Sort((x, y) => x.ListName.CompareTo(y.ListName));
            for (int i = 0; i < simplifiedArtists.Count; i++)
            {
                ArtistSimple artist = simplifiedArtists[i];
                AddArtist(artist);
                objectNumber++;
            }
        }
        // Domain
        if (reference == 1)
        {
            List<ArtistSimple>[] domainArtists = new List<ArtistSimple>[domain.Count];
            for (int i = 0; i < domainArtists.Length; i++)
            {
                domainArtists[i] = new List<ArtistSimple>();
            }
            for (int i = 0; i < simplifiedArtists.Count; i++)
            {
                domainArtists[simplifiedArtists[i].Domain].Add(simplifiedArtists[i]);
            }
            for (int i = 0; i < domainArtists.Length; i++)
            {
                if (domainArtists[i].Count > 0)
                {
                    GameObject newTitle = GameObject.Instantiate(listTitle, artistsList);
                    string titleText = domain[i];
                    string subTitle;
                    if (titleText == "Artification")
                    {
                        GameObject newSubTitle = GameObject.Instantiate(listSubTitle, artistsList);
                        subTitle = " (These persons made great contributions to the Arts by means that are not directly creative.)";
                        newSubTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTitle;
                        titles.Add(newSubTitle);
                    }
                    else if (titleText == "Outsider Art")
                    {
                        GameObject newSubTitle = GameObject.Instantiate(listSubTitle, artistsList);
                        subTitle = " (These artists practice outside the Art world, or at its extreme margins.)";
                        newSubTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTitle;
                        titles.Add(newSubTitle);
                    }
                    else if (titleText == "Comics and Iconotext")
                    {
                        GameObject newSubTitle = GameObject.Instantiate(listSubTitle, artistsList);
                        subTitle = " (Iconotexts combine text with still images. They notably include comics and illustrated books)";
                        newSubTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTitle;
                        titles.Add(newSubTitle);
                    }
                    else if (titleText == "Contemporary Art")
                    {
                        GameObject newSubTitle = GameObject.Instantiate(listSubTitle, artistsList);
                        subTitle = " (Art forms which developed with modernism: installation, performance, assemblage, etc)";
                        newSubTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTitle;
                        titles.Add(newSubTitle);
                    }
                    newTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = titleText;
                    titles.Add(newTitle);
                    objectNumber++;
                    domainArtists[i].Sort((x, y) => x.ListName.CompareTo(y.ListName));
                    for (int y = 0; y < domainArtists[i].Count; y++)
                    {
                        ArtistSimple artist = domainArtists[i][y];
                        AddArtist(artist);
                        objectNumber++;
                    }
                }
            }
        }
        // Birth
        if (reference == 2)
        {
            simplifiedArtists.Sort((x, y) => x.ArtistDetail.Birth.CompareTo(y.ArtistDetail.Birth));
            for (int i = 0; i < simplifiedArtists.Count; i++)
            {
                ArtistSimple artist = simplifiedArtists[i];
                AddArtist(artist);
                objectNumber++;
            }
        }
        // Currents
        if (reference == 3)
        {
            List<ArtistSimple>[] domainArtists = new List<ArtistSimple>[currents.Count];
            for (int i = 0; i < domainArtists.Length; i++)
            {
                domainArtists[i] = new List<ArtistSimple>();
            }
            for (int i = 0; i < simplifiedArtists.Count; i++)
            {
                for (int y = 0; y < simplifiedArtists[i].ArtistDetail.currentRefs.Count; y++)
                {
                    domainArtists[simplifiedArtists[i].ArtistDetail.currentRefs[y]].Add(simplifiedArtists[i]);
                }
            }
            string[] sortingArray = new string[currents.Count];
            for (int i = 0; i < currents.Count; i++)
            {
                sortingArray[i] = currents[i].name;
            }
            Array.Sort(sortingArray, domainArtists);
            for (int i = 0; i < domainArtists.Length; i++)
            {
                if (domainArtists[i].Count > 0)
                {
                    GameObject newTitle = GameObject.Instantiate(listTitle, artistsList);
                    newTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = sortingArray[i];
                    titles.Add(newTitle);
                    objectNumber++;
                    domainArtists[i].Sort((x, y) => x.ListName.CompareTo(y.ListName));
                    for (int y = 0; y < domainArtists[i].Count; y++)
                    {
                        ArtistSimple artist = domainArtists[i][y];
                        AddArtist(artist);
                        objectNumber++;
                    }
                }
            }
        }
        // Tags
        if (reference == 4)
        {
            List<ArtistSimple>[] domainArtists = new List<ArtistSimple>[tags.Count];
            for (int i = 0; i < domainArtists.Length; i++)
            {
                domainArtists[i] = new List<ArtistSimple>();
            }
            for (int i = 0; i < simplifiedArtists.Count; i++)
            {
                for (int y = 0; y < simplifiedArtists[i].ArtistDetail.tagRefs.Count; y++)
                {
                    domainArtists[simplifiedArtists[i].ArtistDetail.tagRefs[y]].Add(simplifiedArtists[i]);
                }
            }
            string[] sortingArray = new string[tags.Count];
            for (int i = 0; i < tags.Count; i++)
            {
                sortingArray[i] = tags[i].name;
            }
            Array.Sort(sortingArray, domainArtists);
            for (int i = 0; i < domainArtists.Length; i++)
            {
                if (domainArtists[i].Count > 0)
                {
                    GameObject newTitle = GameObject.Instantiate(listTitle, artistsList);
                    string subTitle;
                    if (sortingArray[i] == "Queer")
                    {
                        GameObject newSubTitle = GameObject.Instantiate(listSubTitle, artistsList);
                        subTitle = " (These artists are either not cisgender, not heterosexual, or neither of the two)";
                        newSubTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTitle;
                        titles.Add(newSubTitle);
                    }
                    else if (sortingArray[i] == "Racialized")
                    {
                        GameObject newSubTitle = GameObject.Instantiate(listSubTitle, artistsList);
                        subTitle = " (These artists are targeted by racism in their country of residence)";
                        newSubTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTitle;
                        titles.Add(newSubTitle);
                    }
                    else if (sortingArray[i] == "Persecuted")
                    {
                        GameObject newSubTitle = GameObject.Instantiate(listSubTitle, artistsList);
                        subTitle = " (These artists are targeted by religious persecution in their country of residence)";
                        newSubTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTitle;
                        titles.Add(newSubTitle);
                    }
                    else if (sortingArray[i] == "Censored")
                    {
                        GameObject newSubTitle = GameObject.Instantiate(listSubTitle, artistsList);
                        subTitle = " (At least one of these artists's works were banned in their country)";
                        newSubTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTitle;
                        titles.Add(newSubTitle);
                    }
                    else if (sortingArray[i] == "Exile")
                    {
                        GameObject newSubTitle = GameObject.Instantiate(listSubTitle, artistsList);
                        subTitle = " (These artists were forced to leave their country)";
                        newSubTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTitle;
                        titles.Add(newSubTitle);
                    }
                    else if (sortingArray[i] == "Executed")
                    {
                        GameObject newSubTitle = GameObject.Instantiate(listSubTitle, artistsList);
                        subTitle = " (Their life will be taken by an executive power)";
                        newSubTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTitle;
                        titles.Add(newSubTitle);
                    }
                    else if (sortingArray[i] == "Native")
                    {
                        GameObject newSubTitle = GameObject.Instantiate(listSubTitle, artistsList);
                        subTitle = " (These artists live on a colonized land originally occupied by their ethnic group)";
                        newSubTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTitle;
                        titles.Add(newSubTitle);
                    }
                    else if (sortingArray[i] == "Institutionalized")
                    {
                        GameObject newSubTitle = GameObject.Instantiate(listSubTitle, artistsList);
                        subTitle = " (These artists spent part of their life in a psychiatric institution)";
                        newSubTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTitle;
                        titles.Add(newSubTitle);
                    }
                    else if (sortingArray[i] == "Lifelong Illness")
                    {
                        GameObject newSubTitle = GameObject.Instantiate(listSubTitle, artistsList);
                        subTitle = " (These artists live with chronic illness)";
                        newSubTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTitle;
                        titles.Add(newSubTitle);
                    }
                    else if (sortingArray[i] == "Disability")
                    {
                        GameObject newSubTitle = GameObject.Instantiate(listSubTitle, artistsList);
                        subTitle = " (A condition impairs or limits some of these artists's abilities)";
                        newSubTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTitle;
                        titles.Add(newSubTitle);
                    }
                    newTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = sortingArray[i];
                    titles.Add(newTitle);
                    objectNumber++;
                    domainArtists[i].Sort((x, y) => x.ListName.CompareTo(y.ListName));
                    for (int y = 0; y < domainArtists[i].Count; y++)
                    {
                        ArtistSimple artist = domainArtists[i][y];
                        AddArtist(artist);
                        objectNumber++;
                    }
                }
            }
        }
        // Nation
        if (reference == 5)
        {
            List<ArtistSimple>[] domainArtists = new List<ArtistSimple>[bonusLists[0].Count];
            for (int i = 0; i < domainArtists.Length; i++)
            {
                domainArtists[i] = new List<ArtistSimple>();
            }
            for (int i = 0; i < simplifiedArtists.Count; i++)
            {
                for (int y = 0; y < simplifiedArtists[i].ArtistDetail.nationRefs.Count; y++)
                {
                    domainArtists[simplifiedArtists[i].ArtistDetail.nationRefs[y]].Add(simplifiedArtists[i]);
                }
            }
            string[] sortingArray = new string[bonusLists[0].Count];
            for (int i = 0; i < bonusLists[0].Count; i++)
            {
                sortingArray[i] = bonusLists[0][i].name;
            }
            Array.Sort(sortingArray, domainArtists);
            for (int i = 0; i < domainArtists.Length; i++)
            {
                if (domainArtists[i].Count > 0)
                {
                    GameObject newTitle = GameObject.Instantiate(listTitle, artistsList);
                    newTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = sortingArray[i];
                    titles.Add(newTitle);
                    objectNumber++;
                    domainArtists[i].Sort((x, y) => x.ListName.CompareTo(y.ListName));
                    for (int y = 0; y < domainArtists[i].Count; y++)
                    {
                        ArtistSimple artist = domainArtists[i][y];
                        AddArtist(artist);
                        objectNumber++;
                    }
                }
            }
        }
        // Gender
        if (reference == 6)
        {
            List<ArtistSimple>[] domainArtists = new List<ArtistSimple>[4];
            for (int i = 0; i < 4; i++)
            {
                domainArtists[i] = new List<ArtistSimple>();
            }
            for (int i = 0; i < simplifiedArtists.Count; i++)
            {
                if (simplifiedArtists[i].ArtistDetail.Gender.Count > 1)
                {
                    domainArtists[0].Add(simplifiedArtists[i]);
                }
                else if (simplifiedArtists[i].ArtistDetail.Gender[0] == "Female")
                {
                    domainArtists[1].Add(simplifiedArtists[i]);
                }
                else if (simplifiedArtists[i].ArtistDetail.Gender[0] == "Male")
                {
                    domainArtists[2].Add(simplifiedArtists[i]);
                }
                else
                {
                    domainArtists[3].Add(simplifiedArtists[i]);
                }
            }
            for (int i = 0; i < domainArtists.Length; i++)
            {
                if (domainArtists[i].Count > 0)
                {
                    GameObject newTitle = GameObject.Instantiate(listTitle, artistsList);
                    titles.Add(newTitle);
                    objectNumber++;
                    if (i == 0)
                    {
                        newTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Mixed group";
                    }
                    else if (i == 1)
                    {
                        newTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Women";
                    }
                    else if (i == 2)
                    {
                        newTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Men";
                    }
                    else
                    {
                        newTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Non-binary";
                    }
                    domainArtists[i].Sort((x, y) => x.ListName.CompareTo(y.ListName));
                    for (int y = 0; y < domainArtists[i].Count; y++)
                    {
                        ArtistSimple artist = domainArtists[i][y];
                        AddArtist(artist);
                        objectNumber++;
                    }
                }
            }
        }
        // Effects
        if (reference == 7)
        {
            int[] populations = new int[subTypes.Count];
            List<List<List<ArtistSimple>>> domainArtists = new List<List<List<ArtistSimple>>>();
            for (int i = 0; i < subTypes.Count; i++)
            {
                domainArtists.Add(new List<List<ArtistSimple>>());
                for (int y = 0; y < effectBoxes[i].Count; y++)
                {
                    domainArtists[i].Add(new List<ArtistSimple>());
                    for (int x = 0; x < simplifiedArtists.Count; x++)
                    {
                        for (int z = 0; z < simplifiedArtists[x].ArtistDetail.effectRefs.Count; z++)
                        {
                            if (simplifiedArtists[x].ArtistDetail.effectRefs[z] == effectBoxes[i][y])
                            {
                                domainArtists[i][y].Add(simplifiedArtists[x]);
                                populations[i]++;
                            }
                        }
                    }
                }
            }
            for (int i = 0; i < domainArtists.Count; i++)
            {
                if (populations[i] != 0)
                {
                    GameObject newTitle = GameObject.Instantiate(listTitle, artistsList);
                    newTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = subTypes[i];
                    titles.Add(newTitle);
                    objectNumber++;
                    for (int y = 0; y < domainArtists[i].Count; y++)
                    {
                        if (domainArtists[i][y].Count > 0)
                        {
                            GameObject smallTitle = GameObject.Instantiate(listTitleSmall, artistsList);
                            smallTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = effectList.Effect[effectBoxes[i][y]].Name;
                            titles.Add(smallTitle);
                            domainArtists[i][y].Sort((x, w) => x.ListName.CompareTo(w.ListName));
                            objectNumber++;
                            for (int z = 0; z < domainArtists[i][y].Count; z++)
                            {
                                ArtistSimple artist = domainArtists[i][y][z];
                                AddArtist(artist);
                                objectNumber++;
                            }
                        }
                    }
                }
            }
        }
        for (int i = 0; i < simplifiedArtists.Count; i++)
        {
            if (simplifiedArtists[i].Occurences == 0)
            {
                for (int y = 0; y < simplifiedArtists[i].Lines.Count; y++)
                {
                    simplifiedArtists[i].Lines[y].SetActive(false);
                }
            }
            else if (simplifiedArtists[i].Lines.Count > simplifiedArtists[i].Occurences)
            {
                for (int y = simplifiedArtists[i].Occurences; y < simplifiedArtists[i].Lines.Count; y++)
                {
                    simplifiedArtists[i].Lines[y].SetActive(false);
                }
            }
        }
        listScroll.AddItem(objectNumber);
        listScroll.currentTime = 0;
        listScroll.stage = 0;
    }
    void AddArtist(ArtistSimple artist)
    {
        artist.Occurences++;
        if (artist.Lines.Count < artist.Occurences)
        {
            CreateLine(artist);
        }
        else if (!artist.Lines[artist.Occurences - 1].activeSelf)
        {
            artist.Lines[artist.Occurences - 1].SetActive(true);
            artist.Lines[artist.Occurences - 1].transform.SetAsLastSibling();
        }
        else
        {
            artist.Lines[artist.Occurences - 1].transform.SetAsLastSibling();
        }
    }
    void CreateLine(ArtistSimple artist)
    {
        GameObject newLine = GameObject.Instantiate(line, artistsList);
        ColumnTitle script = newLine.GetComponent<ColumnTitle>();
        script.mouse = artist.ArtistDetail.Card.Find("MouseDetector").GetComponent<MouseDetector>();
        script.domain = artist.Domain;
        script.artist = artist.ArtistDetail;
        artist.Lines.Add(newLine);
        newLine.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = artist.ListName;
        newLine.transform.Find("Domain").GetComponent<TextMeshProUGUI>().text = domainShortened[artist.Domain];
        newLine.transform.Find("Era").GetComponent<TextMeshProUGUI>().text = artist.ListEra;
        // Currents
        string listCurrent = "";
        for (int i = 0; i < artist.ArtistDetail.Currents.Count; i++)
        {
            if (i > 0)
            {
                listCurrent += ", ";
            }
            if (artist.ArtistDetail.Currents[i] != "None")
            {
                listCurrent += artist.ArtistDetail.Currents[i];
            }
        }
        newLine.transform.Find("Currents").GetComponent<TextMeshProUGUI>().text = listCurrent;
        // Tags
        string listTags = "";
        for (int i = 0; i < artist.ArtistDetail.Tags.Count; i++)
        {
            if (i > 0)
            {
                listTags += ", ";
            }
            if (artist.ArtistDetail.Tags[i] != "None")
            {
                listTags += artist.ArtistDetail.Tags[i];
            }
        }
        newLine.transform.Find("Tags").GetComponent<TextMeshProUGUI>().text = listTags;
        // Nation
        for (int i = 0; i < artist.ArtistDetail.nationRefs.Count; i++)
        {
            GameObject newNation = GameObject.Instantiate(listNation, newLine.transform.Find("NationSort"));
            if (artist.ArtistDetail.nationRefs[i] == 8 && artist.ArtistDetail.Birth > 1900 && artist.ArtistDetail.Birth < 1980)
            {
                newNation.GetComponent<Image>().sprite = flags[118];
            }
            else
            {
                newNation.GetComponent<Image>().sprite = flags[artist.ArtistDetail.nationRefs[i]];
            }
            ListMouse lm = newNation.GetComponent<ListMouse>();
            lm.text = artist.ArtistDetail.Nation[i];
        }
        // Gender
        string listGender = "";
        for (int i = 0; i < artist.ArtistDetail.Gender.Count; i++)
        {
            if (artist.ArtistDetail.Gender[i] == "Female")
            {
                listGender += "<sprite index=6>";
            }
            else if (artist.ArtistDetail.Gender[i] == "Male")
            {
                listGender += "<sprite index=7>";
            }
            else
            {
                listGender += "<sprite index=8>";
            }
        }
        newLine.transform.Find("Gender").GetComponent<TextMeshProUGUI>().text = listGender;
        // Effects
        Transform effectSort = newLine.transform.Find("EffectSort");
        for (int i = 0; i < artist.ArtistDetail.effectRefs.Count; i++)
        {
            GameObject newEffect = GameObject.Instantiate(listEffect, effectSort);
            newEffect.GetComponent<Image>().color = effectColors[artist.ArtistDetail.Colors[i]];
            ListMouse lm = newEffect.GetComponent<ListMouse>();
            newEffect.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = artist.ArtistDetail.Spes[i];
            lm.isEffect = true;
            lm.text = artist.ArtistDetail.Texts[i];
            lm.title = artist.ArtistDetail.Titles[i];
        }
    }

    #endregion
    // Functions Menu
    #region Menu
    public void Menu()
    {
        StartCoroutine(DisplayMenu());
    }
    public void Quit()
    {
        choice = 0;
        StartCoroutine(ConfirmChoice());
    }
    public void Audio()
    {
        if (audioOn)
        {
            audioOn = false;
            soundText.text = "Audio Off";
        }
        else
        {
            audioOn = true;
            PlaySound(clips[10], 0.25f, 1);
            soundText.text = "Audio On";
        }
        save.audioOn = audioOn;
    }
    public void ToggleDisplay()
    {
        if (displayOn)
        {
            PlaySound(clips[12], 0.25f, 1);
            displayText.text = "Info Off";
            displayOn = false;
        }
        else
        {
            PlaySound(clips[10], 0.25f, 1);
            displayText.text = "Info On";
            displayOn = true;
        }
        save.displayOn = displayOn;
    }
    public void ToggleNoise()
    {
        if (noiseOn)
        {
            PlaySound(clips[12], 0.25f, 1);
            noiseText.text = "Noise Off";
            noiseOn = false;
            grain.enabled.value = false;
        }
        else
        {
            PlaySound(clips[10], 0.25f, 1);
            noiseText.text = "Noise On";
            noiseOn = true;
            grain.enabled.value = true;
        }
        save.noiseOn = noiseOn;
    }
    public void ToggleStars()
    {
        if (starsOn)
        {
            PlaySound(clips[12], 0.25f, 1);
            starsText.text = "Stars Off";
            starsOn = false;
            Background.gameObject.SetActive(false);
        }
        else
        {
            PlaySound(clips[10], 0.25f, 1);
            starsText.text = "Stars On";
            starsOn = true;
            Background.gameObject.SetActive(true);
        }
        save.starsOn = starsOn;
    }
    public void ToggleScreen()
    {
        if (screenOn)
        {
            PlaySound(clips[12], 0.25f, 1);
            screenText.text = "Windowed";
            screenOn = false;
            save.screenOn = false;
            Screen.fullScreen = false;
        }
        else
        {
            PlaySound(clips[10], 0.25f, 1);
            screenText.text = "Fullscreen";
            screenOn = true;
            save.screenOn = true;
            Screen.fullScreen = true;
        }
        save.starsOn = starsOn;
    }
    public void About()
    {
        StartCoroutine(DisplayAbout(1));
    }
    IEnumerator DisplayAbout(int type)
    {
        transPanel.gameObject.SetActive(true);
        transClick.alpha = 0;
        transTitle.alpha = 1;
        transSubTitle.alpha = 1;
        transText.alpha = 1;
        transParagraph.alpha = 1;
        transClick.transform.localPosition = new Vector2(transClick.transform.localPosition.x, -40);
        transSubTitle.transform.localPosition = new Vector2(transSubTitle.transform.localPosition.x, transSubTitle.transform.localPosition.y + 1.5f);
        RectTransform panelSize = transPanel.GetComponent<RectTransform>();
        transText.fontSize = 5;
        transText.alignment = TextAlignmentOptions.Justified;
        if (type == 1)
        {
            PlaySound(clips[11], 0.25f, 2);
            panelSize.sizeDelta = new Vector2(panelSize.sizeDelta.x, 110);
            transTitle.transform.localPosition = new Vector2(transTitle.transform.localPosition.x, 40);
            transText.transform.localPosition = new Vector2(transText.transform.localPosition.x, -8.5f);
            transPanel.transform.localPosition = new Vector2(transPanel.transform.localPosition.x, -3);
            transTitle.text = "About Worldwide Arts Society";
            transSubTitle.text = "An incremental collecting game by Antoine Ramo";
            transText.text = "Special Thanks to\n\nAnne, for her help and support.\nZeph, for his original idea of a Jack Lang simulator.\nAlexandre, for offering to proofread the descriptions.\nAmbrine, Chloé, Pierrick, Paul, Léa, Zeph and Esteban for playtesting this game.\nEvery kind person who has shown interest for it during its development.\nAnyone reading this, for your curiosity and attention.";
        }
        else
        {
            panelSize.sizeDelta = new Vector2(panelSize.sizeDelta.x, 74);
            transTitle.transform.localPosition = new Vector2(transTitle.transform.localPosition.x, 24.3f);
            transText.transform.localPosition = new Vector2(transTitle.transform.localPosition.x, -14.2f);
            transPanel.transform.localPosition = new Vector2(transPanel.transform.localPosition.x, 113.1f);
            PlaySound(melodies[3], 1f, 3);
            transTitle.text = "A Shameful Influence has shown up.";
            transSubTitle.text = "Try not not turn a blind eye on it.";
            transText.text = "The artist on the far right is quite influential, but they've done horrible things. By selecting them, you can call them out and lessen their influence. It's not an easy thing to do, and it will cost you some <sprite index=1>. But each time you let them here and draw another card instead, it will slightly hurt your social impact gauge.";
        }
        transClick.gameObject.GetComponent<TextMeshProUGUI>().text = "Click to close";
        click = true;
        float currentTime = 0;
        while (currentTime <= 0.5 && click)
        {
            currentTime += Time.deltaTime;
            transPanel.alpha = currentTime * 2;
            yield return null;
        }
        transPanel.alpha = 1;
        if (type == 1)
        {
            currentTime = 0;
            while (currentTime <= 0.5f && click)
            {
                currentTime += Time.deltaTime;
                transClick.alpha = currentTime * 2;
                yield return null;
            }
            transClick.alpha = 1;
            while (click)
            {
                yield return null;
            }
        }
        else
        {
            if (!drawOn)
            {
                while (!drawOn)
                {
                    yield return null;
                }
            }
            while (drawOn)
            {
                yield return null;
            }
        }
        currentTime = 0;
        while (currentTime <= 0.25f)
        {
            currentTime += Time.deltaTime;
            transClick.alpha -= currentTime * 4;
            transPanel.alpha = 1 - currentTime * 4;
            yield return null;
        }
        transPanel.alpha = 0;
        transClick.alpha = 0;
        transClick.gameObject.GetComponent<TextMeshProUGUI>().text = "Click to continue";
        transText.fontSize = 6;
        transClick.transform.localPosition = new Vector2(transClick.transform.localPosition.x, -17);
        transText.transform.localPosition = new Vector2(transText.transform.localPosition.x, 15);
        transTitle.transform.localPosition = new Vector2(transTitle.transform.localPosition.x, 28);
        transSubTitle.transform.localPosition = new Vector2(transSubTitle.transform.localPosition.x, transSubTitle.transform.localPosition.y - 1.5f);
        panelSize.sizeDelta = new Vector2(panelSize.sizeDelta.x, panelSize.sizeDelta.y - 14);
        transPanel.transform.localPosition = new Vector2(transPanel.transform.localPosition.x, transPanel.transform.localPosition.y + 3);
        transPanel.gameObject.SetActive(false);
        click = false;
        yield break;
    }
    IEnumerator DisplayMenu()
    {
        if (confirmOn)
        {
            PlaySound(clips[12], 0.25f, 2);
            menuOn = false;
            confirmOn = false;
            float prodAlpha = prodStats.alpha;
            menu.gameObject.SetActive(false);
            float currentTime = 0;
            while (currentTime <= 0.5f)
            {
                menuButton.eulerAngles = new Vector3(0, 0, Mathf.Lerp(180, 0, currentTime * 2));
                currentTime += Time.deltaTime;
                prodStats.alpha = currentTime * 2;
                menu.alpha = Mathf.Lerp(prodAlpha, 0, currentTime * 2);
                confirm.alpha = 1 - currentTime * 2;
                yield return null;
            }
            menuButton.eulerAngles = new Vector3(0, 0, 0);
            prodStats.alpha = 1;
            confirm.alpha = 0;
            menu.alpha = 0;
            confirm.gameObject.SetActive(false);
        }
        else if (!menuOn)
        {
            PlaySound(clips[11], 0.25f, 2);
            menuOn = true;
            float confirmAlpha = confirm.alpha;
            float prodAlpha = prodStats.alpha;
            menu.gameObject.SetActive(true);
            float currentTime = 0;
            while (currentTime <= 0.5f && menuOn)
            {
                menuButton.eulerAngles = new Vector3(0, 0, Mathf.Lerp(0, 180, currentTime * 2));
                currentTime += Time.deltaTime;
                confirm.alpha = Mathf.Lerp(confirmAlpha, 0, currentTime * 2);
                prodStats.alpha = Mathf.Lerp(prodAlpha, 0, currentTime * 2);
                menu.alpha = currentTime * 2;
                yield return null;
            }
            if (menuOn)
            {
                menuButton.eulerAngles = new Vector3(0, 0, 180);
                prodStats.alpha = 0;
                confirm.alpha = 0;
                menu.alpha = 1;
            }
        }
        else if (menuOn)
        {
            PlaySound(clips[12], 0.25f, 2);
            menuOn = false;
            float confirmAlpha = confirm.alpha;
            float menuAlpha = menu.alpha;
            float currentTime = 0;
            while (currentTime <= 0.5f && !menuOn)
            {
                menuButton.eulerAngles = new Vector3(0, 0, Mathf.Lerp(180, 0, currentTime * 2));
                confirm.alpha = Mathf.Lerp(confirmAlpha, 0, currentTime * 2);
                menu.alpha = Mathf.Lerp(menuAlpha, 0, currentTime * 2);
                prodStats.alpha = currentTime * 2;
                currentTime += Time.deltaTime;
                menu.alpha = 1 - currentTime * 2;
                yield return null;
            }
            if (!menuOn)
            {
                menuButton.eulerAngles = new Vector3(0, 0, 0);
                prodStats.alpha = 1;
                confirm.alpha = 0;
                menu.alpha = 0;
                menu.gameObject.SetActive(false);
            }
        }
        yield break;
    }
    public void ChoiceYes()
    {
        if (choice == 0)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
            bf.Serialize(file, save);
            file.Close();
            Application.Quit();
        }
        if (choice == 1)
        {
            File.Delete(Application.persistentDataPath + "/gamesave.save");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (choice == 3)
        {
            StartCoroutine(EndGame());
        }
    }
    public IEnumerator ConfirmChoice()
    {
        PlaySound(clips[10], 0.25f, 1);
        if (menuOn)
        {
            prodStats.alpha = 0;
            confirm.alpha = 0;
            menu.alpha = 1;
            menuOn = true;
        }
        menu.gameObject.SetActive(true);
        if (choice == 3)
        {
            confirmText.text = "Do you want to\nend the game?";
        }
        else if (choice == 0)
        {
            confirmText.text = "Do you want to\nquit the game?";
        }
        else
        {
            confirmText.text = "Your game will be erased. Are you sure?";
        }
        confirmOn = true;
        confirm.gameObject.SetActive(true);
        float menuAlpha = menu.alpha;
        float prodAlpha = prodStats.alpha;
        float currentTime = 0;
        while (currentTime <= 0.5f)
        {
            currentTime += Time.deltaTime;
            prodStats.alpha = Mathf.Lerp(prodAlpha, 0, currentTime * 2);
            menu.alpha = Mathf.Lerp(menuAlpha, 0, currentTime * 2);
            confirm.alpha = 1 - currentTime * 2;
            confirm.alpha = currentTime * 2;
            yield return null;
        }
        prodStats.alpha = 0;
        menu.alpha = 0;
        confirm.alpha = 1;
        yield break;
    }

    #endregion
    // Functions Save & Load
    #region Save & Load
    public void ResetGame()
    {
        choice = 1;
        StartCoroutine(ConfirmChoice());
    }
    void SaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, save);
        file.Close();
    }
    void LoadGame()
    {
        gameOn = false;
        StartCoroutine(EndLoad());
        // Set Panel
        transParagraph.alpha = 1;
        transPanel.transform.localPosition = new Vector2(37, -54);
        transClick.transform.localPosition = new Vector2(transClick.transform.localPosition.x, -17);
        transText.transform.localPosition = new Vector2(transText.transform.localPosition.x, 15);
        transTitle.transform.localPosition = new Vector2(transTitle.transform.localPosition.x, 28);
        transTitle.alpha = 0;
        transSubTitle.alpha = 0;
        RectTransform rect = transPanel.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, 90);
        //transBackground.transform.SetParent(transPanel.transform);
        transBackground.gameObject.SetActive(false);
        transPanel.alpha = 0;
        transPanel.gameObject.SetActive(false);
        // Load Save
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
        save = (Save)bf.Deserialize(file);
        file.Close();
        // Set Domains
        for (int i = 0; i < save.freeDomains.Count; i++)
        {
            ArtistList tempArtistDomainLocked = artistDomainLocked[save.freeDomains[i]];
            ArtistList tempArtistDomainAvailable = artistDomainAvailable[save.freeDomains[i]];
            ArtistList tempArtistDomainActive = artistDomainActive[save.freeDomains[i]];
            ArtistList tempShamefulActive = shamefulInfluenceAvailable[save.freeDomains[i]];
            string tempDomain = domain[save.freeDomains[i]];
            string tempDomainShortened = domainShortened[save.freeDomains[i]];
            artistDomainLocked[save.freeDomains[i]] = artistDomainLocked[save.outDomains[i]];
            artistDomainAvailable[save.freeDomains[i]] = artistDomainAvailable[save.outDomains[i]];
            artistDomainActive[save.freeDomains[i]] = artistDomainActive[save.outDomains[i]];
            shamefulInfluenceAvailable[save.freeDomains[i]] = shamefulInfluenceAvailable[save.outDomains[i]];
            domain[save.freeDomains[i]] = domain[save.outDomains[i]];
            domainShortened[save.freeDomains[i]] = domainShortened[save.outDomains[i]];
            artistDomainLocked[save.outDomains[i]] = tempArtistDomainLocked;
            artistDomainAvailable[save.outDomains[i]] = tempArtistDomainAvailable;
            artistDomainActive[save.outDomains[i]] = tempArtistDomainActive;
            shamefulInfluenceAvailable[save.outDomains[i]] = tempShamefulActive;
            domain[save.outDomains[i]] = tempDomain;
            domainShortened[save.outDomains[i]] = tempDomainShortened;
        }
        for (int y = 0; y < domainShops.Length; y++)
        {
            domainShops[y].transform.parent.Find("DomainName").GetComponent<TextMeshProUGUI>().text = artistDomainAvailable[y].name;
        }
        // Get Artists
        List<Artist> artists = new List<Artist>();
        List<int> artistsCount = new List<int>();
        List<Artist> leaders = new List<Artist>();
        //
        save.stockA = 0;
        save.stockC = 0;
        save.stockP = 0;
        //
        for (int i = 0; i < artistDomainLocked.Count; i++)
        {
            for (int y = 0; y < artistDomainLocked[i].Artist.Count; y++)
            {
                for (int x = 0; x < save.artistDomainActive[i].Count; x++)
                {
                    if (artistDomainLocked[i].Artist[y].Name == save.artistDomainActive[i][x])
                    {
                        Artist artist = artistDomainLocked[i].Artist[y];
                        artistDomainActive[i].Artist.Add(artist);
                        artists.Add(artist);
                        artistsCount.Add(i);
                        Transform newCard = GameObject.Instantiate(card, cardPlace).transform;
                        DisplayCard(artist, newCard, i, false, true);
                        AddCard(artist, newCard, i, false);
                        if (save.isArtistLead[i][x] == 1)
                        {
                            CreateTrendsetter(save.isArtistLead[i][x], artist, 0);
                        }
                        else if (save.isArtistLead[i][x] == 2)
                        {
                            leaders.Add(artist);
                        }
                    }
                }
            }
        }
        for (int i = 0; i < artistsCount.Count; i++)
        {
            artistDomainLocked[artistsCount[i]].Artist.Remove(artists[i]);
        }
        for (int i = 0; i < artistDomainActive.Count; i++)
        {
            if (artistDomainActive[i].Artist.Count > 0)
            {
                artistDomainActive[i].Artist.Sort((x, y) => x.Birth.CompareTo(y.Birth));
                for (int y = 1; y < artistDomainActive[i].Artist.Count; y++)
                {
                    artistDomainActive[i].Artist[y].Card.SetParent(artistDomainActive[i].Artist[y - 1].Card.Find("NextCard"));
                    artistDomainActive[i].Artist[y].Card.localPosition = Vector2.zero;
                }
                artistDomainActive[i].Artist[0].Card.SetParent(cardBoxes[i]);
                domainShops[i].boxMouse.AddItem(artistDomainActive[i].Artist.Count);
                artistDomainActive[i].Artist[0].Card.localPosition = new Vector2(0, 460);
            }
        }
        // Get Society Stage
        for (int i = 0; i < societies.Length - 1; i++)
        {
            if (available >= societies[i + 1].Population)
            {
                currentSociety++;
                if (i == 1 || i == 3 || i == 5 || i == 8)
                {
                    slotsList.parent.Find("Trendsetters").GetComponent<TextMeshProUGUI>().text = "Global Trendsetters";
                    trendSlots[0].SetActive(true);
                    headerDrops.Add(trendSlots[0].transform);
                    trendSlots.Remove(trendSlots[0]);
                    headerLock.Add(false);
                    save.headerLock.Add(false);
                }
            }
        }
        name1.text = societies[currentSociety].Name;
        for (int i = 0; i < leaders.Count; i++)
        {
            if (i < headerDrops.Count)
            {
                CreateTrendsetter(2, leaders[i], i);
            }
        }
        // Get Scumbags
        for (int i = 0; i < save.shamefulInfluenceActive.Count; i++)
        {
            for (int y = 0; y < shamefulInfluenceLocked.Artist.Count; y++)
            {
                if (shamefulInfluenceLocked.Artist[y].Name == save.shamefulInfluenceActive[i])
                {
                    Artist shame = shamefulInfluenceLocked.Artist[y];
                    shamefulInfluenceActive.Artist.Add(shame);
                    shamefulInfluenceLocked.Artist.Remove(shame);
                    Transform newCard = GameObject.Instantiate(shameCard, cardPlace).transform;
                    DisplayCard(shame, newCard, y, true, true);
                    AddCard(shame, newCard, y, true);
                }
            }
        }
        cardBoxShame.parent.GetComponent<BoxMouse>().AddItem(shamefulInfluenceActive.Artist.Count);
        shamefulInfluenceActive.Artist.Sort((x, y) => x.Birth.CompareTo(y.Birth));
        if (shamefulInfluenceActive.Artist.Count > 0)
        {
            shamefulInfluenceActive.Artist[0].Card.SetParent(cardBoxShame);
            shamefulInfluenceActive.Artist[0].Card.localPosition = new Vector2(0, 494);
            for (int i = 1; i < shamefulInfluenceActive.Artist.Count; i++)
            {
                shamefulInfluenceActive.Artist[i].Card.SetParent(shamefulInfluenceActive.Artist[i - 1].Card.Find("NextCard"));
                shamefulInfluenceActive.Artist[i].Card.localPosition = Vector2.zero;
            }
        }
        // Get Eras
        int maxDomain = 1;
        for (int i = 0; i < domainShops.Length; i++)
        {
            erasPassed += save.eraList[i];
            domainShops[i].era = save.eraList[i];
            if (save.eraList[i] > 0)
            {
                scrollTexts[i + 1].text = artistDomainAvailable[i].name;
                scrollTexts[i + 1].color = domainColors[i];
                if (i > maxDomain)
                {
                    maxDomain = i;
                }
            }
            for (int y = 0; y < artistDomainLocked[i].Artist.Count; y++)
            {
                if (artistDomainLocked[i].Artist[y].Era <= domainShops[i].era)
                {
                    Artist artist = artistDomainLocked[i].Artist[y];
                    artistDomainAvailable[i].Artist.Add(artist);
                    artistDomainLocked[i].Artist.Remove(artist);
                    y--;
                }
            }
            domainShops[i].availableCards = (domainShops[i].era * 10) - artistDomainActive[i].Artist.Count;
            domainShops[i].Initiate(i, artistDomainLocked[i].Artist.Count.ToString());
            for (int y = 0; y < shamefulInfluenceLocked.Artist.Count; y++)
            {
                if (shamefulInfluenceLocked.Artist[y].Domain == artistDomainLocked[i].name)
                {
                    if (shamefulInfluenceLocked.Artist[y].Era <= domainShops[i].era)
                    {
                        shamefulInfluenceAvailable[i].Artist.Add(shamefulInfluenceLocked.Artist[y]);
                        shamefulInfluenceLocked.Artist.Remove(shamefulInfluenceLocked.Artist[y]);
                    }
                }
            }
            if (artistDomainAvailable[i].Artist.Count > 0)
            {
                domainShops[i].cardShadow.SetActive(false);
            }
            else
            {
                domainShops[i].cardShadow.SetActive(true);
            }
        }
        // Get Items
        int itemsCount = 0;
        for (int i = 0; i < save.items.Length; i++)
        {
            if (save.itemsAvailable[i])
            {
                DisplayItem(i);
                if (save.items[i] != 0)
                {
                    availableItems[i].Level = save.items[i] - 1;
                    BuyItem(availableItems[i], i);
                    itemsCount += 1;
                }
            }
        }
        cardBoxItems.parent.GetComponent<BoxMouse>().AddItem(itemsCount);
        int bonusNumber = 0;
        int childCount = 0;
        for (int i = 0; i < bonuses.Length; i++)
        {
            if (bonuses[i] != null)
            {
                bonusNumber++;
                childCount = bonuses[i].Item.transform.parent.childCount;
            }
        }
        if (bonusNumber > 0)
        {
            for (int i = 0; i < bonuses.Length; i++)
            {
                if (bonuses[i] != null)
                {
                    bonuses[i].Item.transform.SetSiblingIndex(childCount - (i));
                }
            }
        }
        // Set Game
        stockC = save.stockC;
        stockA = save.stockA;
        stockP = save.stockP;
        if (!save.audioOn)
        {
            Audio();
        }
        if (!save.displayOn)
        {
            ToggleDisplay();
        }
        if (!save.noiseOn)
        {
            ToggleNoise();
        }
        if (!save.starsOn)
        {
            ToggleStars();
        }
        if (!save.screenOn)
        {
            screenOn = true;
            ToggleScreen();
        }
        if (maxDomain >= 19)
        {
            scrollTexts[21].text = "--- ";
        }
        if (maxDomain > 2)
        {
            domainScroll = maxDomain;
        }
        else
        {
            domainScroll = 2;
        }
        if (save.newsEra > 1)
        {
            newsEra = save.newsEra;
        }
        else
        {
            newsEra = 1;
        }
        newsChange = true;
        blindEyes = save.blindEyes;
        for (int i = 0; i < domainLock.Length; i++)
        {
            domainLock[i] = save.domainLock[i];
        }
        for (int i = 0; i < save.headerLock.Count; i++)
        {
            if (headerLock.Count < i + 1)
            {
                headerLock.Add(save.headerLock[i]);
            }
            else
            {
                headerLock[i] = save.headerLock[i];
            }
        }
        for (int i = 0; i < introPanels.Count; i++)
        {
            introPanels[i].gameObject.SetActive(false);
        }
        Calculation();
        // Bonus Price
        for (int i = 0; i < bonuses.Length; i++)
        {
            if (bonuses[i] != null)
            {
                if (bonuses[i].Level < 4)
                {
                    bonuses[i].Price = BonusPrice(i);
                }
            }
        }
        GameObject.Find("IntroSafe").SetActive(false);
        GameObject.Find("DomainSort").GetComponent<Scroll>().mouse = true;
        introCards.gameObject.SetActive(false);
        drawing = false;
        gameOn = true;
        StartCoroutine(ItemsFade());
    }
    void CreateTrendsetter(int lead, Artist artist, int id)
    {
        artist.Lead = lead;
        GameObject newCard = (GameObject)Instantiate(Resources.Load("FlyingCard"));
        Image imageCard = newCard.transform.Find("Image").GetComponent<Image>();
        int domainRef = 0;
        for (int i = 0; i < artistDomainAvailable.Count; i++)
        {
            if (artistDomainAvailable[i].name == artist.Domain)
            {
                domainRef = i;
            }
        }
        Color cardColor = domainColors[domainRef];
        FlyingCard fly = newCard.GetComponent<FlyingCard>();
        fly.mouse = artist.Card.Find("MouseDetector").GetComponent<MouseDetector>();
        fly.mouse.card = newCard.transform;
        fly.mouse.imageCard = imageCard;
        fly.domain = domainRef;
        string artistName = fly.mouse.artistName;
        string[] effectName = fly.mouse.effectName;
        Color[] effectColor = fly.mouse.effectColor;
        newCard.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = artistName;
        fly.mouse.dragSpawn = true;
        fly.mouse.fly = fly;
        for (int i = 0; i < effectName.Length; i++)
        {
            GameObject newEffect = (GameObject)Instantiate(Resources.Load("SmallEffect"), newCard.transform.Find("Effects"));
            newEffect.GetComponent<Image>().color = effectColor[i];
            newEffect.transform.Find("Effect Name").GetComponent<TextMeshProUGUI>().text = effectName[i];
        }
        if (lead == 1)
        {
            fly.global = false;
            newCard.transform.SetParent(domainDrops[domainRef]);
            imageCard.color = new Color(cardColor.r, cardColor.g, cardColor.b, 1);
            newCard.GetComponent<Image>().raycastTarget = true;
            imageCard.raycastTarget = false;
            newCard.transform.localScale = new Vector2(1, 1);
            newCard.transform.localPosition = new Vector3(0, 0, 0);
            leadArtists[domainRef] = artist;
            fly.mouse.cardDomainDropped = true;
        }
        else if (lead == 2)
        {
            int leadNumber = id;
            Transform parent = headerDrops[leadNumber];
            newCard.transform.SetParent(parent);
            fly.global = true;
            imageCard.color = new Color(cardColor.r, cardColor.g, cardColor.b, 1);
            imageCard.raycastTarget = true;
            newCard.transform.localScale = new Vector2(1, 1);
            newCard.transform.localPosition = new Vector3(0, 0, 0);
            supremeLeadArtists[leadNumber] = artist;
            fly.mouse.cardHeaderDropped = true;
            fly.mouse.headDropRef = leadNumber;
        }
    }

    #endregion
    // Functions Introduction
    #region Introduction
    IEnumerator Beginning()
    {
        secondDrawOn = false;
        transBackground.gameObject.SetActive(true);
        transPanel.gameObject.SetActive(true);
        transBackground.alpha = 1;
        yield return StartCoroutine(EndLoad());
        //Display Text
        click = true;
        float currentTime = 0;
        while (currentTime <= 4 && click)
        {
            currentTime += Time.deltaTime;
            transPanel.alpha = currentTime * 0.25f;
            yield return null;
        }
        transPanel.alpha = 1;
        currentTime = 0;
        while (currentTime <= 1 && click)
        {
            currentTime += Time.deltaTime;
            transClick.alpha = currentTime;
            yield return null;
        }
        transClick.alpha = 1;
        while (click)
        {
            yield return null;
        }
        drawing = true;
        currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transClick.alpha = 1 - currentTime;
            transParagraph.alpha = 1 - currentTime;
            yield return null;
        }
        transParagraph.alpha = 0;
        transClick.alpha = 0;
        transText.alignment = TextAlignmentOptions.Center;
        impactsButton.gameObject.SetActive(true);
        transText.text = "<color=#AF8B5A><size=7><b>Currents:</b></size></color>\n\nYou may select one of the 3 Painters below (right click on a card if its text is too small). Each one unlocks an artistic <color=#AF8B5A><b>Current</b></color>, and the other two will be available later.\n\nArtists influence one another. At the bottom of each card, you will find the different <color=#AF8B5A><b>Effects</b></color> of the Artist it represents. These may affect any of your society's <color=#AF8B5A><b>Impacts</b></color>.";
        currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transParagraph.alpha = currentTime;
            yield return null;
        }
        transParagraph.alpha = 1;
        currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            impactsButton.alpha = currentTime;
            yield return null;
        }
        impactsButton.alpha = 1;
        // Change Era
        domainShops[0].era = 1;
        save.eraList[0] = 1;
        domainShops[0].availableCards += 9;
        erasPassed++;
        statisticTexts[2].text = "<b>Eras:</b> " + erasPassed + "%";
        for (int i = 0; i < artistDomainLocked[0].Artist.Count; i++)
        {
            if (artistDomainLocked[0].Artist[i].Era == domainShops[0].era)
            {
                artistDomainAvailable[0].Artist.Add(artistDomainLocked[0].Artist[i]);
                artistDomainLocked[0].Artist.Remove(artistDomainLocked[0].Artist[i]);
                i--;
            }
        }
        scrollTexts[1].text = artistDomainAvailable[0].name;
        scrollTexts[1].color = domainColors[0];
        // Draw Cards
        Artist[] firstDraw = new Artist[3];
        for (int i = 0; i < firstDraw.Length; i++)
        {
            firstDraw[i] = artistDomainAvailable[0].Artist[i];
            firstCards.Add(GameObject.Instantiate(card, transBackground.transform).transform);
            DisplayCard(artistDomainAvailable[0].Artist[i], firstCards[i], 0, false, false);
            firstCards[i].Find("ProductionC").gameObject.SetActive(false);
            firstCards[i].Find("ProductionA").gameObject.SetActive(false);
            firstCards[i].Find("ProductionP").gameObject.SetActive(false);
            artistDomainAvailable[0].Artist[i].Locked = false;
            CardSelection detector = firstCards[i].GetComponent<CardSelection>();
            detector.unlockText = GameObject.Instantiate(unlockText, firstCards[i]);
            if (firstDraw[i].Name == "Katsushika Hokusai")
            {
                detector.unlockText.GetComponent<TextMeshProUGUI>().text = "Unlocks Modern Ornament";
            }
            else if (firstDraw[i].Name == "Adélaïde Labille-Guiard\n")
            {
                detector.unlockText.GetComponent<TextMeshProUGUI>().text = "Unlocks Neoclassicism";
            }
            else if (firstDraw[i].Name == "Francisco Goya")
            {
                detector.unlockText.GetComponent<TextMeshProUGUI>().text = "Unlocks Romanticism";
            }
        }
        // Display Cards
        PlaySound(melodies[0], 1f, 3);
        StartCoroutine(SpreadCards(firstDraw.Length, firstCards, false));
        for (int i = 0; i < firstDraw.Length; i++)
        {
            firstCards[i].transform.SetParent(introCards);
        }
        firstDrawOn = true;
        yield break;
    }
    public IEnumerator Beginning2(Artist artist, Transform artistCard)
    {
        drawing = true;
        firstDrawOn = false;
        // Set Stats
        save.artistDomainActive[0].Add(artist.Name);
        save.isArtistLead[0].Add(artist.Lead);
        artistDomainActive[0].Artist.Add(artist);
        EraPriceDisplay(domainShops[0]);
        domainShops[0].cardShadow.SetActive(false);
        // Add Card
        artistDomainAvailable[0].Artist.Remove(artist);
        AddCard(artist, artistCard, 0, false);
        domainShops[0].boxMouse.AddItem(artistDomainActive[0].Artist.Count);
        domainShops[0].cardText.text = domainShops[0].availableCards.ToString();
        domainShops[0].cardText2.text = artistDomainLocked[0].Artist.Count.ToString();
        artistCard.Find("ProductionC").gameObject.SetActive(true);
        artistCard.Find("ProductionA").gameObject.SetActive(true);
        artistCard.Find("ProductionP").gameObject.SetActive(true);
        //Buy Currents
        cardBoxItems.parent.GetComponent<BoxMouse>().AddItem(itemNumber);
        if (artist.Name == "Katsushika Hokusai")
        {
            DisplayItem(5);
            BuyItem(availableItems[5], 5);
        }
        else if (artist.Name == "Adélaïde Labille-Guiard\n")
        {
            BuyItem(availableItems[0], 0);
        }
        else if (artist.Name == "Francisco Goya")
        {
            BuyItem(availableItems[1], 1);
        }
        // Hide Cards
        StartCoroutine(HideCards(artistCard, artistCard, firstCards));
        yield return new WaitForSeconds(1);
        // Set Cards
        artistCard.SetParent(cardBoxes[0]);
        Vector2 destination = new Vector2(0, 460);
        artistCard.localPosition = destination;
        for (int i = 0; i < firstCards.Count; i++)
        {
            if (firstCards[i] != artistCard)
            {
                GameObject.Destroy(firstCards[i].gameObject);
            }
        }
        GameObject detectorObject = artistCard.Find("MouseDetector").gameObject;
        detectorObject.SetActive(true);
        MouseDetector detector = detectorObject.GetComponent<MouseDetector>();
        detector.nextCard = artistCard.Find("NextCard").transform;
        detector.currentCard = artistCard;
        //Change Text
        float currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transParagraph.alpha = 1 - currentTime;
            yield return null;
        }
        transParagraph.alpha = 0;
        transText.text = "<color=#AF8B5A><size=7><b>Domains:\n\n</b></size></color>You may select <color=#AF8B5A><b>2</b></color> of the 4 Artists below. Each one unlocks a different <color=#AF8B5A><b>Domain</b></color>, and the others will be available later.\n\nAlong with the painter you have just picked, these Artists will form the founding core of the <color=#AF8B5A><b>New Arts Circle</b></color>.";
        currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transParagraph.alpha = currentTime;
            yield return null;
        }
        transParagraph.alpha = 1;
        // Draw Cards
        Artist[] secondDraw = new Artist[4];
        borders = new GameObject[4];
        secondDraw[0] = artistDomainLocked[1].Artist[0];
        secondDraw[1] = artistDomainLocked[2].Artist[0];
        secondDraw[2] = artistDomainLocked[3].Artist[0];
        secondDraw[3] = artistDomainLocked[5].Artist[0];
        for (int i = 0; i < secondDraw.Length; i++)
        {
            borders[i] = GameObject.Instantiate(border, introCards);
            secondCards.Add(GameObject.Instantiate(card, transBackground.transform).transform);
            secondDraw[i].Card = secondCards[i];
            int domain = 0;
            for (int x = 0; x < artistDomainActive.Count; x++)
            {
                if (secondDraw[i].Domain == artistDomainActive[x].name)
                {
                    domain = x;
                }
            }
            DisplayCard(secondDraw[i], secondCards[i], domain, false, false);
            secondCards[i].Find("ProductionC").gameObject.SetActive(false);
            secondCards[i].Find("ProductionA").gameObject.SetActive(false);
            secondCards[i].Find("ProductionP").gameObject.SetActive(false);
            secondDraw[i].Locked = false;
            CardSelection detect = secondCards[i].GetComponent<CardSelection>();
            detect.border = borders[i];
            borders[i].transform.SetParent(secondCards[i]);
            borders[i].transform.position = secondCards[i].position;
            borders[i].transform.SetSiblingIndex(0);
            detect.unlockText = GameObject.Instantiate(unlockText, secondCards[i]);
            if (secondDraw[i].Name == "William Blake")
            {
                detect.unlockText.GetComponent<TextMeshProUGUI>().text = "Unlocks Poetry";
            }
            else if (secondDraw[i].Name == "Johann Wolfgang von Goethe\n")
            {
                detect.unlockText.GetComponent<TextMeshProUGUI>().text = "Unlocks Theatre";
            }
            else if (secondDraw[i].Name == "Antonio Canova")
            {
                detect.unlockText.GetComponent<TextMeshProUGUI>().text = "Unlocks Sculpture";
            }
            else if (secondDraw[i].Name == "Germaine de Staël")
            {
                detect.unlockText.GetComponent<TextMeshProUGUI>().text = "Unlocks Literature";
            }
        }
        // Display Cards
        PlaySound(melodies[1], 1f, 3);
        StartCoroutine(SpreadCards(secondDraw.Length, secondCards, false));
        for (int i = 0; i < secondDraw.Length; i++)
        {
            secondCards[i].transform.SetParent(introCards);
        }
        secondDrawOn = true;
        yield break;
    }
    public void Beginning3()
    {
        PlaySound(clips[11], 1f, 1);
        validate.gameObject.SetActive(false);
        for (int i = 0; i < borders.Length; i++)
        {
            GameObject.Destroy(borders[i].gameObject);
        }
        drawing = true;
        secondDrawOn = false;
        StartCoroutine(Beginning4());
    }
    IEnumerator Beginning4()
    {
        // Hide Cards
        yield return StartCoroutine(HideCards(selected[0].Card, selected[1].Card, secondCards));
        int domainMax = 0;
        float currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transPanel.alpha = 1 - currentTime;
            impacts.alpha = 1 - currentTime;
            yield return null;
        }
        impactsButton.gameObject.SetActive(false);
        impacts.gameObject.SetActive(false);
        // Swap Domains
        List<int> freeDomains = new List<int>();
        List<int> outDomains = new List<int>();
        if (selected[0].Domain != "Poetry" && selected[1].Domain != "Poetry") { freeDomains.Add(1); }
        if (selected[0].Domain != "Theatre" && selected[1].Domain != "Theatre") { freeDomains.Add(2); }
        if (selected[0].Domain == "Sculpture" || selected[0].Domain == "Narrative Literature")
        {
            int dom = 0;
            for (int i = 0; i < domain.Count; i++)
            {
                if (selected[0].Domain == domain[i])
                {
                    dom = i;
                }
            }
            outDomains.Add(dom);
        }
        if (selected[1].Domain == "Sculpture" || selected[1].Domain == "Narrative Literature")
        {
            int dom = 0;
            for (int i = 0; i < domain.Count; i++)
            {
                if (selected[1].Domain == domain[i])
                {
                    dom = i;
                }
            }
            outDomains.Add(dom);
        }
        for (int i = 0; i < freeDomains.Count; i++)
        {
            ArtistList tempArtistDomainLocked = artistDomainLocked[freeDomains[i]];
            ArtistList tempArtistDomainAvailable = artistDomainAvailable[freeDomains[i]];
            ArtistList tempArtistDomainActive = artistDomainActive[freeDomains[i]];
            ArtistList tempShamefulActive = shamefulInfluenceAvailable[freeDomains[i]];
            string tempDomain = domain[freeDomains[i]];
            string tempDomainShortened = domainShortened[freeDomains[i]];
            artistDomainLocked[freeDomains[i]] = artistDomainLocked[outDomains[i]];
            artistDomainAvailable[freeDomains[i]] = artistDomainAvailable[outDomains[i]];
            artistDomainActive[freeDomains[i]] = artistDomainActive[outDomains[i]];
            shamefulInfluenceAvailable[freeDomains[i]] = shamefulInfluenceAvailable[outDomains[i]];
            domain[freeDomains[i]] = domain[outDomains[i]];
            domainShortened[freeDomains[i]] = domainShortened[outDomains[i]];
            artistDomainLocked[outDomains[i]] = tempArtistDomainLocked;
            artistDomainAvailable[outDomains[i]] = tempArtistDomainAvailable;
            artistDomainActive[outDomains[i]] = tempArtistDomainActive;
            shamefulInfluenceAvailable[outDomains[i]] = tempShamefulActive;
            domain[outDomains[i]] = tempDomain;
            domainShortened[outDomains[i]] = tempDomainShortened;
        }
        for (int y = 0; y < domainShops.Length; y++)
        {
            domainShops[y].transform.parent.Find("DomainName").GetComponent<TextMeshProUGUI>().text = artistDomainAvailable[y].name;
        }
        save.freeDomains = freeDomains;
        save.outDomains = outDomains;
        // Read Cards
        for (int y = 0; y < selected.Count; y++)
        {
            // Define domain
            int domain = 0;
            for (int x = 0; x < artistDomainActive.Count; x++)
            {
                if (selected[y].Domain == artistDomainActive[x].name)
                {
                    domain = x;
                    if (domain > domainMax)
                    {
                        domainMax = domain;
                    }
                }
            }
            // Set Stats
            domainShops[domain].era = 1;
            EraChange(domain, domainShops[domain]);
            artistDomainActive[domain].Artist.Add(selected[y]);
            save.artistDomainActive[domain].Add(selected[y].Name);
            save.isArtistLead[domain].Add(selected[y].Lead);
            artistDomainAvailable[domain].Artist.Remove(selected[y]);
            // Add Card
            Transform artistCard = selected[y].Card;
            AddCard(selected[y], artistCard, domain, false);
            domainShops[domain].boxMouse.AddItem(artistDomainActive[domain].Artist.Count);
            domainShops[domain].cardText.text = domainShops[domain].availableCards.ToString();
            domainShops[domain].cardText2.text = artistDomainLocked[domain].Artist.Count.ToString();
            artistCard.Find("ProductionC").gameObject.SetActive(true);
            artistCard.Find("ProductionA").gameObject.SetActive(true);
            artistCard.Find("ProductionP").gameObject.SetActive(true);
            // Set Cards
            artistCard.SetParent(cardBoxes[domain]);
            Vector2 destination = new Vector2(0, 460);
            artistCard.localPosition = destination;
            for (int i = 0; i < secondCards.Count; i++)
            {
                if (secondCards[i] != selected[0].Card && secondCards[i] != selected[1].Card)
                {
                    GameObject.Destroy(secondCards[i].gameObject);
                }
            }
            GameObject detectorObject = artistCard.Find("MouseDetector").gameObject;
            detectorObject.SetActive(true);
            MouseDetector detector = detectorObject.GetComponent<MouseDetector>();
            detector.nextCard = artistCard.Find("NextCard").transform;
            detector.currentCard = artistCard;
        }
        domainScroll = domainMax;
        Calculation();
        StartCoroutine(ItemsFade());
        StartCoroutine(Information());
        yield break;
    }
    IEnumerator Information()
    {
        float currentTime;
        transParagraph.alpha = 1;
        transPanel.transform.localPosition = new Vector2(37, -54);
        transClick.transform.localPosition = new Vector2(transClick.transform.localPosition.x, -24);
        transText.transform.localPosition = new Vector2(transText.transform.localPosition.x, 6);
        transTitle.transform.localPosition = new Vector2(transTitle.transform.localPosition.x, 28);
        transTitle.alpha = 0;
        transSubTitle.alpha = 0;
        RectTransform rect = transPanel.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, 90);
        transText.fontSize = 7.5f;
        transBackground.alpha = 1;
        transText.alpha = 0;
        transClick.alpha = 0;
        transPanel.alpha = 1;
        transText.text = "Let's take a look at our group now.";
        currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transPanel.alpha = currentTime;
            introMenu.alpha = currentTime;
            transClick.alpha = currentTime;
            transText.alpha = currentTime;
            yield return null;
        }
        transPanel.alpha = 1;
        introMenu.alpha = 0;
        transText.alpha = 1;
        transClick.alpha = 1;
        click = true;
        while (click) { yield return null; }
        timeIntro = 0;
        while (timeIntro < 1)
        {
            timeIntro += Time.deltaTime;
            transClick.alpha = 1 - timeIntro;
            transText.alpha = 1 - timeIntro;
            yield return null;
        }
        transText.alpha = 1;
        Transform highlightCard = GameObject.Instantiate(highLight, domainShops[0].transform.GetChild(0)).transform;
        Transform highlightTrend = GameObject.Instantiate(highLight, domainShops[0].transform.GetChild(0)).transform;
        Transform highlightEra = GameObject.Instantiate(highLight, domainShops[0].transform.GetChild(2)).transform;
        highlightCard.SetSiblingIndex(0);
        highlightTrend.position = domainShops[0].transform.GetChild(1).position;
        highlightCard.localScale = new Vector2(0.8f, 0.8f);
        highlightEra.localScale = new Vector2(0.9f, 0.9f);
        highlightTrend.localScale = new Vector2(0.8f, 0.8f);
        highlightEra.SetSiblingIndex(0);
        float factor = 0.3f;
        float limitPlus = 1.2f;
        CanvasGroup previous = introMenu.transform.GetChild(0).GetComponent<CanvasGroup>();
        bool ascending = true;
        introMenu.gameObject.SetActive(true);
        while (infoOn)
        {
            for (infoRef = 0; infoRef < introTexts.Count; infoRef++)
            {
                if (!goingOn && reversing)
                {
                    infoRef -= 2;
                    if (infoRef == 0)
                    {
                        introButton = false;
                    }
                    else
                    {
                        int prevRef = infoRef - 1;
                        transText.alpha = 0;
                        transClick.alpha = 0;
                        highlightTrend.localScale = new Vector2(1f, 1f);
                        highlightTrend.gameObject.SetActive(false);
                        highlightEra.localScale = new Vector2(1f, 1f);
                        highlightEra.gameObject.SetActive(false);
                        highlightCard.localScale = new Vector2(1f, 1f);
                        highlightCard.gameObject.SetActive(false);
                        for (int i = 0; i < introPanels.Count; i++)
                        {
                            introPanels[i].alpha = 0;
                        }
                        if (prevRef == 5 || prevRef == 7 || prevRef == 10)
                        {
                            transBackground.alpha = 1;
                        }
                        else if (prevRef == 0)
                        {
                            introPanels[0].alpha = 1;
                        }
                        else if (prevRef == 1)
                        {
                            introPanels[0].alpha = 1;
                        }
                        else if (prevRef == 2)
                        {
                            introPanels[0].alpha = 1;
                        }
                        else if (prevRef == 3)
                        {
                            introPanels[1].alpha = 1;
                        }
                        else if (prevRef == 4)
                        {
                            introPanels[2].alpha = 1;
                        }
                        else if (prevRef == 8 || prevRef == 9)
                        {
                            introPanels[5].alpha = 1;
                        }
                        else if (prevRef == 13 || prevRef == 14)
                        {
                            introPanels[6].alpha = 1;
                        }
                    }
                    reversing = false;
                }
                goingOn = true;
                while (goingOn)
                {
                    transText.text = introTexts[infoRef];
                    timeIntro = 0;
                    if (infoRef == 0)
                    {
                        introPanels[0].alpha = 1;
                        introMenu.transform.GetChild(0).gameObject.SetActive(false);
                    }
                    else if (infoRef == 1)
                    {
                        introMenu.transform.GetChild(0).gameObject.SetActive(true);
                        highlightCard.gameObject.SetActive(true);
                    }
                    else if (infoRef == 2)
                    {
                        highlightEra.gameObject.SetActive(true);
                    }
                    else if (infoRef == 3)
                    {
                        highlightTrend.gameObject.SetActive(true);
                    }
                    else if (infoRef == 4)
                    {
                        introPanels[1].alpha = 1;
                    }
                    else if (infoRef == 6)
                    {
                        introPanels[3].alpha = 1;
                    }
                    else if (infoRef == 7)
                    {
                        introPanels[4].alpha = 1;
                    }
                    else if (infoRef == 8)
                    {
                        introPanels[5].alpha = 1;
                    }
                    else if (infoRef == 11)
                    {
                        transBackground.alpha = 1;
                    }
                    else if (infoRef == 12)
                    {
                        introPanels[6].alpha = 1;
                    }
                    while (timeIntro < 1)
                    {
                        timeIntro += Time.deltaTime;
                        if (infoRef == 1)
                        {
                            previous.alpha = timeIntro;
                            if (highlightCard.localScale.x >= limitPlus)
                            {
                                ascending = false;
                            }
                            if (highlightCard.localScale.x <= 1)
                            {
                                ascending = true;
                            }
                            if (ascending)
                            {
                                highlightCard.localScale = new Vector2(highlightCard.localScale.x + Time.deltaTime * factor, highlightCard.localScale.y + Time.deltaTime * factor);
                            }
                            else
                            {
                                highlightCard.localScale = new Vector2(highlightCard.localScale.x - Time.deltaTime * factor, highlightCard.localScale.y - Time.deltaTime * factor);
                            }
                        }
                        else if (infoRef == 2)
                        {
                            if (highlightEra.localScale.x > limitPlus)
                            {
                                ascending = false;
                            }
                            if (highlightEra.localScale.x < 1)
                            {
                                ascending = true;
                            }
                            if (ascending)
                            {
                                highlightEra.localScale = new Vector2(highlightEra.localScale.x + Time.deltaTime * factor, highlightEra.localScale.y + Time.deltaTime * factor);
                            }
                            else
                            {
                                highlightEra.localScale = new Vector2(highlightEra.localScale.x - Time.deltaTime * factor, highlightEra.localScale.y - Time.deltaTime * factor);
                            }
                        }
                        else if (infoRef == 3)
                        {
                            if (highlightTrend.localScale.x > 1f)
                            {
                                ascending = false;
                            }
                            if (highlightTrend.localScale.x < 0.8f)
                            {
                                ascending = true;
                            }
                            if (ascending)
                            {
                                highlightTrend.localScale = new Vector2(highlightTrend.localScale.x + Time.deltaTime * factor, highlightTrend.localScale.y + Time.deltaTime * factor);
                            }
                            else
                            {
                                highlightTrend.localScale = new Vector2(highlightTrend.localScale.x - Time.deltaTime * factor, highlightTrend.localScale.y - Time.deltaTime * factor);
                            }
                        }
                        else if (infoRef == 6 || infoRef == 8 || infoRef == 12 || infoRef == 0)
                        {
                            if (infoRef == 0)
                            {
                                introMenu.alpha = timeIntro;
                            }
                            transBackground.alpha = 1 - timeIntro;
                        }
                        else if (infoRef == 4)
                        {
                            introPanels[0].alpha = 1 - timeIntro;
                        }
                        else if (infoRef == 5)
                        {
                            introPanels[1].alpha = 1 - timeIntro;
                        }
                        else if (infoRef == 7)
                        {
                            introPanels[3].alpha = 1 - timeIntro;
                        }
                        if (infoRef < 11 && transBackground.alpha > 0)
                        {
                            transBackground.alpha = 1 - timeIntro;
                        }
                        transClick.alpha = timeIntro;
                        transText.alpha = timeIntro;
                        yield return null;
                    }
                    transText.alpha = 1;
                    click = true;
                    while (click)
                    {
                        if (infoRef == 1)
                        {
                            if (highlightCard.localScale.x >= limitPlus)
                            {
                                ascending = false;
                            }
                            if (highlightCard.localScale.x <= 1)
                            {
                                ascending = true;
                            }
                            if (ascending)
                            {
                                highlightCard.localScale = new Vector2(highlightCard.localScale.x + Time.deltaTime * factor, highlightCard.localScale.y + Time.deltaTime * factor);
                            }
                            else
                            {
                                highlightCard.localScale = new Vector2(highlightCard.localScale.x - Time.deltaTime * factor, highlightCard.localScale.y - Time.deltaTime * factor);
                            }
                        }
                        else if (infoRef == 2)
                        {
                            if (highlightEra.localScale.x >= limitPlus)
                            {
                                ascending = false;
                            }
                            if (highlightEra.localScale.x <= 1)
                            {
                                ascending = true;
                            }
                            if (ascending)
                            {
                                highlightEra.localScale = new Vector2(highlightEra.localScale.x + Time.deltaTime * factor, highlightEra.localScale.y + Time.deltaTime * factor);
                            }
                            else
                            {
                                highlightEra.localScale = new Vector2(highlightEra.localScale.x - Time.deltaTime * factor, highlightEra.localScale.y - Time.deltaTime * factor);
                            }
                        }
                        else if (infoRef == 3)
                        {
                            if (highlightTrend.localScale.x > 1.1f)
                            {
                                ascending = false;
                            }
                            if (highlightTrend.localScale.x < 0.9f)
                            {
                                ascending = true;
                            }
                            if (ascending)
                            {
                                highlightTrend.localScale = new Vector2(highlightTrend.localScale.x + Time.deltaTime * factor, highlightTrend.localScale.y + Time.deltaTime * factor);
                            }
                            else
                            {
                                highlightTrend.localScale = new Vector2(highlightTrend.localScale.x - Time.deltaTime * factor, highlightTrend.localScale.y - Time.deltaTime * factor);
                            }
                        }
                        yield return null;
                    }
                    timeIntro = 0;
                    while (timeIntro < 1)
                    {
                        timeIntro += Time.deltaTime;
                        transText.alpha = 1 - timeIntro;
                        transClick.alpha = 1 - timeIntro;
                        if (infoRef == 5 || infoRef == 7 || infoRef == 10 || infoRef == introTexts.Count - 1)
                        {
                            transBackground.alpha = timeIntro;
                            if (infoRef == introTexts.Count - 1)
                            {
                                introMenu.alpha = 1 - timeIntro;
                            }
                        }
                        if (infoRef == 1)
                        {
                            highlightCard.localScale = Vector2.Lerp(highlightCard.localScale, new Vector2(0.9f, 0.9f), timeIntro);
                        }
                        else if (infoRef == 2)
                        {
                            highlightEra.localScale = Vector2.Lerp(highlightEra.localScale, new Vector2(0.9f, 0.9f), timeIntro);
                        }
                        else if (infoRef == 3)
                        {
                            highlightTrend.localScale = Vector2.Lerp(highlightTrend.localScale, new Vector2(0.8f, 0.8f), timeIntro);
                            introPanels[1].alpha = timeIntro;
                        }
                        else if (infoRef == 4)
                        {
                            introPanels[2].alpha = timeIntro;
                        }
                        yield return null;
                    }
                    transText.alpha = 0;
                    if (infoRef == 5 || infoRef == 7 || infoRef == 10)
                    {
                        transBackground.alpha = 1;
                        for (int i = 0; i < introPanels.Count; i++)
                        {
                            introPanels[i].alpha = 0;
                        }
                    }
                    else if (infoRef == 1)
                    {
                        highlightCard.gameObject.SetActive(false);
                    }
                    else if (infoRef == 2)
                    {
                        highlightEra.gameObject.SetActive(false);
                    }
                    else if (infoRef == 3)
                    {
                        highlightTrend.gameObject.SetActive(false);
                    }
                    else if (infoRef == introTexts.Count - 1)
                    {
                        infoOn = false;
                    }
                    goingOn = false;
                }
            }
            yield return null;
        }
        for (int i = 0; i < introPanels.Count; i++)
        {
            introPanels[i].gameObject.SetActive(false);
        }
        introPanels.Clear();
        GameObject.Find("IntroSafe").SetActive(false);
        introMenu.gameObject.SetActive(false);
        highlightTrend.gameObject.SetActive(false);
        highlightEra.gameObject.SetActive(false);
        highlightCard.gameObject.SetActive(false);
        StartCoroutine(EndBeginning());
        yield break;
    }
    public void EndTuto()
    {
        if (goingOn)
        {
            PlaySound(clips[10], 0.25f, 2);
        }
        introButton = false;
        infoRef = introTexts.Count - 1;
        click = false;
    }
    public void PreviousTuto()
    {
        if (introButton)
        {
            PlaySound(clips[12], 0.25f, 2);
        }
        reversing = true;
        goingOn = false;
        click = false;
    }
    public void ButtonIntro()
    {
        introButton = true;
    }
    public void ButtonIntroFalse()
    {
        introButton = false;
    }
    IEnumerator EndBeginning()
    {
        // Display Text
        PlaySound(melodies[2], 1f, 3);
        transText.fontSize = 8;
        transClick.alpha = 0;
        transText.alpha = 0;
        transText.alignment = TextAlignmentOptions.Center;
        transText.text = "Everything is now set for your work to begin.";
        GameObject.Find("DomainSort").GetComponent<Scroll>().mouse = true;
        // Hide Intro
        gameOn = true;
        float currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transText.alpha = currentTime;
            yield return null;
        }
        save.loadOn = true;
        save.newsEra = 1;
        currentTime = 0;
        while (currentTime <= 2)
        {
            currentTime += Time.deltaTime;
            transBackground.alpha = 1 - currentTime * 0.5f;
            transClick.alpha = currentTime * 0.5f;
            yield return null;
        }
        transBackground.alpha = 0;
        transBackground.gameObject.SetActive(false);
        drawing = false;
        click = true;
        while (click) { yield return null; }
        currentTime = 0;
        while (currentTime <= 1)
        {
            currentTime += Time.deltaTime;
            transPanel.alpha = 1 - currentTime;
            yield return null;
        }
        transText.alpha = 0;
        transPanel.alpha = 0;
        transPanel.gameObject.SetActive(false);
        introCards.gameObject.SetActive(false);
        yield break;
    }

    #endregion
    // Functions Ending
    #region Ending
    public IEnumerator EndGame()
    {
        statisticTexts[2].text = "<b>Eras:</b> " + 101 + "%";
        sound3.loop = true;
        PlaySound(melodies[5], 0.8f, 3);
        transBackground.GetComponent<Image>().color = Color.black;
        transPanel.gameObject.SetActive(true);
        transBackground.gameObject.SetActive(true);
        transBackground.transform.SetParent(transPanel.transform.parent);
        transPanel.transform.SetSiblingIndex(transPanel.transform.parent.childCount - 1);
        RectTransform rect = transPanel.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(300, 200);
        transSubTitle.GetComponent<RectTransform>().sizeDelta = new Vector2(138.75f, transSubTitle.GetComponent<RectTransform>().sizeDelta.y);
        transPanel.transform.localPosition = new Vector2(0, 21.3f);
        transClick.transform.localPosition = new Vector2(transClick.transform.localPosition.x, -44.2f);
        transText.transform.localPosition = new Vector2(transText.transform.localPosition.x, -13.19f);
        transTitle.transform.localPosition = new Vector2(transTitle.transform.localPosition.x, 77f);
        transSubTitle.transform.localPosition = new Vector2(transSubTitle.transform.localPosition.x, -12.5f);
        transText.fontSize = 7;
        RectTransform rectText = transText.gameObject.GetComponent<RectTransform>();
        rectText.sizeDelta = new Vector2(243.41f, rectText.sizeDelta.y);
        transTitle.text = "The Time has come for the Earth to End";
        transSubTitle.text = "\"Art\" is no longer a thing.";
        transText.text = "<align=\"center\">This simulation was brought to you by the governmental department of controlled territory studies.</align>\n\nThe facts it presents took place during the last moments preceding planet EI-8972's sanitization and exploitation. It is based on the only documents retrived from the astronomical body. "
            + "The academic agent in charge of the planet's inspection found no other valuable material. The nature and purpose of the Society described in these archives is still unclear, as the \"Arts\" component remains a mystery. The present reconstitution has been elaborated in order to explore this unknown matter. "
            + "\n\n\n<align=\"center\">It is the very first of its kind.";
        transTitle.fontSize = 9;
        transSubTitle.fontSize = 7.5f;
        transText.alignment = TextAlignmentOptions.Justified;
        transTitle.alpha = 1;
        transSubTitle.alpha = 0;
        transText.alpha = 0;
        transClick.alpha = 0;
        transBackground.transform.GetChild(0).GetComponent<Image>().color = Color.black;
        transParagraph.alpha = 1;
        float currentTime = 0;
        while (currentTime <= 2)
        {
            currentTime += Time.deltaTime;
            transBackground.alpha = currentTime * 0.5f;
            yield return null;
        }
        transBackground.alpha = 1;
        for (int i = 0; i < stars.Count; i++)
        {
            stars[i].SetParent(transBackground.transform);
            starsAlpha[i].alpha = 0;
        }
        currentTime = 0;
        while (currentTime <= 2)
        {
            currentTime += Time.deltaTime;
            transPanel.alpha = currentTime * 0.5f;
            yield return null;
        }
        transPanel.alpha = 1;
        currentTime = 0;
        while (currentTime <= 2)
        {
            currentTime += Time.deltaTime;
            transSubTitle.alpha = currentTime * 0.5f;
            yield return null;
        }
        currentTime = 0;
        while (currentTime <= 4)
        {
            currentTime += Time.deltaTime;
            transText.alpha = currentTime * 0.25f;
            yield return null;
        }
        yield return new WaitForSeconds(120);
        currentTime = 0;
        while (currentTime <= 4)
        {
            currentTime += Time.deltaTime;
            transPanel.alpha = 1 - currentTime * 0.25f;
            yield return null;
        }
        yield break;
    }

    #endregion
}
