﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class ShopButton : MonoBehaviour, IDragHandler
{
    ListMaker script;
    RectTransform rect;
    ShopScroll scroll;
    Canvas canvas;
    float currentPos;
    float previousPos;
    int stage;
    void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        canvas = GameObject.Find("User Interface").GetComponent<Canvas>();
        scroll = GameObject.Find("Shop").GetComponent<ShopScroll>();
        rect = this.GetComponent<RectTransform>();
        rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, scroll.newOrigin - (stage * scroll.newInterval));
    }
    public void OnDrag(PointerEventData eventData)
    {
        if (script.focus)
        {
            currentPos += eventData.delta.y / canvas.scaleFactor;
        }
    }
    void SetPos()
    {
        currentPos = scroll.newOrigin - (stage * scroll.newInterval);
        previousPos = currentPos;
    }
    public void Update()
    {
        if (currentPos > previousPos + scroll.newInterval)
        {
            if (stage > 0)
            {
                script.PlaySound(script.clips[1], 1.5f, 2);
                stage--;
                scroll.stage--;
                scroll.currentTime = 0;
            }
            SetPos();
        }
        if (currentPos < previousPos - scroll.newInterval)
        {
            if (stage < scroll.maxStage)
            {
                script.PlaySound(script.clips[1], 1.5f, 2);
                stage++;
                scroll.stage++;
                scroll.currentTime = 0;
            }
            SetPos();
        }
        if (scroll.stage != stage)
        {
            stage = scroll.stage;
            SetPos();
        }
    }
}
