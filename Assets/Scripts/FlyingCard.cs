﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class FlyingCard : MonoBehaviour
{
    private ListMaker script;
    public MouseDetector mouse;
    Scroll scroll;
    public bool drag = false;
    public bool global = false;
    public int domain;
    float currentTime = 0.1f;
    Vector2 basisScale;
    Vector2 newScale = new Vector2(1f, 1f);
    Image ray;
    private void Start()
    {
        ray = this.gameObject.GetComponent<Image>();
        scroll = GameObject.Find("DomainSort").GetComponent<Scroll>();
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
    }
    private void Update()
    {
        if (currentTime < 0.1f)
        {
            currentTime += Time.deltaTime;
            this.transform.localScale = Vector2.Lerp(basisScale, newScale, currentTime * 10f);
        }
    }
    public void Drag()
    {
        if (!script.rightClick && script.focus)
        {
            ray.raycastTarget = false;
            if (global)
            {
                script.HeaderDropAvailable = false;
            }
            else
            {
                script.DomainDropAvailable = false;
            }
            mouse.Drag();
        }
    }
    public void Drop()
    {
        if (drag)
        {
            mouse.Drop();
        }
        drag = false;
    }
    public void In()
    {
        if (!script.drawOn && !script.drawing && !scroll.moving && mouse.artist.Lead != 0 && script.focus)
        {
            if (script.displayOn)
            {
                script.panel.gameObject.SetActive(true);
                //script.panelText.fontSize = 5f;
                if (global)
                {
                    script.panelTitle.text = "Global Trendsetter";
                    script.panelText.text = "Drag and drop an Artist here to have their Effects enhanced for each other enrolled Artist.";
                }
                else
                {
                    script.panelTitle.text = "Domain's Trendsetter";
                    script.panelText.text = script.DomainText(domain);
                }
                script.panelDisplay = true;
            }
            basisScale = this.transform.localScale;
            currentTime = 0;
            newScale = new Vector2(1.05f, 1.05f);
        }
    }
    public void Out()
    {
        basisScale = this.transform.localScale;
        currentTime = 0;
        newScale = new Vector2(1f, 1f);
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
}
