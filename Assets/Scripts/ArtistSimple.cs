﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ArtistSimple
{
    public List<GameObject> Lines = new List<GameObject>();
    public Artist ArtistDetail;
    public string ListName;
    public string ListEra;
    public int Domain;
    public int Occurences;

public ArtistSimple(Artist artist, string listName, string listEra, int domain)
    {
        ArtistDetail = artist;
        ListName = listName;
        ListEra = listEra;
        Domain = domain;
    }
}
