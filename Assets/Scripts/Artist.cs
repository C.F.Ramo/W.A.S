﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class Artist
{
    public string Name;
    public string Name2;
    public string Name3;
    public string CommonName;
    public List<string> Nation;
    public List<string> Tags;
    public List<string> Effects;
    public List<string> Currents;
    public List<string> Gender;
    public string Domain;
    public string Text;
    public int Birth;
    public int Death;
    public int Birth2;
    public int Death2;
    public int Birth3;
    public int Death3;
    public int Era;

    public List<int> nationRefs;
    public List<int> tagRefs;
    public List<int> effectRefs;
    public List<int> currentRefs;

    public Transform Card;
    public TextMeshProUGUI ProdC;
    public TextMeshProUGUI ProdA;
    public TextMeshProUGUI ProdP;
    public double ProductionC;
    public double ProductionA;
    public double ProductionP;
    public int Lead;
    public bool Locked;

    public string Shame;
    public string[] CW;

    public List<int> Colors;
    public List<string> Spes;
    public List<string> Titles;
    public List<string> Texts;
}
