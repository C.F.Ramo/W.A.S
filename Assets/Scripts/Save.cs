﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Save
{
    public List<List<string>> artistDomainActive = new List<List<string>>();
    public List<List<int>> isArtistLead = new List<List<int>>();
    public List<string> shamefulInfluenceActive = new List<string>();
    public List<int> eraList = new List<int>();
    public bool[] itemsAvailable = new bool[13];
    public int[] items = new int[13];
    public List<int> freeDomains = new List<int>();
    public List<int> outDomains = new List<int>();
    public bool[] domainLock = new bool[20];
    public List<bool> headerLock = new List<bool>();
    public int newsEra;
    public int blindEyes;
    public double stockC;
    public double stockA;
    public double stockP;
    public bool audioOn;
    public bool displayOn;
    public bool noiseOn;
    public bool starsOn;
    public bool loadOn;
    public bool screenOn;
    public int[] bonus = new int[5];
}
