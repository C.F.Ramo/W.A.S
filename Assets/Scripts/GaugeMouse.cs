﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GaugeMouse : MonoBehaviour
{
    ListMaker script;
    Image level;
    void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        level = GameObject.Find("Level").GetComponent<Image>();
    }
    public void MouseIn()
    {
        if (!script.drawOn && !script.drawing && script.focus)
        {
            if (script.displayOn)
            {
                script.panel.gameObject.SetActive(true);
                //script.panelText.fontSize = 5f;
                script.panelTitle.text = "Social Impact";
                script.panelText.text = "This gauge measures how ethical your Arts Society is. Its level affects your production. \n\nCurrent level:<b> " + Math.Round(script.globalMultiplicator, 1);
                script.panelDisplay = true;
            }
            level.color = new Color(0.42f, 0.1f, 0.1f);
        }
    }
    public void MouseOut()
    {
        level.color = new Color(0.3f, 0.08f, 0.08f);
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
}
