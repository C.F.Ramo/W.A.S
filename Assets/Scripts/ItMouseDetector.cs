﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItMouseDetector : MonoBehaviour
{
    ListMaker script;
    RectTransform detector;
    Vector2 origin;
    Vector2 originScale;
    Vector2 partlyOpen;
    Vector2 partlyOpenScale;
    float closedPos = 9.5f;
    float openPos = -72;
    public BoxMouse box;
    public Transform nextCard;
    private bool mouseOver = false;
    private bool open = false;
    public RectTransform background;
    float backOrigin = 65;
    public bool small = false;
    // MoveCards
    Vector2 position;
    Vector2 scale;
    float currentTime;
    float nextPos;
    float nDelta;
    bool isOK;
    void Awake()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        detector = this.gameObject.GetComponent<RectTransform>();
        origin = new Vector2(0, 37.3f);
        originScale = new Vector2(115, 14);
        partlyOpen = new Vector2(0, 0);
        partlyOpenScale = new Vector2(115, 88.7f);
    }
    void Update()
    {
        if (currentTime < 1f && isOK)
        {
            currentTime += Time.deltaTime;
            float sDelta = Mathf.Lerp(detector.sizeDelta.y, scale.y, currentTime);
            float nPos = Mathf.Lerp(nextCard.localPosition.y, nextPos, currentTime);
            detector.localPosition = Vector2.Lerp(detector.localPosition, position, currentTime);
            detector.sizeDelta = new Vector2(detector.sizeDelta.x, sDelta);
            nextCard.localPosition = new Vector2(nextCard.localPosition.x, nPos);
            if (!small)
            {
                background.sizeDelta = new Vector2(background.sizeDelta.x, Mathf.Lerp(background.sizeDelta.y, nDelta, currentTime));
            }
        }
    }
    public void Small()
    {
        origin = new Vector2(0, 11.64999f);
        originScale = new Vector2(115, 12.7f);
        partlyOpen = new Vector2(0, 0);
        partlyOpenScale = new Vector2(115, 36.17F);
        openPos = -50;
        closedPos = -15;
        detector.localPosition = origin;
        detector.sizeDelta = new Vector2(originScale.x, originScale.y);
        nextCard.localPosition = new Vector2(nextCard.localPosition.x, closedPos);
        small = true;
    }
    public void PointerIn()
    {
        if (!script.drawOn && script.focus)
        {
            script.PlaySound(script.clips[3], 0.75f, 1);
            mouseOver = true;
            if (!open && !box.moving)
            {
                MoveCard(partlyOpen, partlyOpenScale, openPos, 0, mouseOver);
            }
        }
    }
    public void PointerOut()
    {
        mouseOver = false;
        if (!open)
        {
            MoveCard(origin, originScale, closedPos, backOrigin, !mouseOver);
        }
    }
    public void PointerClick()
    {
        if (script.focus)
        {
            if (!open && !script.drawOn)
            {
                script.PlaySound(script.clips[13], 1f, 1);
                open = true;
            }
            else if (!script.drawOn)
            {
                script.PlaySound(script.clips[4], 0.75f, 1);
                open = false;
            }
        }
    }
    void MoveCard(Vector2 nPosition, Vector2 nScale, float nNextPos, float newDelta, bool nIsOK)
    {
        currentTime = 0;
        nDelta = newDelta;
        position = nPosition;
        scale = nScale;
        nextPos = nNextPos;
        isOK = nIsOK;
    }
}
