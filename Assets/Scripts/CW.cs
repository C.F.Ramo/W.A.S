﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CW : MonoBehaviour
{
    ListMaker script;
    CanvasGroup group;
    public MouseDetector mouse;
    void Start()
    {
        group = this.GetComponent<CanvasGroup>();
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
    }
    public void Mouse()
    {
        if (script.focus)
        {
            if (script.drawOn)
            {
                script.overDraw = true;
            }
            else if (mouse.gameObject.activeSelf)
            {
                mouse.mouseOver = true;
            }
        }
    }
    public void Click()
    {
        if (script.focus)
        {
            StartCoroutine(ClickFade());
        }
    }
    IEnumerator ClickFade()
    {
        float currentTime = 0;
        while (currentTime < 0.5f)
        {
            group.alpha = Mathf.Lerp(1, 0, currentTime * 2);
            currentTime += Time.deltaTime;
            yield return null;
        }
        this.gameObject.SetActive(false);
        yield break;
    }
}
