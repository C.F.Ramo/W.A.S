﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DomainDrop : MonoBehaviour
{
    ListMaker script;
    public int domain;
    Scroll scroll;
    void Start()
    {
        scroll = GameObject.Find("DomainSort").GetComponent<Scroll>();
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
    }
    public void MouseIn()
    {
        script.DomainDropOn(domain);
        if (!script.drawOn && script.displayOn && !script.drawing && !scroll.moving && script.focus)
        {
            script.panel.gameObject.SetActive(true);
            //script.panelText.fontSize = 5f;
            script.panelTitle.text = "Domain's Trendsetter";
            script.panelText.text = script.DomainText(domain);
            script.panelDisplay = true;
        }
    }
    public void MouseOut()
    {
        script.DomainDropOff();
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
}
