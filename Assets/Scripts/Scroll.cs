﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scroll : MonoBehaviour
{
    public ListMaker script;
    public int stage;
    public float currentTime = 0;
    public float origin;
    public float interval;
    bool check;
    public bool foreignMovement;
    float delay = 0.25f;
    float pos;
    float posY;
    float posZ;
    float speed = 2;
    float nextPos;
    float nextPosAscending;
    readonly float reference = 88.35f;
    public Transform cam;
    public bool mouse = true;
    public bool moving = false;
    Image blur;
    Sprite left;
    Sprite middle;
    Sprite right;
    private void Start()
    {
        blur = GameObject.Find("DrawBlur").GetComponent<Image>();
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        stage = 1;
        currentTime = 2.1f;
        origin = cam.position.x;
        mouse = true;
        interval = reference / 19;
        posY = cam.position.y;
        posZ = cam.position.z;
        left = Resources.Load<Sprite>("BlurLeft");
        middle = Resources.Load<Sprite>("Blur");
        right = Resources.Load<Sprite>("BlurRight");
    }
    void Update()
    {
        if (mouse && !script.drawOn && !script.drawing && script.gameOn && !script.interfaceList)
        {
            if (Input.mouseScrollDelta.y < 0 && stage < 20)
            {
                if (currentTime < delay && stage >= 18) { }
                else if (stage < script.domainScroll)
                {
                    SetMovement(true, false);
                }
            }
            else if (Input.mouseScrollDelta.y > 0 && stage > 0)
            {
                if (currentTime < delay && stage <= 1) { }
                else
                {
                    SetMovement(false, false);
                }
            }
        }
        if (stage < 20)
        {
            if (currentTime <= delay)
            {
                currentTime += Time.deltaTime;
                pos = Mathf.Lerp(cam.position.x, origin + (stage * interval), currentTime * speed);
                cam.position = new Vector3(pos, posY, posZ);
            }
            else if (!check)
            {
                moving = false;
                cam.position = new Vector3(origin + (stage * interval), posY, posZ);
                check = true;
            }
        }
        else
        {
            if (currentTime <= delay)
            {
                currentTime += Time.deltaTime;
                pos = Mathf.Lerp(cam.position.x, origin + ((stage - 1) * interval), currentTime * speed);
                cam.position = new Vector3(pos, cam.position.y, cam.position.z);
            }
            else if (!check)
            {
                moving = false;
                cam.position = new Vector3(origin + ((stage - 1) * interval), cam.position.y, cam.position.z);
                check = true;
            }
        }
    }
    public void SetMovement(bool ascending, bool exterior)
    {
        if (exterior)
        {
            foreignMovement = true;
        }
        else
        {
            foreignMovement = false;
            script.PlaySound(script.clips[0], 1f, 2);
        }
        if (ascending)
        {
            stage++;
            nextPosAscending = origin + (stage * interval);
        }
        else
        {
            stage--;
            nextPos = origin + ((stage - 1) * interval);
        }
        if (stage == 0)
        {
            blur.sprite = left;
        }
        else if (stage == 19)
        {
            blur.sprite = right;
        }
        else
        {
            blur.sprite = middle;
        }
        currentTime = 0;
        moving = true;
        check = false;
    }
    /*public void SetBoxes(int ascending, int previous)
    {
        for (int i = 0; i < script.cardBoxes.Count; i++)
        {
            if (Mathf.Abs(stage - i) > 2)
            {
                if (ascending == 0)
                {
                    if (script.cardBoxes[i].gameObject.activeSelf)
                    {
                        script.cardBoxes[i].gameObject.SetActive(false);
                    }
                }
                else if (ascending == 1)
                {
                    if (i < previous || i > stage)
                    {
                        if (script.cardBoxes[i].gameObject.activeSelf)
                        {
                            script.cardBoxes[i].gameObject.SetActive(false);
                        }
                    }
                }
                else
                {
                    if (i > previous || i < stage)
                    {
                        if (script.cardBoxes[i].gameObject.activeSelf)
                        {
                            script.cardBoxes[i].gameObject.SetActive(false);
                        }
                    }
                }
            }
            else
            {
                if (!script.cardBoxes[i].gameObject.activeSelf)
                {
                    script.cardBoxes[i].gameObject.SetActive(true);
                }
            }
        }
    }*/
}