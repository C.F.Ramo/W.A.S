﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class DomainShop : MonoBehaviour
{
    public int domain;
    public int era = 0;
    public int availableCards = 0;
    bool initialized = false;
    ListMaker script;
    public TextMeshProUGUI cardText;
    public TextMeshProUGUI cardText2;
    public TextMeshProUGUI eraText;
    public TextMeshProUGUI eraText2;
    public double price;
    double redrawPrice;
    Scroll scroll;
    float currentTime = 0.5f;
    float currentTime2 = 0.5f;
    public GameObject eraShadow;
    public GameObject cardShadow;
    public Transform cardShop;
    public Transform eraShop;
    public BoxMouse boxMouse;
    bool mouse;
    bool eraMouse;
    RectTransform drawIcon;
    RectTransform eraIcon;
    float rectOrigin = 9.643425f;
    float rectOpen = 39.17367f;
    float posOrigin = -16.153f;
    float posOpen = -1.391f;
    float currentRect;
    float currentPos;
    float basisRect;
    float basisPos;
    float currentRectEra;
    float currentPosEra;
    float basisRectEra;
    float basisPosEra;
    public int titleCount;

    public void Initiate(int Domain, string text)
    {
        domain = Domain;
        drawIcon = this.transform.GetChild(0).Find("MiddlePart").GetComponent<RectTransform>();
        //drawText = drawIcon.Find("Draw").GetComponent<TextMeshProUGUI>();
        eraIcon = this.transform.GetChild(2).Find("MiddlePart").GetComponent<RectTransform>();
        //eraButtonText = eraIcon.Find("NextEra").GetComponent<TextMeshProUGUI>();
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        scroll = GameObject.Find("DomainSort").GetComponent<Scroll>();
        cardShop = this.transform.Find("Card (1)");
        eraShop = this.transform.Find("Card (3)");
        cardText = cardShop.Find("Available").GetComponent<TextMeshProUGUI>();
        cardText2 = cardShop.Find("Locked").GetComponent<TextMeshProUGUI>();
        cardText.text = availableCards.ToString();
        cardText2.text = text;
        eraText = eraShop.Find("Present").GetComponent<TextMeshProUGUI>();
        eraText2 = eraShop.Find("Price").GetComponent<TextMeshProUGUI>();
        eraShadow = eraShop.Find("Filter").gameObject;
        cardShadow = cardShop.Find("Filter").gameObject;
        boxMouse = this.transform.parent.parent.Find("Background").GetComponent<BoxMouse>();
        price = script.EraPrice(domain, this);
        script.EraPriceDisplay(this);
        initialized = true;
    }
    private void Update()
    {
        if (initialized)
        {
            if (currentTime < 0.2f)
            {
                currentTime += Time.deltaTime;
                if (mouse)
                {
                    currentPos = Mathf.Lerp(basisPos, posOpen, currentTime * 5);
                    currentRect = Mathf.Lerp(basisRect, rectOpen, currentTime * 5);
                    //currentText = Mathf.Lerp(basisText, textOpen, currentTime * 5);
                }
                else
                {
                    currentPos = Mathf.Lerp(basisPos, posOrigin, currentTime * 5);
                    currentRect = Mathf.Lerp(basisRect, rectOrigin, currentTime * 5);
                    //currentText = Mathf.Lerp(basisText, textOrigin, currentTime * 5);
                }
                drawIcon.localPosition = new Vector2(drawIcon.localPosition.x, currentPos);
                drawIcon.sizeDelta = new Vector2(drawIcon.sizeDelta.x, currentRect);
                //drawText.fontSize = currentText;
            }
        }
        if (currentTime2 < 0.2f)
        {
            currentTime2 += Time.deltaTime;
            if (eraMouse)
            {
                if (era < 5 || script.endAvailable)
                {
                    currentPosEra = Mathf.Lerp(basisPosEra, posOpen, currentTime2 * 5);
                    currentRectEra = Mathf.Lerp(basisRectEra, rectOpen, currentTime2 * 5);
                    //currentTextEra = Mathf.Lerp(basisTextEra, textOpen - 1, currentTime2 * 5);
                }
            }
            else
            {
                currentPosEra = Mathf.Lerp(basisPosEra, posOrigin, currentTime2 * 5);
                currentRectEra = Mathf.Lerp(basisRectEra, rectOrigin, currentTime2 * 5);
                //currentTextEra = Mathf.Lerp(basisTextEra, textOrigin, currentTime2 * 5);
            }
            eraIcon.localPosition = new Vector2(drawIcon.localPosition.x, currentPosEra);
            eraIcon.sizeDelta = new Vector2(drawIcon.sizeDelta.x, currentRectEra);
            //eraButtonText.fontSize = currentTextEra;
        }
        else
        {
            if (script.stockC >= price && era < 5)
            {
                if (eraShadow.activeSelf)
                {
                    eraShadow.SetActive(false);
                }
            }
            else
            {
                if (script.endAvailable && script.stockC >= price)
                {
                    if (eraShadow.activeSelf)
                    {
                        eraShadow.SetActive(false);
                    }
                }
                else
                {
                    if (!eraShadow.activeSelf && currentTime2 >= 0.2f)
                    {
                        eraShadow.SetActive(true);
                    }
                }
            }
        }
    }
    public void CardDraw()
    {
        if (!script.drawing && !script.rightClick && !Input.GetMouseButton(1) && script.focus)
        {
            if (!script.drawOn)
            {
                if (era > 0 && availableCards > 0)
                {
                    script.drawing = true;
                    MouseOut();
                    int cardCount = 5;
                    if (availableCards < 5)
                    {
                        cardCount = availableCards;
                    }
                    StartCoroutine(script.DrawCards(domain, cardCount));
                }
                else
                {
                    script.PlaySound(script.clips[7], 1, 1);
                    script.panel.gameObject.SetActive(true);
                    script.panelTitle.text = "No Artists available!";
                    if (era == 0)
                    {
                        script.panelText.text = "You need to unlock this domain's first era before you can draw cards here.";
                    }
                    if (era < 5)
                    {
                        script.panelText.text = "You enrolled every Artist for the past eras. Try getting to the next one!";
                    }
                    else
                    {
                        script.panelText.text = "You enrolled every single Artist in this Domain. Bravo!";
                    }
                }
            }
            else
            {
                if (script.drawRef == domain)
                {
                    if (availableCards > 5)
                    {
                        if (script.stockA > redrawPrice)
                        {
                            script.drawing = true;
                            MouseOut();
                            int cardCount = 5;
                            StartCoroutine(script.DrawCards(domain, cardCount));
                            script.stockA -= redrawPrice;
                        }
                        else
                        {
                            PanelDisplay();
                            script.panelTitle.text = "Not enough <sprite index=1> !";
                            script.panelText.text = "You need " + script.RoundNumber(redrawPrice - script.stockA, true) + " more <sprite index=1> to redraw.";
                        }
                    }
                    else
                    {
                        PanelDisplay();
                        script.panelTitle.text = "Not enough cards!";
                        script.panelText.text = "You don't have enough cards available to redraw.";
                    }
                }
                else
                {
                    script.drawing = true;
                    MouseOut();
                    int cardCount = 5;
                    if (availableCards < 5)
                    {
                        cardCount = availableCards;
                    }
                    StartCoroutine(script.DrawCards(domain, cardCount));
                }
            }
        }
    }
    void PanelDisplay()
    {
        script.PlaySound(script.clips[7], 1, 1);
        script.panel.gameObject.SetActive(true);
        //script.panelText.fontSize = 5f;
        script.panel.position = new Vector3(0, 3f, 1000);
    }
    public void EraDraw()
    {
        if (!script.drawing && !script.drawOn && !script.rightClick && script.focus)
        {
            if (era < 5)
            {
                if (script.stockC >= price)
                {
                    script.PlaySound(script.clips[8], 1, 1);
                    currentTime2 = 0;
                    EraMouseOut();
                    script.stockC -= price;
                    era++;
                    availableCards += 10;
                    script.EraChange(domain, this);
                    price = script.EraPrice(domain, this);
                    script.EraPriceDisplay(this);
                    cardText.text = availableCards.ToString();
                    cardText2.text = ((5 - era) * 10).ToString();
                }
                else
                {
                    script.PlaySound(script.clips[7], 1, 1);
                    script.panel.gameObject.SetActive(true);
                    script.panelTitle.text = "Not enough <sprite index=0> !";
                    script.panelText.text = "You need " + script.RoundNumber(price - script.stockC, true) + " more <sprite index=0> before you can unlock this Era.";
                    script.panelDisplay = true;
                }
            }
            else
            {
                if (script.endAvailable)
                {
                    if (script.stockC >= price)
                    {
                        script.choice = 3;
                        StartCoroutine(script.ConfirmChoice());
                    }
                    else
                    {
                        script.PlaySound(script.clips[7], 1, 1);
                        script.panel.gameObject.SetActive(true);
                        script.panelTitle.text = "Not enough <sprite index=0> !";
                        script.panelText.text = "You need " + script.RoundNumber(price - script.stockC, true) + " more <sprite index=0> before you can go further.";
                        script.panelDisplay = true;
                    }
                }
                else
                {
                    script.PlaySound(script.clips[7], 1, 1);
                    script.panel.gameObject.SetActive(true);
                    script.panelTitle.text = "Last era reached!";
                    script.panelText.text = "Reach the last era everywhere, and we'll be able to go further.";
                    script.panelDisplay = true;
                }
            }
        }
    }
    public void MouseIn()
    {
        if (!script.drawing && !scroll.moving && script.focus)
        {
            script.overDraw = true;
            if (script.drawRef == domain && script.drawOn && availableCards > 0)
            {
                mouse = true;
                basisPos = drawIcon.localPosition.y;
                basisRect = drawIcon.sizeDelta.y;
                //basisText = drawText.fontSize;
                currentTime = 0;
                script.PlaySound(script.clips[3], 0.25f, 1);
                redrawPrice = 0;
                for (int i = 0; i < script.cards[domain].Count; i++)
                {
                    redrawPrice += script.cards[domain][0].GetComponent<CardSelection>().price;
                }
                redrawPrice = Math.Round((redrawPrice / script.cards[domain].Count) * 0.25f);
                script.panel.gameObject.SetActive(true);
                //script.panelText.fontSize = 5f;
                script.panelTitle.text = "Domain's Artists";
                script.panelText.text = "Pay " + script.RoundNumber(redrawPrice, true) + " <sprite index=1> to redraw.";
                script.panel.position = new Vector3(0, 3f, 1000);
            }
            else if (availableCards > 0)
            {
                mouse = true;
                basisPos = drawIcon.localPosition.y;
                basisRect = drawIcon.sizeDelta.y;
                //basisText = drawText.fontSize;
                currentTime = 0;
                script.PlaySound(script.clips[3], 0.25f, 1);
                if (script.displayOn && !script.drawOn)
                {
                    script.panel.gameObject.SetActive(true);
                    //script.panelText.fontSize = 5f;
                    script.panelTitle.text = "Domain's Artists";
                    script.panelText.text = "Draws 5 random Artists. You can then select one to enroll if you have enough <sprite index=1>. Clicking here again allows you to redraw for half the price of a card.";
                    script.panelDisplay = true;
                }
            }
        }
    }
    public void MouseOut()
    {
        mouse = false;
        basisPos = drawIcon.localPosition.y;
        basisRect = drawIcon.sizeDelta.y;
        //basisText = drawText.fontSize;
        currentTime = 0;
        script.panel.gameObject.SetActive(false);
        script.overDraw = false;
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
    public void EraMouseIn()
    {
        if (!script.drawOn && !script.drawing && !scroll.moving && script.focus)
        {
            if (era < 5 || script.endAvailable)
            {
                if (script.stockC >= price)
                {
                    script.PlaySound(script.clips[3], 0.25f, 1);
                    eraMouse = true;
                    basisPosEra = eraIcon.localPosition.y;
                    basisRectEra = eraIcon.sizeDelta.y;
                    //basisTextEra = eraButtonText.fontSize;
                    currentTime2 = 0;
                }
                if (script.displayOn)
                {
                    script.panel.gameObject.SetActive(true);
                    //script.panelText.fontSize = 5f;
                    if (era < 5)
                    {
                        script.panelTitle.text = "Domain's Era";
                        script.panelText.text = "The present Era for this Domain is displayed here. Clicking will unlock the next Era if you have enough <sprite index=0>. Each Era includes 10 new Artists.";
                    }
                    else
                    {
                        script.panelTitle.text = "Go further";
                        script.panelText.text = "End this simulation.";
                    }
                    script.panelDisplay = true;
                }
            }
        }
    }
    public void EraMouseOut()
    {
        eraMouse = false;
        basisPosEra = eraIcon.localPosition.y;
        basisRectEra = eraIcon.sizeDelta.y;
        //basisTextEra = eraButtonText.fontSize;
        currentTime2 = 0;
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
}
