﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupremeDrop : MonoBehaviour
{
    ListMaker script;
    void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
    }
    public void MouseIn1()
    {
        script.HeaderDropOn(0);
    }
    public void MouseOff1()
    {
        script.HeaderDropOff();
    }
    public void MouseIn2()
    {
        script.HeaderDropOn(1);
    }
    public void MouseOff2()
    {
        script.HeaderDropOff();
    }
    public void MouseIn3()
    {
        script.HeaderDropOn(2);
    }
    public void MouseOff3()
    {
        script.HeaderDropOff();
    }
    public void MouseIn4()
    {
        script.HeaderDropOn(3);
    }
    public void MouseOff4()
    {
        script.HeaderDropOff();
    }
    public void MouseIn5()
    {
        script.HeaderDropOn(4);
    }
    public void MouseOff5()
    {
        script.HeaderDropOff();
    }
}
