﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class CardScroll : MonoBehaviour, IDragHandler
{
    ListMaker script;
    RectTransform rect;
    BoxMouse scroll;
    Canvas canvas;
    public float currentPos;
    public int stage;
    public bool isGeneral;
    void Start()
    {
        if (!isGeneral)
        {
            canvas = GameObject.Find("Cards").GetComponent<Canvas>();
            scroll = this.transform.parent.parent.GetComponent<BoxMouse>();
        }
        else
        {
            canvas = GameObject.Find("User Interface").GetComponent<Canvas>();
            scroll = GameObject.Find("ArtistsList").GetComponent<BoxMouse>();
        }
        rect = this.GetComponent<RectTransform>();
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, scroll.newOrigin - (stage * scroll.newInterval));
        currentPos = scroll.newOrigin;
    }
    public void OnDrag(PointerEventData eventData)
    {
        if (script.focus) {
        scroll.drag = true;
        currentPos = Mathf.Clamp(currentPos + eventData.delta.y / canvas.scaleFactor, scroll.intervals[scroll.intervals.Length - 1], scroll.intervals[0]); }
    }
    public void Drop()
    {
        scroll.drag = false;
    }
    public void Update()
    {
        if (scroll.drag && !scroll.sizeChange)
        {
            if (currentPos > scroll.intervals[stage])
            {
                if (currentPos >= scroll.intervals[stage - 1])
                {
                    script.PlaySound(script.clips[1], 0.5f, 2);
                    int current = stage - 1;
                    for (int i = stage - 1; i > 0; i--)
                    {
                        if (currentPos >= scroll.intervals[i])
                        {
                            current = i;
                        }
                        else
                        {
                            break;
                        }
                    }
                    stage = current;
                    scroll.stage = current;
                    scroll.currentTime = 0;
                }
            }
            if (currentPos < scroll.intervals[stage])
            {
                if (currentPos <= scroll.intervals[stage + 1])
                {
                    script.PlaySound(script.clips[1], 0.5f, 2);
                    int current = stage + 1;
                    for (int i = stage + 1; i < scroll.intervals.Length - 1; i++)
                    {
                        if (currentPos <= scroll.intervals[i])
                        {
                            current = i;
                        }
                        else
                        {
                            break;
                        }
                    }
                    stage = current;
                    scroll.stage = current;
                    scroll.currentTime = 0;
                }
            }
        }
        else
        {
            if (scroll.stage != stage)
            {
                stage = scroll.stage;
            }
            if (isGeneral)
            {
                if (scroll.sizeChange)
                {
                    scroll.stage = 0;
                    stage = 0;
                    scroll.currentTime = 0;
                }
                if (!scroll.sizeChange)
                {
                    currentPos = scroll.intervals[stage];
                }
            }
        }
    }
}
