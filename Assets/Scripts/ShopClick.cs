﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ShopClick : MonoBehaviour
{
    private ListMaker script;
    public Item current;
    public Bonus currentB;
    public GameObject shadow;
    float currentTime = 0.5f;
    public bool isBonus = false;
    Vector2 newScale = new Vector2(1f, 1f);
    void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
    }
    private void Update()
    {
        if (currentTime < 0.25f)
        {
            currentTime += Time.deltaTime;
            transform.localScale = Vector2.Lerp(transform.localScale, newScale, currentTime * 4);
        }
        if (isBonus)
        {
            if (script.stockP >= currentB.Price && currentB.Price != 0)
            {
                if (shadow.activeSelf)
                {
                    shadow.SetActive(false);
                }
            }
            else
            {
                if (!shadow.activeSelf)
                {
                    shadow.SetActive(true);
                }
            }
        }
        else
        {
            if (script.stockP >= current.Price && current.Price != 0)
            {
                if (shadow.activeSelf)
                {
                    shadow.SetActive(false);
                }
            }
            else
            {
                if (!shadow.activeSelf)
                {
                    shadow.SetActive(true);
                }
            }
        }
    }
    public void Click()
    {
        if (!script.rightClick && script.focus)
        {
            if (isBonus)
            {
                if (currentB.Price == 0)
                {
                    script.PlaySound(script.clips[7], 1, 2);
                    script.panel.gameObject.SetActive(true);
                    script.panelTitle.text = "Maximum level reached!";
                    script.panelText.text = "You cannot further upgrade this Bonus.";
                }
                else if (script.stockP > currentB.Price)
                {
                    script.PlaySound(script.clips[9], 1, 2);
                    script.stockP -= currentB.Price;
                    script.BuyBonus(currentB.Ref);
                }
                else
                {
                    script.PlaySound(script.clips[7], 1, 2);
                    script.panel.gameObject.SetActive(true);
                    script.panelTitle.text = "Not enough <sprite index=2>!";
                    script.panelText.text = "You need " + script.RoundNumber(currentB.Price - script.stockP, true) + " more <sprite index=2> before you can unlock this Bonus.";
                }
            }
            else
            {
                if (current.Price == 0)
                {
                    script.PlaySound(script.clips[7], 1, 2);
                    script.panel.gameObject.SetActive(true);
                    script.panelTitle.text = "Maximum level reached!";
                    script.panelText.text = "You cannot further upgrade this Current.";
                }
                else if (script.stockP > current.Price)
                {
                    script.PlaySound(script.clips[9], 1, 2);
                    script.stockP -= current.Price;
                    script.BuyItem(current, current.Ref);
                }
                else
                {
                    script.PlaySound(script.clips[7], 1, 2);
                    script.panel.gameObject.SetActive(true);
                    script.panelTitle.text = "Not enough <sprite index=2> !";
                    script.panelText.text = "You need " + script.RoundNumber(current.Price - script.stockP, true) + " more <sprite index=2> before you can unlock this Current.";
                }
            }
        }
    }
    public void MouseIn()
    {
        if (script.focus)
        {
            script.PlaySound(script.clips[3], 0.25f, 1);
            currentTime = 0;
            newScale = new Vector2(1.1f, 1.1f);
            script.overDraw = true;
            script.panel.gameObject.SetActive(true);
            script.panelText.alignment = TMPro.TextAlignmentOptions.Center;
            if (isBonus)
            {
                if (currentB.Price == 0)
                {
                    script.panelTitle.text = currentB.Name;
                    //script.panelText.fontSize = 5.5f;
                    script.panelText.text = currentB.Description + "\nMaximum Level reached!";
                }
                else
                {
                    script.panelTitle.text = currentB.Name;
                    //script.panelText.fontSize = 5.5f;
                    script.panelText.text = currentB.Description + "\n\n<b><color=#AF8B5A>Price:</b></color> " + script.RoundNumber(currentB.Price, true) + " <sprite index=2>";
                }
            }
            else
            {
                if (current.Level > 0 && current.Price != 0)
                {
                    script.panelTitle.text = current.Name + " " + (current.Level + 1);
                }
                else
                {
                    script.panelTitle.text = current.Name;
                }
                if (current.Price == 0)
                {
                    //script.panelText.fontSize = 5.5f;
                    script.panelText.text = currentB.Description + "\nMaximum Level reached!";
                }
                else if (current.SubGenders.Count > 1)
                {
                    //script.panelText.fontSize = 5.5f;
                    script.panelText.text = "<b><color=#AF8B5A>Price:</b></color> " + script.RoundNumber(current.Price, true) + " <sprite index=2><b><color=#AF8B5A>\nLinked Artists:</b></color> " + current.Total + "\n<b><color=#AF8B5A>Included Currents:</b></color> " + current.SubGenders.Count + "\nEach level gives a boost.";
                }
                else
                {
                    //script.panelText.fontSize = 5.5f;
                    script.panelText.text = "<b><color=#AF8B5A>Price:</b></color> " + script.RoundNumber(current.Price, true) + " <sprite index=2><b><color=#AF8B5A>\nLinked Artists:</b></color> " + current.Total + "\nEach level gives a boost.";
                }
                if (!script.drawOn)
                {
                    script.panelDisplay = true;
                }
                else
                {
                    script.panel.position = new Vector3(-3.85f, 1.5f, 1000);
                }
            }
        }
    }
    public void MouseOut()
    {
        script.panelText.alignment = TMPro.TextAlignmentOptions.Justified;
        currentTime = 0;
        newScale = new Vector2(1f, 1f);
        script.overDraw = false;
        //script.panelText.fontSize = 5f;
        script.panel.gameObject.SetActive(false);
        script.panelTitle.text = "";
        script.panelText.text = "";
        script.panelDisplay = false;
    }
}
