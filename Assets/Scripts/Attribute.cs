﻿using System.Collections.Generic;

[System.Serializable]
public class Attribute
{
    public string name;
    public int amount;
    public int total;
    public int area;
    public float multiplicator;
    public bool available = false;

    public Attribute(string AttributeName, int AttributeAmount)
    {
        name = AttributeName;
        total = AttributeAmount;
    }
    public Attribute(string AttributeName, float Multiplicator)
    {
        name = AttributeName;
        multiplicator = Multiplicator;
    }
}