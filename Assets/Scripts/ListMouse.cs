﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListMouse : MonoBehaviour
{
    ListMaker script;
    public bool isEffect = false;
    public string title;
    public string text;
    float scaleOutBasis;
    float scaleInBasis;
    float scaleTextBasis;
    float scaleOutNew;
    float scaleInNew;
    float scaleTextNew;
    void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        scaleOutBasis = script.nationPanel.sizeDelta.x;
        scaleInBasis = script.nationPanelIn.sizeDelta.x;
        scaleTextBasis = script.nationPanelText.sizeDelta.x;
        scaleOutNew = scaleOutBasis + 60;
        scaleInNew = scaleInBasis + 60;
        scaleTextNew = scaleTextBasis + 60;
    }
    public void MouseIn()
    {
        if (script.focus)
        {
            script.nationPanel.gameObject.SetActive(true);
            script.nationText.text = text;
            if (isEffect)
            {
                script.nationTitle.text = title;
                script.nationPanel.sizeDelta = new Vector2(scaleOutNew, script.nationPanel.sizeDelta.y);
                script.nationPanelIn.sizeDelta = new Vector2(scaleInNew, script.nationPanelIn.sizeDelta.y);
                script.nationPanelText.sizeDelta = new Vector2(scaleTextNew, script.nationPanelText.sizeDelta.y);
                script.nationPanel.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x - 1.9f, Camera.main.ScreenToWorldPoint(Input.mousePosition).y + 1, 1000);

            }
            else
            {
                script.nationTitle.text = "Nation";
                script.nationPanel.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y + 1, 1000);
            }
        }
    }
    public void MouseOut()
    {
        if (isEffect)
        {
            script.nationPanel.sizeDelta = new Vector2(scaleOutBasis, script.nationPanel.sizeDelta.y);
            script.nationPanelIn.sizeDelta = new Vector2(scaleInBasis, script.nationPanelIn.sizeDelta.y);
            script.nationPanelText.sizeDelta = new Vector2(scaleTextBasis, script.nationPanelText.sizeDelta.y);
        }
        script.nationTitle.text = "";
        script.nationText.text = "";
        script.nationPanel.gameObject.SetActive(false);
    }
}