﻿using System.Collections.Generic;

[System.Serializable]
public class Effect
{
    public string Name;
    public string Type;
    public string Area;
    public string SubType;
    public List<string> Domain;
    public string Text;
    public bool Added = false;
}