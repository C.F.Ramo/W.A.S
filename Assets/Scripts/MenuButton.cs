﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButton : MonoBehaviour
{
    public bool isClose;
    float currentTime;
    Vector2 newScale = new Vector2(1f, 1f);
    Vector2 bigScale;
    void Start()
    {
        if (isClose)
        {
            bigScale = new Vector2(1.2f, 1.2f);
        }
        else
        {
            bigScale = new Vector2(1.05f, 1.05f);
        }
    }
    void Update()
    {
        if (currentTime < 0.1f)
        {
            currentTime += Time.deltaTime;
            this.transform.localScale = Vector2.Lerp(this.transform.localScale, newScale, currentTime * 10);
        }
    }
    public void In()
    {
        currentTime = 0;
        newScale = bigScale;
    }
    public void Out()
    {
        currentTime = 0;
        newScale = Vector2.one;
    }
}
