﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class MouseDetector : MonoBehaviour
{
    public Transform card;
    public Transform nextCard;
    public Transform currentCard;
    public Image imageCard;
    public FlyingCard fly;
    public Artist artist;
    public BoxMouse box;
    public Color[] effectColor;
    public Color cardColor;
    Scroll scroll;
    ListMaker script;
    RectTransform detector;
    Object flyingCard;
    Object littleEffect;
    Vector2 origin = new Vector2(0, 59);
    Vector2 originScale = new Vector2(115, 19);
    int originNext = -17;
    Vector2 partlyOpen = new Vector2(0, 29f);
    Vector2 partlyOpenScale = new Vector2(115, 79);
    int partlyOpenNext = -79;
    Vector2 fullyOpen = new Vector2(0, 0);
    Vector2 fullyOpenScale = new Vector2(115, 137);
    int fullyOpenNext = -142;
    public bool dragSpawn = false;
    public bool cardDomainDropped = false;
    public bool cardHeaderDropped = false;
    public bool outOfDomain = false;
    public bool nationOver = false;
    bool dragActive = false;
    public bool mouseOver = false;
    bool open = false;
    bool inOK = false;
    bool outOK = false;
    bool clickOK = false;
    float currentTime2;
    public int domain;
    public int headDropRef;
    public string artistName;
    public string[] effectName;
    CanvasGroup blur;
    // MoveCards
    float currentTime;
    Vector2 position;
    Vector2 scale;
    int nextPos;
    bool isOK;
    bool playSound = true;
    private void Start()
    {
        blur = GameObject.Find("ListBlur").GetComponent<CanvasGroup>();
        scroll = GameObject.Find("DomainSort").GetComponent<Scroll>();
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        detector = this.gameObject.GetComponent<RectTransform>();
        flyingCard = Resources.Load("FlyingCard");
        littleEffect = Resources.Load("SmallEffect");
    }
    private void Update()
    {
        if (dragActive)
        {
            if (currentTime2 < 0.5f)
            {
                currentTime2 += Time.deltaTime;
                card.localScale = new Vector2(currentTime2 * 2, currentTime2 * 2);
            }
        }
        if (open && !mouseOver && script.focus)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
            {
                open = false;
                CheckBools();
                script.PlaySound(script.clips[4], 0.75f, 1);
                MoveCard(origin, originScale, -17, !clickOK);
            }
        }
        if (currentTime < 1f && isOK)
        {
            CheckBools();
            currentTime += Time.deltaTime;
            float sDelta = Mathf.Lerp(detector.sizeDelta.y, scale.y, currentTime);
            float nPos = Mathf.Lerp(nextCard.localPosition.y, nextPos, currentTime);
            detector.localPosition = Vector2.Lerp(detector.localPosition, position, currentTime);
            detector.sizeDelta = new Vector2(detector.sizeDelta.x, sDelta);
            nextCard.localPosition = new Vector2(nextCard.localPosition.x, nPos);
        }
        if (nationOver && !mouseOver)
        {
            if (!open)
            {
                MoveCard(partlyOpen, partlyOpenScale, partlyOpenNext, true);
            }
            mouseOver = true;
        }
    }
    public void SwitchSize()
    {
        origin = new Vector2(0, 24.14f);
        partlyOpen = new Vector2(0, 1);
        partlyOpenScale = new Vector2(114.14f, 61.88f);
        fullyOpen = new Vector2(0, 1);
        fullyOpenScale = new Vector2(114.14f, 61.88f);
        partlyOpenNext = -70;
        fullyOpenNext = -70;
        mouseOver = false;
    }
    void CheckBools()
    {
        if (!open)
        {
            clickOK = false;
            if (mouseOver)
            {
                outOK = false;
                if (!script.drag)
                {
                    inOK = true;
                }
                else
                {
                    inOK = false;
                }
            }
            else
            {
                inOK = false;
                outOK = true;
            }
        }
        else
        {
            clickOK = true;
        }
    }
    public void PointerIn()
    {
        if (!script.drawOn && !script.drawing && !scroll.moving && !open && script.focus)
        {
            mouseOver = true;
            CheckBools();
            if (nationOver)
            {
                nationOver = false;
            }
            if (playSound)
            {
                script.PlaySound(script.clips[3], 0.4f, 1);
            }
            else
            {
                playSound = true;
            }
            MoveCard(partlyOpen, partlyOpenScale, partlyOpenNext, inOK);
        }
    }
    public void PointerOut()
    {
        if (nationOver)
        {
            playSound = false;
        }
        nationOver = false;
        mouseOver = false;
        CheckBools();
        if (!open)
        {
            MoveCard(origin, originScale, originNext, outOK);
        }
    }
    public void PointerClick()
    {
        if (!script.drawOn && !script.drawing && !scroll.moving && !open && script.focus)
        {
            script.PlaySound(script.clips[13], 1f, 1);
            if (!Input.GetMouseButton(1))
            {
                open = true;
                CheckBools();
                MoveCard(fullyOpen, fullyOpenScale, fullyOpenNext, clickOK);
            }
            else
            {
                StartCoroutine(ClickCard());
            }
        }
    }
    public void Drag()
    {
        if (!outOfDomain && !Input.GetMouseButton(1) && script.focus)
        {
            if ((cardHeaderDropped && script.headerLock[headDropRef]) || (cardDomainDropped && script.domainLock[domain]))
            {
            }
            else
            {
                if (!dragActive)
                {
                    script.PlaySound(script.clips[5], 0.5f, 1);
                    script.cardDomain = domain;
                    script.drag = true;
                    dragActive = true;
                    if (!dragSpawn)
                    {
                        currentTime2 = 0;
                        dragSpawn = true;
                        GameObject newCard = (GameObject)Object.Instantiate(flyingCard, GameObject.Find("User Interface").transform);
                        card = newCard.transform;
                        card.Find("Name").GetComponent<TextMeshProUGUI>().text = artistName;
                        imageCard = card.transform.Find("Image").GetComponent<Image>();
                        imageCard.color = new Color(cardColor.r, cardColor.g, cardColor.b, 0.5f);
                        card.GetComponent<Image>().raycastTarget = false;
                        fly = card.GetComponent<FlyingCard>();
                        fly.mouse = this;
                        fly.domain = domain;
                        for (int i = 0; i < effectName.Length; i++)
                        {
                            GameObject newEffect = (GameObject)Object.Instantiate(littleEffect, card.Find("Effects"));
                            newEffect.GetComponent<Image>().color = effectColor[i];
                            newEffect.transform.Find("Effect Name").GetComponent<TextMeshProUGUI>().text = effectName[i];
                        }
                    }
                    else if (cardDomainDropped)
                    {
                        RemoveCard(false);
                    }
                    else if (cardHeaderDropped)
                    {
                        RemoveCard(true);
                    }
                    else
                    {
                        currentTime2 = 0;
                        card.gameObject.SetActive(true);
                    }
                }
                card.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 1000);
            }
        }
    }
    void RemoveCard(bool head)
    {
        card.gameObject.SetActive(true);
        card.GetComponent<FlyingCard>().drag = true;
        card.SetParent(GameObject.Find("User Interface").transform);
        card.GetComponent<Image>().raycastTarget = false;
        if (head)
        {
            cardHeaderDropped = false;
            script.supremeLeadArtists[headDropRef] = null;
        }
        else
        {
            cardDomainDropped = false;
            script.leadArtists[domain] = null;
        }
        artist.Lead = 0;
        script.Calculation();
    }
    public void Drop()
    {
        if (!outOfDomain)
        {
            if (dragActive)
            {
                script.PlaySound(script.clips[6], 0.4f, 1);
                if (script.DomainDropAvailable)
                {
                    if (!script.domainLock[domain])
                    {
                        if (script.domainDrops[domain].childCount > 0)
                        {
                            for (int i = 0; i < script.domainDrops[domain].childCount; i++)
                            {
                                GameObject previous = script.domainDrops[domain].GetChild(i).gameObject;
                                previous.GetComponent<FlyingCard>().mouse.dragSpawn = false;
                                Destroy(previous);
                            }
                        }
                        fly.global = false;
                        card.SetParent(script.domainDrops[domain]);
                        imageCard.color = new Color(cardColor.r, cardColor.g, cardColor.b, 1);
                        card.GetComponent<Image>().raycastTarget = true;
                        card.localScale = new Vector2(1, 1);
                        card.localPosition = new Vector3(0, 0, 0);
                        if (script.leadArtists[domain] != null)
                        {
                            for (int i = 0; i < script.save.artistDomainActive[domain].Count; i++)
                            {
                                if (script.save.artistDomainActive[domain][i] == artist.Name)
                                {
                                    script.save.isArtistLead[domain][i] = 1;
                                }
                                if (script.save.artistDomainActive[domain][i] == script.leadArtists[domain].Name)
                                {
                                    script.save.isArtistLead[domain][i] = 0;
                                }
                            }
                            script.leadArtists[domain].Lead = 0;
                        }
                        else
                        {
                            for (int i = 0; i < script.save.artistDomainActive[domain].Count; i++)
                            {
                                if (script.save.artistDomainActive[domain][i] == artist.Name)
                                {
                                    script.save.isArtistLead[domain][i] = 1;
                                }
                            }
                        }
                        script.leadArtists[domain] = artist;
                        artist.Lead = 1;
                        cardDomainDropped = true;
                        script.DomainChange(domain);
                        script.Calculation();
                    }
                    else
                    {
                        DestroyCard();
                        script.PlaySound(script.clips[7], 1, 1);
                        script.panel.gameObject.SetActive(true);
                        script.panelTitle.text = "This slot is locked!";
                        script.panelText.text = "You can't change this trendsetter until you've enrolled a new Artist in this Domain.";
                    }
                }
                else if (script.HeaderDropAvailable)
                {
                    if (!script.headerLock[script.headerDrop])
                    {
                        fly.global = true;
                        if (script.headerDrops[script.headerDrop].childCount > 0)
                        {
                            for (int i = 0; i < script.headerDrops[script.headerDrop].childCount; i++)
                            {
                                GameObject previous = script.headerDrops[script.headerDrop].GetChild(i).gameObject;
                                previous.GetComponent<FlyingCard>().mouse.dragSpawn = false;
                                Destroy(previous);
                            }
                            script.headerDrops[script.headerDrop].DetachChildren();
                        }
                        card.SetParent(script.headerDrops[script.headerDrop]);
                        imageCard.color = new Color(cardColor.r, cardColor.g, cardColor.b, 1);
                        card.GetComponent<Image>().raycastTarget = true;
                        card.localScale = new Vector2(1, 1);
                        card.localPosition = new Vector3(0, 0, 0);
                        if (script.supremeLeadArtists[script.headerDrop] != null)
                        {
                            for (int i = 0; i < script.save.artistDomainActive[domain].Count; i++)
                            {
                                if (script.save.artistDomainActive[domain][i] == artist.Name)
                                {
                                    script.save.isArtistLead[domain][i] = 2;
                                }
                            }
                            for (int i = 0; i < script.save.artistDomainActive.Count; i++)
                            {
                                for (int y = 0; y < script.save.artistDomainActive[i].Count; y++)
                                {
                                    if (script.save.artistDomainActive[i][y] == script.supremeLeadArtists[script.headerDrop].Name)
                                    {
                                        script.save.isArtistLead[i][y] = 0;
                                    }
                                }
                            }
                            script.supremeLeadArtists[script.headerDrop].Lead = 0;
                            script.supremeLeadArtists[script.headerDrop] = artist;
                            artist.Lead = 2;
                            cardHeaderDropped = true;
                            headDropRef = script.headerDrop;
                            script.HeaderChange(headDropRef);
                            script.Calculation();
                        }
                        else
                        {
                            for (int i = 0; i < script.save.artistDomainActive[domain].Count; i++)
                            {
                                if (script.save.artistDomainActive[domain][i] == artist.Name)
                                {
                                    script.save.isArtistLead[domain][i] = 2;
                                }
                            }
                        }
                        script.supremeLeadArtists[script.headerDrop] = artist;
                        artist.Lead = 2;
                        cardHeaderDropped = true;
                        headDropRef = script.headerDrop;
                        script.Calculation();
                    }
                    else
                    {
                        DestroyCard();
                        script.PlaySound(script.clips[7], 1, 1);
                        script.panel.gameObject.SetActive(true);
                        script.panelTitle.text = "This slot is locked!";
                        script.panelText.text = "You can't change this trendsetter until you've enrolled a new Artist.";
                    }
                }
                else
                {
                    DestroyCard();
                }
                dragActive = false;
                script.drag = false;
                fly.drag = false;
            }
        }
    }
    void DestroyCard()
    {
        dragSpawn = false;
        Destroy(card.gameObject);
        for (int i = 0; i < script.save.artistDomainActive[domain].Count; i++)
        {
            if (script.save.artistDomainActive[domain][i] == artist.Name)
            {
                script.save.isArtistLead[domain][i] = 0;
            }
        }
        fly.global = false;
    }
    void MoveCard(Vector2 nPosition, Vector2 nScale, int nNextPos, bool nIsOK)
    {
        CheckBools();
        currentTime = 0;
        position = nPosition;
        scale = nScale;
        nextPos = nNextPos;
        isOK = nIsOK;
    }
    public IEnumerator ClickCard()
    {
        script.drawing = true;
        Transform card = GameObject.Instantiate(script.card, script.canvasTR).transform;
        script.DisplayCard(artist, card, domain, false, true);
        card.localScale = new Vector2(1.4f, 1.4f);
        card.Find("ProductionA").gameObject.SetActive(false);
        card.localPosition = new Vector2(0, -250);
        float actual;
        float currentTime = 0;
        blur.blocksRaycasts = true;
        while (currentTime <= 0.5f)
        {
            currentTime += Time.deltaTime;
            actual = Mathf.Lerp(card.localPosition.y, 0, currentTime * 2);
            card.localPosition = new Vector2(0, actual);
            blur.alpha = Mathf.Lerp(0, 1, currentTime * 2);
            yield return null;
        }
        card.localPosition = new Vector2(0, 0);
        blur.alpha = 1;
        while (Input.GetMouseButton(1)) { yield return null; }
        script.PlaySound(script.clips[4], 0.75f, 1);
        currentTime = 0;
        while (currentTime <= 0.5f)
        {
            currentTime += Time.deltaTime;
            actual = Mathf.Lerp(card.localPosition.y, 250, currentTime * 2);
            card.localPosition = new Vector2(0, actual);
            blur.alpha = Mathf.Lerp(1, 0, currentTime * 2);
            yield return null;
        }
        card.localPosition = new Vector2(0, 250);
        script.drawing = false;
        blur.blocksRaycasts = false;
        GameObject.Destroy(card.gameObject);
    }
}
