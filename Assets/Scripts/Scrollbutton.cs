﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class Scrollbutton : MonoBehaviour, IDragHandler
{
    ListMaker script;
    RectTransform rect;
    Scroll cam;
    Canvas canvas;
    private readonly float max = 60.9f;
    private readonly float min = -60.16f;
    private float interval;
    readonly float[] intervals = new float[20];
    float origin;
    float currentPos;
    float posX;
    int stage;
    private void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        canvas = GameObject.Find("User Interface").GetComponent<Canvas>();
        cam = GameObject.Find("DomainSort").GetComponent<Scroll>();
        rect = this.GetComponent<RectTransform>();
        stage = 1;
        origin = rect.anchoredPosition.y;
        currentPos = origin;
        interval = (max - min) / 19;
        for (int i = 0; i < intervals.Length; i++)
        {
            intervals[i] = origin - (interval * i);
        }
        posX = rect.anchoredPosition.x;
    }
    public void OnDrag(PointerEventData eventData)
    {
        if (script.focus)
        {
            if (stage > 0)
            {
                if (eventData.delta.y > 0)
                {
                    currentPos += eventData.delta.y / canvas.scaleFactor;
                }
            }
            if (stage < script.domainScroll)
            {
                if (eventData.delta.y < 0)
                {
                    currentPos += eventData.delta.y / canvas.scaleFactor;
                }
            }
        }
    }
    public void Update()
    {
        if (stage > 0)
        {
            if (cam.moving && !cam.foreignMovement)
            {
                stage = cam.stage;
                currentPos = intervals[stage];
            }
            else if (currentPos > intervals[stage - 1])
            {
                script.PlaySound(script.clips[1], 1.5f, 2);
                stage--;
                cam.SetMovement(false, true);
            }
        }
        if (stage < intervals.Length - 1)
        {
            if (cam.moving && !cam.foreignMovement)
            {
                stage = cam.stage;
                currentPos = intervals[stage];
            }
            else if (currentPos < intervals[stage + 1] && stage < script.domainScroll)
            {
                script.PlaySound(script.clips[1], 1.5f, 2);
                stage++;
                cam.SetMovement(true, true);
            }
        }
        rect.anchoredPosition = new Vector2(posX, intervals[stage]);
    }
}