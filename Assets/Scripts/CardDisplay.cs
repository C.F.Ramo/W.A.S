﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class CardDisplay
{
    public TextMeshProUGUI cardText;
    public TextMeshProUGUI cardTags;
    public TextMeshProUGUI cardDesc;
    public TextMeshProUGUI cardCurrents;
    public TextMeshProUGUI cardPrice;
    public TextMeshProUGUI cash;
    public Transform nationSort;
    public Transform effectSort;
    public CardSelection script;

    public CardDisplay(Transform card)
    {
        cardText = card.Find("Name").GetComponent<TextMeshProUGUI>();
        cardTags = card.Find("Tags").GetComponent<TextMeshProUGUI>();
        cardDesc = card.Find("Text").GetComponent<TextMeshProUGUI>();
        cardCurrents = card.Find("Currents").GetComponent<TextMeshProUGUI>();
        cardPrice = card.Find("ProductionA").GetComponent<TextMeshProUGUI>();
        nationSort = card.Find("NationSort");
        effectSort = card.Find("EffectSort");
        script = card.GetComponent<CardSelection>();
    }
}
