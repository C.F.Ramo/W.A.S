﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Accessibility : MonoBehaviour
{
    Transform panel;
    TextMeshProUGUI panelText;
    ListMaker script;
    CanvasGroup blur;
    public string text;

    void Start()
    {
        panel = GameObject.Find("TextAccessibility").transform;
        panelText = panel.GetChild(0).GetComponent<TextMeshProUGUI>();
        blur = GameObject.Find("ListBlur").GetComponent<CanvasGroup>();
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
    }
    public void Click()
    {
        StartCoroutine(ClickCard());
    }
    IEnumerator ClickCard()
    {
        panel.gameObject.SetActive(true);
        panel.localPosition = new Vector2(0, -250);
        panelText.text = text;
        float actual;
        float currentTime = 0;
        blur.blocksRaycasts = true;
        while (currentTime <= 0.5f)
        {
            currentTime += Time.deltaTime;
            actual = Mathf.Lerp(panel.localPosition.y, 0, currentTime * 0.5f);
            panel.localPosition = new Vector2(0, actual);
            blur.alpha = Mathf.Lerp(0, 1, currentTime * 2);
            yield return null;
        }
        panel.localPosition = new Vector2(0, 0);
        blur.alpha = 1;
        script.click = true;
        while (script.click) { yield return null; }
        currentTime = 0;
        while (currentTime <= 0.5f)
        {
            currentTime += Time.deltaTime;
            actual = Mathf.Lerp(panel.localPosition.y, 250, currentTime * 0.5f);
            panel.localPosition = new Vector2(0, actual);
            blur.alpha = Mathf.Lerp(1, 0, currentTime * 2);
            yield return null;
        }
        panel.localPosition = new Vector2(0, 250);
        blur.blocksRaycasts = false;
        panel.gameObject.SetActive(false);
    }
}
