﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class Bonus
{
    public Sprite Icon;
    public GameObject Item;
    public GameObject Filter;
    public GameObject EraIcon;
    public string Name;
    public string Description;
    public int Level;
    public int Ref;
    public float Price;
    public Bonus(Sprite icon, string name, int level, string description, int bonusRef)
    {
        Icon = icon;
        Name = name;
        Level = level;
        Description = description;
        Ref = bonusRef;
    }
}
