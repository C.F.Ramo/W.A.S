﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NationMouse : MonoBehaviour
{
    ListMaker script;
    MouseDetector card;
    public bool active = false;
    public bool isEffect = false;
    public string title;
    public string nation;
    bool wasActive = false;
    public bool cardActive = false;
    public Transform pos;
    void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        card = this.transform.parent.parent.Find("MouseDetector").GetComponent<MouseDetector>();
    }
    public void MouseIn()
    {
        if (script.focus && active && (!cardActive || (!script.drawing && !script.drawOn)))
        {
            script.nationPanel.gameObject.SetActive(true);
            if (script.panel.gameObject.activeInHierarchy)
            {
                wasActive = true;
                script.panel.gameObject.SetActive(false);
            }
            else
            {
                script.nationPanel.gameObject.SetActive(true);
            }
            if (cardActive)
            {
                card.nationOver = true;
                script.nationPanel.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y + 1, 1000);
            }
            else
            {
                script.nationPanel.position = new Vector3(pos.position.x, pos.position.y + 2.5f, 1000);
            }
            script.nationText.text = nation;
            if (isEffect)
            {
                script.nationTitle.text = title;
            }
            else
            {
                script.nationTitle.text = "Nation";
            }
        }
    }
    public void MouseOut()
    {
        if (wasActive)
        {
            script.panel.gameObject.SetActive(true);
            wasActive = false;
        }
        if (card.nationOver)
        {
            card.PointerOut();
        }
        script.nationText.text = "";
        script.nationPanel.gameObject.SetActive(false);
    }
}
