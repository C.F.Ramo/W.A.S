﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColumnTitle : MonoBehaviour
{
    public int reference;
    Transform textSize;
    Image highlight;
    ListMaker script;
    float currentTime = 0.1f;
    Vector2 basisScale;
    Vector2 newScale = new Vector2(1f, 1f);
    Color basisColor;
    Color newColor;
    public MouseDetector mouse;
    public Artist artist;
    public int domain;
    Transform canvas;
    CanvasGroup blur;
    void Start()
    {
        canvas = GameObject.Find("User Interface").transform;
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        blur = GameObject.Find("ListBlur").GetComponent<CanvasGroup>();
        textSize = this.transform.GetChild(0);
        highlight = this.GetComponent<Image>();
    }
    void Update()
    {
        if (currentTime < 0.1f)
        {
            currentTime += Time.deltaTime;
            textSize.localScale = Vector2.Lerp(basisScale, newScale, currentTime * 10f);
            highlight.color = Color.Lerp(basisColor, newColor, currentTime * 10f);
        }
    }
    public void PointerIn()
    {
        if (script.focus)
        {
            basisScale = textSize.localScale;
            newScale = new Vector2(1.07f, 1.07f);
            basisColor = highlight.color;
            newColor = new Color(highlight.color.r, highlight.color.g, highlight.color.b, 0.02f);
            currentTime = 0;
        }
    }
    public void PointerOut()
    {
        basisScale = textSize.localScale;
        newScale = Vector2.one;
        basisColor = highlight.color;
        newColor = new Color(highlight.color.r, highlight.color.g, highlight.color.b, 0f);
        currentTime = 0;
    }
    public void Click()
    {
        if (script.focus)
        {
            if (reference == 10)
            {
                StartCoroutine(ClickCard());
            }
            else
            {
                script.SortList(reference);
            }
        }
    }
    public void Drag()
    {
        if (script.focus)
        {
            mouse.Drag();
        }
    }
    public void Drop()
    {
        mouse.Drop();
    }
    public IEnumerator ClickCard()
    {
        script.PlaySound(script.clips[13], 1f, 1);
        script.drawing = true;
        Transform card = GameObject.Instantiate(script.card, canvas).transform;
        card.localScale = new Vector2(1.4f, 1.4f);
        script.DisplayCard(artist, card, domain, false, true);
        card.Find("ProductionA").gameObject.SetActive(false);
        card.localPosition = new Vector2(0, -250);
        float actual;
        float currentTime = 0;
        blur.blocksRaycasts = true;
        while (currentTime <= 0.5f)
        {
            currentTime += Time.deltaTime;
            actual = Mathf.Lerp(card.localPosition.y, 0, currentTime * 2);
            card.localPosition = new Vector2(0, actual);
            blur.alpha = Mathf.Lerp(0, 1, currentTime * 2);
            yield return null;
        }
        card.localPosition = new Vector2(0, 0);
        blur.alpha = 1;
        script.click = true;
        while (script.click) { yield return null; }
        script.PlaySound(script.clips[4], 0.75f, 1);
        currentTime = 0;
        while (currentTime <= 0.5f)
        {
            currentTime += Time.deltaTime;
            actual = Mathf.Lerp(card.localPosition.y, 250, currentTime * 2);
            card.localPosition = new Vector2(0, actual);
            blur.alpha = Mathf.Lerp(1, 0, currentTime * 2);
            yield return null;
        }
        card.localPosition = new Vector2(0, 250);
        script.drawing = false;
        blur.blocksRaycasts = false;
        GameObject.Destroy(card.gameObject);
    }
}
