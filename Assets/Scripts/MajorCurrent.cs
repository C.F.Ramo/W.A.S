﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class MajorCurrent
{
    public string Name;
    public int Total;
    public int Doubles;
    public int Amount;
    public double Prod;
    public List<int> SubCurrents;
    public int Era;
    public int Level;
    public bool Unlocked = false;
}
