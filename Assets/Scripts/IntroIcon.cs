﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroIcon : MonoBehaviour
{
    ListMaker script;
    Transform icon;
    Transform text;
    Vector2 basisScale;
    Vector2 newScale = Vector2.one;
    float currentTime = 0.1f;
    float basisText;
    float newText;
    float textOrigin;
    float displacementAmount = 10;
    public bool isNoIntro;
    void Start()
    {
        script = GameObject.Find("DBReader").GetComponent<ListMaker>();
        icon = this.transform;
        if (!isNoIntro)
        {
            text = icon.GetChild(0).transform;
            textOrigin = text.localPosition.x;
        }
    }
    void Update()
    {
        if (currentTime < 0.1f)
        {
            currentTime += Time.deltaTime;
            icon.localScale = Vector2.Lerp(basisScale, newScale, currentTime * 10f);
            if (!isNoIntro)
            {
                text.localPosition = new Vector2(Mathf.Lerp(basisText, newText, currentTime * 10f), text.localPosition.y);
            }
        }
    }
    public void MouseIn()
    {
        script.PlaySound(script.clips[3], 0.25f, 1);
        currentTime = 0;
        newScale = new Vector2(1.1f, 1.1f);
        basisScale = icon.localScale;
        if (!isNoIntro)
        {
            newText = textOrigin - displacementAmount;
            basisText = text.localPosition.x;
        }
    }
    public void MouseOut()
    {
        currentTime = 0;
        newScale = Vector2.one;
        if (!isNoIntro)
        {
            basisText = text.localPosition.x;
            newText = textOrigin;
        }
    }
}
